<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Bap;
use Carbon\Carbon;
use DateTimeZone;

class UpdateAsistenStatus extends Command
{
    protected $signature = 'update:asisten-status';
    protected $description = 'Update asisten status to "Tidak Hadir" for unvalidated BAPs';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        // Set zona waktu menjadi "Asia/Jakarta"    
        date_default_timezone_set('Asia/Jakarta');
        $tanggalKustom = Carbon::createFromFormat('d-m-Y', '08-09-2023');

        $unvalidatedBaps = Bap::where('tanggal', '<', Carbon::now('Asia/Jakarta'))->get();
        $carbonUTC = Carbon::now();
        
        
        foreach ($unvalidatedBaps as $bap) {
            // Periksa asisten1
            if (!is_null($bap->asisten1) && $bap->asisten1 !== 'HADIR') {
                $bap->asisten1 = 'TIDAK HADIR';
            }
        
            // Periksa asisten2
            if (!is_null($bap->asisten2) && $bap->asisten2 !== 'HADIR') {
                $bap->asisten2 = 'TIDAK HADIR';
            }
        
            // Periksa asisten3
            if (!is_null($bap->asisten3) && $bap->asisten3 !== 'HADIR') {
                $bap->asisten3 = 'TIDAK HADIR';
            }
        
            // Menyimpan perubahan BAP
            $bap->save();
        }
        

        $this->info('Asisten status updated for unvalidated BAPs.');
    }
}
