<?php

namespace App\Exceptions;


use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\Debug\ExceptionHandler as SymfonyExceptionHandler;
use App\Mail\ExceptionOccured;
use Illuminate\Support\Facades\Mail;
 
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<\Throwable>, \Psr\Log\LogLevel::*>
     */
    protected $levels = [
        //
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<\Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

//     public function report(Throwable $e)
// {
//     if ($e instanceof \Exception) {
//         // Fetch the error information we would like to 
//         // send to the view for emailing
//         $error['file']    = $e->getFile();
//         $error['code']    = $e->getCode();
//         $error['line']    = $e->getLine();
//         $error['message'] = $e->getMessage();
//         $error['trace']   = $e->getTrace();

//         // Only send email reports on production server
//         if(ENV('APP_ENV') == "production"){
//             #1. Queue email for sending on "exceptions_emails" queue
//             #2. Use the emails.exception_notif view shown below
//             #3. Pass the error array to the view as variable $e
//             Mail::queueOn('exception_emails', 'emails.exception_notif', ["e" => $error], function ($m) {
//                 $m->subject("Laravel Error");
//                 $m->from(ENV("MAIL_FROM"), ENV("MAIL_NAME"));
//                 $m->to("", "Webmaster");
//             });

//         }
//     }

//     // Pass the error on to continue processing
//     return parent::report($e);
// }

//     public function report(Throwable $exception)
//     {   
//        if ($this->shouldReport($exception)) {
//             $this->sendEmail($exception); // sends an email
//         }
//         parent::report($exception);
//     }
 
//     /**
//      * Render an exception into an HTTP response.
//      *
//      * @param  \Illuminate\Http\Request  $request
//      * @param  \Exception  $exception
//      * @return \Illuminate\Http\Response
//      */
//     public function render($request, Throwable $exception)
//     {
//         return parent::render($request, $exception);
//     }
 
//     public function sendEmail(Throwable $exception)
//     {
//         try {
//             $e = FlattenException::create($exception);
 
//             $handler = new SymfonyExceptionHandler();
 
//             $html = $handler->getHtml($e);
 
//             Mail::to('developer@gmail.com')->send(new ExceptionMail($html));
//         } catch (Exception $ex) {
//             dd($ex);
//         }
//     }
}
