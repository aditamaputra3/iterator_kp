<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;

class PenggunaExport implements FromCollection
{
    public function collection()
    {
        return User::all();
    }
}
