<?php

namespace App\Http\Controllers;

use App\Models\DetailKondisi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class DetailKondisiController extends Controller
{
    public function index(Request $request)
    {
        $data = DetailKondisi::all();
        if ($request->ajax()) {
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('aksi', function ($row) {
                    $btn = '<a href="javascript:void(0)" data-id="' . $row->id . '" class="edit btn btn-warning btn-sm editData"><i class="fa fa-edit"></i>&nbsp;Edit</a>';

                    $btn .= ' <a href="javascript:void(0)" data-id="' . $row->id . '" class="btn btn-danger btn-sm deleteData" data-url="' . route('detail-kondisi.store') . '"><i class="fa fa-trash"></i>&nbsp;Delete</a>';

                    return $btn;
                })
                ->rawColumns(['aksi'])
                ->make(true);
        }

        return view('detailkondisi.list');
    }

    public function store(Request $request)
    {
        if (!empty($request->id)) {
            $data = DetailKondisi::find($request->id);
            $data->nama_kondisi = $request->nama_kondisi;
            $data->keterangan_kondisi = $request->keterangan_kondisi;
            $data->save();
        } else {
            $detailKondisi = new DetailKondisi;
            $detailKondisi->nama_kondisi = $request->nama_kondisi;
            $detailKondisi->keterangan_kondisi = $request->keterangan_kondisi;
            $detailKondisi->save();
        }

        return response()->json(['success' => 'Data saved successfully.']);
    }

    public function edit($id)
    {
        $detail_kondisi = DetailKondisi::find($id);
        return response()->json($detail_kondisi);
    }

    public function destroy($id)
    {
        DetailKondisi::find($id)->delete();
        return response()->json(['success' => 'Data deleted successfully.']);
    }
}
