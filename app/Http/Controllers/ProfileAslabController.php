<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AsistenLab;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class ProfileAslabController extends Controller
{
    // public function index(Request $request)
    // {
    //     $user = $request->user(); // Mengambil user yang sedang login

    //     $data['profile'] = AsistenLab::with(['user'])
    //     ->where('id_user', $user->id)
    //             ->latest()
    //             ->get();


    //     return view('profile.profile-aslab',$data);
    // }
    
    public function getStatusAsisten()
    {
        // Mendapatkan id_user dari pengguna yang sudah login
        $userId = Auth::id();

        if ($userId) {
            $kodePraktikum = AsistenLab::join('users', 'asisten_lab.id_user', '=', 'users.id')
                ->join('asisten_praktikum', 'asisten_lab.nrp', '=', 'asisten_praktikum.nrp')
                ->join('praktikum', 'asisten_praktikum.kode_praktikum', '=', 'praktikum.kode_praktikum')
                ->join('matakuliah', 'praktikum.kode_matakuliah', '=', 'matakuliah.kode_matakuliah')
                ->select('asisten_praktikum.kode_praktikum', 'matakuliah.kode_matakuliah', 'matakuliah.nama_matakuliah', 'praktikum.kelas', 'users.nama_lengkap')
                ->where('asisten_lab.id_user', $userId)
                ->get();

            // if ($kodePraktikum) {
            //     $kodePraktikum = $kodePraktikum->kode_praktikum;
            //     // Lakukan sesuatu dengan $kodePraktikum
            // } else {
            //     return 'tidak ditemukan';
            // }

            // // Debugging: Tampilkan nilai $kodePraktikum
            // dd($kodePraktikum);
        } else {
            return 'tidak ditemukan';
        }

        // Kembalikan nilai $kodePraktikum jika diperlukan
        return $kodePraktikum;
    }

    public function index()
    {   

        $kodePraktikum = $this->getStatusAsisten();
        // Ambil data profil pengguna, misalnya berdasarkan user yang sedang login
        $users = auth()->user();

        // Jika Anda menggunakan model AsistenLab, Anda dapat menyesuaikan ini
        $profile = AsistenLab::with(['users'])
            ->where('id_user', $users->id)->first();

        if (!$profile) {

            return view('errors.belum-tersedia');
        }
        
        return view('profile.profile-aslab', compact('profile','kodePraktikum'));
    }

    public function edit(Request $request, $id)
    {
        // Validasi data yang diterima dari form
        $request->validate([
            'nrp' => 'required',
            'no_telp' => 'nullable',
            'no_rekening' => 'nullable',
            'nama_bank' => 'nullable',
            // Tambahkan validasi lainnya sesuai kebutuhan
        ]);

        // Cari profil pengguna berdasarkan ID
        $profile = AsistenLab::find($id);

        // Update data profil pengguna
        $profile->nrp = $request->nrp;
        $profile->no_telp = $request->no_telp;
        $profile->no_rekening = $request->no_rekening;
        $profile->nama_bank = $request->nama_bank;

        // Simpan perubahan
        $profile->save();

        return response()->json(['success' => 'Profil berhasil diperbarui.']);
    }
}
