<?php

namespace App\Http\Controllers;

use App\Models\AsistenLab;
use App\Models\AsistenPraktikum;
use App\Models\Dosen;
use App\Models\Matakuliah;
use App\Models\Bap;
use App\Models\Praktikum;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Str;



class PraktikumController extends Controller
{

    public function index(Request $request)
    {
        $matakuliah = Matakuliah::pluck('nama_matakuliah', 'kode_matakuliah')->toArray();
        $dosen = Dosen::pluck('nama_dosen', 'nip')->toArray();
        if ($request->ajax()) {
            $data = Praktikum::with(['matakuliah', 'dosen'])->latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('nama_matakuliah', function ($row) {
                    return $row->matakuliah->nama_matakuliah ?? '';
                })
                ->addColumn('nama_dosen', function ($row) {
                    return $row->dosen->nama_dosen ?? '';
                })
                ->addColumn('aksi', function ($row) {
                    $btn = '<a href="javascript:void(0)" data-id="' . $row->id . '" class="edit btn btn-warning btn-sm editData"><i class="fa fa-edit"></i>&nbsp;Edit</a>';
                    $btn .= ' <a href="javascript:void(0)" data-id="' . $row->id . '" class="btn btn-danger btn-sm deleteData" data-url="' . route('praktikum.store') . '"><i class="fa fa-trash"></i>&nbsp;Delete</a>';
                    $btn .= ' <a href="' . route('praktikum.asisten', ['kode_praktikum' => $row->kode_praktikum]) . '" class="btn btn-info btn-sm"><i class="fa fa-eye"></i>&nbsp;Detail</a>'; // Tambahkan ini
                    return $btn;
                })

                ->rawColumns(['nama_matakuliah', 'nama_dosen', 'aksi'])
                ->make(true);
        }

        return view('praktikum.list', [
            "matakuliah" => $matakuliah, "dosen" => $dosen
        ]);
    }

    public function store(Request $request)
    {
        $kodePraktikum = $this->generatePraktikumId();

        // Memeriksa apakah kode_matakuliah dan kelas sudah ada di database
        $existingData = Praktikum::where('kode_matakuliah', $request->kode_matakuliah)
            ->where('kelas', $request->kelas)
            ->first();

        if ($existingData) {
            return response()->json(['error' => 'Praktikum sudah ada'], 422);
        }

        if (!empty($request->id)) {
            $data = Praktikum::find($request->id);
            $data->kode_matakuliah = $request->kode_matakuliah;
            $data->kelas = $request->kelas;
            $data->jumlah_peserta = $request->jumlah_peserta;
            $data->jumlah_week = $request->jumlah_week;
            $data->nip = $request->nip;
            $data->save();
        } else {
            Praktikum::create([
                'kode_praktikum' => $kodePraktikum,
                'kode_matakuliah' => $request->kode_matakuliah,
                'kelas' => $request->kelas,
                'jumlah_peserta' => $request->jumlah_peserta,
                'jumlah_week' => $request->jumlah_week,
                'nip' => $request->nip,
            ]);
        }

        return response()->json(['success' => 'Data saved successfully.']);
    }

    public function edit($id)
    {
        $praktikum = Praktikum::find($id);
        return response()->json($praktikum);
    }

    public function destroy($id)
    {
        Praktikum::find($id)->delete();
        return response()->json(['success' => 'Data deleted successfully.']);
    }

    protected function generatePraktikumId()
    {
        $lastPraktikum = Praktikum::latest('kode_praktikum')->first();

        if (!$lastPraktikum) {
            return 'PRK-001'; // If no previous supplier exists, start with SUP-001
        }

        $lastId = intval(substr($lastPraktikum->kode_praktikum, 4)); // Extract the numeric portion of the last ID
        $newId = $lastId + 1;
        $paddedNewId = str_pad($newId, 3, '0', STR_PAD_LEFT); // Pad the new ID with leading zeros if necessary
        $generatedId = 'PRK-' . $paddedNewId;

        return $generatedId;
    }

    public function asisten(Request $request, $kode_praktikum)
    {
        $praktikum = Praktikum::join('matakuliah', 'praktikum.kode_matakuliah', '=', 'matakuliah.kode_matakuliah')
            ->where('praktikum.kode_praktikum', $kode_praktikum)
            ->select('praktikum.kode_praktikum', 'praktikum.kode_matakuliah', 'praktikum.kelas', 'matakuliah.nama_matakuliah')
            ->first();
            // dd($praktikum);

        $asistenPraktikum = AsistenLab::join('users', 'asisten_lab.id_user', '=', 'users.id')
            ->select('asisten_lab.nrp', 'users.nama_lengkap')
            ->get()
            ->toArray();

        if ($request->ajax()) {
            $data = AsistenPraktikum::with(['asistenLab.users'])
                ->whereHas('asistenLab.users', function ($query) use ($kode_praktikum) {
                    $query->where('kode_praktikum', $kode_praktikum);
                })
                ->latest()
                ->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('nama_lengkap', function ($row) {
                    return $row->asistenLab->users->nama_lengkap ?? '';
                })
                ->addColumn('no_telp', function ($row) {
                    return $row->asistenLab->no_telp ?? '';
                })
                ->addColumn('no_rekening', function ($row) {
                    return $row->asistenLab->no_rekening ?? '';
                })
                ->addColumn('nama_bank', function ($row) {
                    return $row->asistenLab->nama_bank ?? '';
                })
                
                ->addColumn('aksi', function ($row) {
                    // $btn = '<a href="javascript:void(0)" data-id="' . $row->id . '" class="edit btn btn-warning btn-sm editData"><i class="fa fa-edit"></i>&nbsp;Edit</a>';
                    $btn = ' <a href="javascript:void(0)" data-id="' . $row->id . '" class="btn btn-danger btn-sm deleteData" data-url="' . route('praktikum.store') . '"><i class="fa fa-trash"></i>&nbsp;Delete</a>';
                    return $btn;
                })
                ->rawColumns(['nama_lengkap', 'aksi'])
                ->make(true);
        }


        return view('praktikum.asisten', [
            "asistenPraktikum" => $asistenPraktikum, "praktikum" => $praktikum
        ]);
    }

    public function storeAsisten(Request $request)
    {
        // $kodePraktikum = Praktikum::where('kode_praktikum', $request->kode_praktikum)->first();

        $existingDataAsisten = AsistenPraktikum::where('kode_praktikum', $request->kode_praktikum)
            ->where('nrp', $request->nrp)
            ->first();

        if ($existingDataAsisten) {
            return response()->json(['error' => 'Asisten Sudah Ada Pada Pada Kelas Praktikum Ini'], 422);
        }

        AsistenPraktikum::create([
            'kode_praktikum' => $request->kode_praktikum,
            'nrp' => $request->nrp,
        ]);

        return redirect()->route('praktikum.asisten', ['kode_praktikum' => $request->kode_praktikum])
            ->with('success', 'Asisten Lab berhasil ditambahkan');
    }

    public function deleteAsisten($id)
    {
        AsistenPraktikum::find($id)->delete();
        return response()->json(['success' => 'Data deleted successfully.']);
    }

    protected function generateBapId()
    {
        $lastBap = Bap::latest('kode_bap')->first();

        if (!$lastBap) {
            return 'BAP-001'; // If no previous BAP exists, start with BAP-001
        }

        $lastId = intval(substr($lastBap->kode_bap, 4)); // Extract the numeric portion of the last ID
        $newId = $lastId + 1;
        $paddedNewId = str_pad($newId, 3, '0', STR_PAD_LEFT); // Pad the new ID with leading zeros if necessary
        $generatedId = 'BAP-' . $paddedNewId;

        return $generatedId;
    }
}
