<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BapPraktikum;
use App\Models\AsistenLab;
use App\Models\Praktikum;
use App\Models\ValidasiMahasiswa;
use App\Models\ValidasiAsisten;
use App\Models\AsistenPraktikum;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;

class BapPraktikumController extends Controller
{
    public function index(Request $request)
    {
        $userId = Auth::id();
        $role = auth()->user()->peran_pengguna;

        if ($userId && $role == 'ASISTEN') {
            $kodePraktikum = AsistenLab::join('users', 'asisten_lab.id_user', '=', 'users.id')
                ->join('asisten_praktikum', 'asisten_lab.nrp', '=', 'asisten_praktikum.nrp')
                ->join('praktikum', 'asisten_praktikum.kode_praktikum', '=', 'praktikum.kode_praktikum')
                ->join('matakuliah', 'praktikum.kode_matakuliah', '=', 'matakuliah.kode_matakuliah')
                ->select('praktikum.kode_praktikum', 'matakuliah.kode_matakuliah', 'matakuliah.nama_matakuliah', 'praktikum.kelas')
                ->where('asisten_lab.id_user', $userId)
                ->get();
        } elseif ($role == 'KEPALA' || $role == 'TEKNISI') {
            $kodePraktikum = Praktikum::join('matakuliah', 'praktikum.kode_matakuliah', '=', 'matakuliah.kode_matakuliah')
                ->select('praktikum.kode_praktikum', 'matakuliah.kode_matakuliah', 'matakuliah.nama_matakuliah', 'praktikum.kelas')
                ->get();
        }

        return view('bap.list', compact('kodePraktikum'));
    }

    public function listBap(Request $request, $encryptedKodePraktikum){
        $kode_praktikum = decrypt($encryptedKodePraktikum);
        $carbonUTC = Carbon::now();
        $tanggalSekarang= $carbonUTC->tz('Asia/Jakarta');

        $praktikum = Praktikum::join('matakuliah', 'praktikum.kode_matakuliah', '=', 'matakuliah.kode_matakuliah')
            ->where('praktikum.kode_praktikum', $kode_praktikum)
            ->select('praktikum.kode_praktikum', 'praktikum.kode_matakuliah', 'praktikum.kelas', 'matakuliah.nama_matakuliah')
            ->first();

        $Asisten = AsistenLab::join('users', 'asisten_lab.id_user', '=', 'users.id')
            ->join('asisten_praktikum', 'asisten_lab.nrp', '=', 'asisten_praktikum.nrp')
            ->join('praktikum', 'asisten_praktikum.kode_praktikum', '=', 'praktikum.kode_praktikum')
            ->join('matakuliah', 'praktikum.kode_matakuliah', '=', 'matakuliah.kode_matakuliah')
            ->where('praktikum.kode_praktikum', $kode_praktikum)
            ->select('asisten_praktikum.nrp')
            ->get();
        // Mendapatkan status mahasiswa berdasarkan BAP
    $statusMahasiswa = ValidasiMahasiswa::where('nrp', $Asisten);
    $statusAsisten = ValidasiAsisten::where('nrp', $Asisten);
        $bapList = BapPraktikum::where('kode_praktikum', $kode_praktikum)->get();

        return view('bap.list-bap', [
            "praktikum" => $praktikum,
            'kode_praktikum' => $kode_praktikum,
            'Asisten' => $Asisten,
            'tanggalSekarang' => $tanggalSekarang,
            'bapList' => $bapList,
            'statusMahasiswa' => $statusMahasiswa,
            'statusAsisten' => $statusAsisten
        ]);
    }

    public function storeBap(Request $request)
    {
        // Validasi input sesuai kebutuhan Anda
        $validatedData = $request->validate([
            'kode_praktikum' => 'required',
            'tanggal' => 'required|date',
            'materi_praktikum' => 'required',
        ]);

        // Simpan data BAP
        BapPraktikum::create($validatedData);

        return redirect()->route('bap_praktikum.index')->with('success', 'BAP berhasil disimpan.');
    }

    public function deleteBap(Request $request)
    {
      
    }

    public function validateAsisten(Request $request)
    {
      
    }

    public function validateMhs(Request $request)
    {
      
    }
}
