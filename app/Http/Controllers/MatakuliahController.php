<?php

namespace App\Http\Controllers;

use App\Models\Matakuliah;
use App\Models\Dosen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\MatakuliahImport;

class MatakuliahController extends Controller
{

    public function index(Request $request)
    {
        $dosen = Dosen::pluck('nama_dosen', 'nip')->toArray();
        if ($request->ajax()) {
            $data = Matakuliah::All();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('aksi', function ($row) {
                    $btn = '<a href="javascript:void(0)" data-id="' . $row->id . '" class="edit btn btn-warning btn-sm editData"><i class="fa fa-edit"></i>&nbsp;Edit</a>';
                    $btn .= ' <a href="javascript:void(0)" data-id="' . $row->id . '" class="btn btn-danger btn-sm deleteData" data-url="' . route('matakuliah.store') . '"><i class="fa fa-trash"></i>&nbsp;Delete</a>';

                    return $btn;
                })
                ->rawColumns(['aksi'])
                ->make(true);
        }

        return view('matakuliah.list', [
            "dosen" => $dosen,
        ]);
    }


    public function store(Request $request)
    {
        if (!empty($request->id)) {
            $data = Matakuliah::find($request->id);
            $data->kode_matakuliah = $request->kode_matakuliah;
            $data->nama_matakuliah = $request->nama_matakuliah;
            $data->sks = $request->sks;
            $data->semester = $request->semester;
            $data->tahun_ajaran = $request->tahun_ajaran;
            $data->save();
        } else {
            Matakuliah::create([
                'kode_matakuliah' => $request->kode_matakuliah,
                'nama_matakuliah' => $request->nama_matakuliah,
                'sks' => $request->sks,
                'semester' => $request->semester,
                'tahun_ajaran' => $request->tahun_ajaran,
            ]);
        }

        return response()->json(['success' => 'Data saved successfully.']);
    }

    public function edit($id)
    {
        $matakuliah = Matakuliah::find($id);
        return response()->json($matakuliah);
    }

    public function destroy($id)
    {
        Matakuliah::find($id)->delete();
        return response()->json(['success' => 'Data deleted successfully.']);
    }

    public function import(Request $request) 
    {
        // Validasi tipe file yang diizinkan
        $request->validate([
            'file' => 'required|mimes:csv,xls,xlsx',
        ]);
    
        $file = $request->file('file');
    
        Excel::import(new MatakuliahImport, $file);
    
        return response()->json(['success' => 'Data imported successfully.']);
    }
}
