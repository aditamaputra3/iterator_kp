<?php

namespace App\Http\Controllers;

use App\Models\Matakuliah;
use App\Models\Jadwal;
use App\Models\Praktikum;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Str;
use Carbon\Carbon;

class JadwalController extends Controller
{
    public function index(Request $request)
    {
        $praktikum = Praktikum::join('matakuliah', 'praktikum.kode_matakuliah', '=', 'matakuliah.kode_matakuliah')
            ->select('praktikum.kode_praktikum', 'praktikum.kode_matakuliah', 'praktikum.kelas', 'matakuliah.nama_matakuliah')
            ->get()
            ->toArray();
        $existingKodePraktikum = Jadwal::pluck('kode_praktikum')->toArray();

        if ($request->ajax()) {
            $data = Jadwal::with(['praktikum.matakuliah'])->latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('kode_matakuliah', function ($row) {
                    return $row->praktikum->kode_matakuliah ?? '';
                })
                ->addColumn('kelas', function ($row) {
                    return $row->praktikum->kelas ?? '';
                })

                ->addColumn('nama_matakuliah', function ($row) {
                    return $row->praktikum->matakuliah->nama_matakuliah ?? '';
                })

                ->addColumn('aksi', function ($row) {
                    $btn = '<a href="javascript:void(0)" data-id="' . $row->id . '" class="edit btn btn-warning btn-sm editData"><i class="fa fa-edit"></i>&nbsp;Edit</a>';
                    $btn .= ' <a href="javascript:void(0)" data-id="' . $row->id . '" class="btn btn-danger btn-sm deleteData" data-url="' . route('jadwal.store') . '"><i class="fa fa-trash"></i>&nbsp;Delete</a>';
                    return $btn;
                })
                ->rawColumns(['kode_matakuliah', 'nama_matakuliah', 'kelas', 'aksi'])
                ->make(true);
        }

        return view('jadwal.list', ["praktikum" => $praktikum, "existingKodePraktikum" => $existingKodePraktikum]);
    }

    public function store(Request $request)
    {
        $kodeJadwal = $this->generateJadwalId();
        $hari = $request->hari;
        $ruang = $request->ruang;
        $jamMulai = $request->jam_mulai;
        $jamBerakhir = $this->calculateEndTime($jamMulai, $request->durasi);
    
        if (!empty($request->id)) {
            $data = Jadwal::find($request->id);
            $cekBentrok = $this->cekJadwalBentrok($jamMulai, $jamBerakhir, $hari, $ruang, $request->id);
            if ($cekBentrok) {
                return response()->json(['error' => $cekBentrok], 400);
            }
            $data->kode_praktikum = $request->kode_praktikum;
            $data->hari = $request->hari;
            $data->jam_mulai = $request->jam_mulai;
            $data->jam_berakhir = $this->calculateEndTime($request->jam_mulai, $request->durasi);
            $data->ruang = $request->ruang;
            $data->durasi = $request->durasi;
            $data->save();
        
            $jumlahPertemuan = 0;
        
            if ($request->durasi == 200) {
                $jumlahPertemuan = 14;
            } elseif ($request->durasi == 160) {
                $jumlahPertemuan = 16;
            } elseif ($request->durasi == 170) {
                $jumlahPertemuan = 15;
            }
        
            if ($jumlahPertemuan > 0) {
                $praktikum = Praktikum::where('kode_praktikum', $request->kode_praktikum)->first();
                if ($praktikum) {
                    $praktikum->jumlah_week = $jumlahPertemuan;
                    $praktikum->save();
                }
            }
        } else {
            // // Memeriksa apakah kode_matakuliah dan kelas sudah ada di database
            $existingData = Jadwal::where('kode_praktikum', $request->kode_praktikum)
                ->first();
    
            if ($existingData) {
                return response()->json(['error' => 'Jadwal Praktikum sudah ada'], 422);
            }
    
            $cekBentrok = $this->cekJadwalBentrok($jamMulai, $jamBerakhir, $hari, $ruang);
            if ($cekBentrok) {
                return response()->json(['error' => $cekBentrok], 400);
            }
            
            Jadwal::create([
                'kode_jadwal' => $kodeJadwal,
                'kode_praktikum' => $request->kode_praktikum,
                'hari' => $request->hari,
                'jam_mulai' => $request->jam_mulai,
                'jam_berakhir' => $jamBerakhir,
                'ruang' => $request->ruang,
                'durasi' => $request->durasi,
            ]);
    
            $jumlahPertemuan = 0;
    
            if ($request->durasi == 200) {
                $jumlahPertemuan = 14;
            } elseif ($request->durasi == 160) {
                $jumlahPertemuan = 16;
            } elseif ($request->durasi == 170) {
                $jumlahPertemuan = 15;
            }
    
            if ($jumlahPertemuan > 0) {
                $praktikum = Praktikum::where('kode_praktikum', $request->kode_praktikum)->first();
                if ($praktikum) {
                    $praktikum->jumlah_week = $jumlahPertemuan;
                    $praktikum->save();
                }
            }
        }
    
        return response()->json(['success' => 'Data saved successfully.']);
    }
    

    public function edit($id)
    {
        $jadwal = Jadwal::find($id);

        return response()->json($jadwal);
    }

    public function destroy($id)
    {
        Jadwal::find($id)->delete();
        return response()->json(['success' => 'Data deleted successfully.']);
    }

    public static function generateJadwalId()
    {
        $lastJadwal = Jadwal::latest('kode_jadwal')->first();

        if (!$lastJadwal) {
            return 'JDW-001'; // If no previous supplier exists, start with SUP-001
        }

        $lastId = intval(substr($lastJadwal->kode_jadwal, 4)); // Extract the numeric portion of the last ID
        $newId = $lastId + 1;
        $paddedNewId = str_pad($newId, 3, '0', STR_PAD_LEFT); // Pad the new ID with leading zeros if necessary
        $generatedId = 'JDW-' . $paddedNewId;

        return $generatedId;
    }

    public function calculateEndTime($startTime, $duration)
    {
        $startTime = Carbon::createFromFormat('H:i', $startTime);
        $endTime = $startTime->copy()->addMinutes($duration);
        return $endTime->format('H:i');
    }

    public function cekJadwalBentrok($jamMulai, $jamBerakhir, $hari, $ruang, $id = null)
    {
        $query = Jadwal::where('hari', $hari)
            ->where('ruang', $ruang)
            ->where(function ($query) use ($jamMulai, $jamBerakhir) {
                $query->where(function ($subQuery) use ($jamMulai, $jamBerakhir) {
                    $subQuery->where('jam_mulai', '<', $jamBerakhir)
                        ->where('jam_berakhir', '>', $jamMulai);
                })
                    ->orWhere(function ($subQuery) use ($jamMulai, $jamBerakhir) {
                        $subQuery->where('jam_mulai', '>=', $jamMulai)
                            ->where('jam_mulai', '<', $jamBerakhir);
                    })
                    ->orWhere(function ($subQuery) use ($jamMulai, $jamBerakhir) {
                        $subQuery->where('jam_berakhir', '>', $jamMulai)
                            ->where('jam_berakhir', '<=', $jamBerakhir);
                    });
            });

        if ($id) {
            $query->where('id', '!=', $id);
        }

        $existingJadwal = $query->first();

        if ($existingJadwal) {
            return "Jadwal bentrok dengan jadwal yang sudah ada.";
        }

        return null; // Tidak ada bentrok
    }
}
