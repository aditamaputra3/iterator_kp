<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AsistenLab;
use App\Models\User;
use App\Models\Mahasiswa;
use App\Models\Praktikum;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class MahasiswaController extends Controller
{
    public function index(Request $request)
    {
        $user = User::where('peran_pengguna', 'MAHASISWA')->pluck('nama_lengkap', 'id')->toArray();
        $praktikum = Praktikum::join('matakuliah', 'praktikum.kode_matakuliah', '=', 'matakuliah.kode_matakuliah')
            ->select('praktikum.kode_praktikum', 'praktikum.kode_matakuliah', 'praktikum.kelas', 'matakuliah.nama_matakuliah')
            ->get()
            ->toArray();
        if ($request->ajax()) {
            $data = Mahasiswa::with(['users','praktikum.matakuliah'])->latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('nama_lengkap', function ($row) {
                    return $row->users->nama_lengkap ?? '';
                })

                ->addColumn('kode_matakuliah', function ($row) {
                    return $row->praktikum->kode_matakuliah ?? '';
                })

                ->addColumn('kelas', function ($row) {
                    return $row->praktikum->kelas ?? '';
                })

                ->addColumn('nama_matakuliah', function ($row) {
                    return $row->praktikum->matakuliah->nama_matakuliah ?? '';
                })
                ->addColumn('aksi', function ($row) {
                    $btn = '<a href="javascript:void(0)" data-id="' . $row->id . '" class="edit btn btn-warning btn-sm editData"><i class="fa fa-edit"></i>&nbsp;Edit</a>';
                    $btn .= ' <a href="javascript:void(0)" data-id="' . $row->id . '" class="btn btn-danger btn-sm deleteData" data-url="' . route('mahasiswa.store') . '"><i class="fa fa-trash"></i>&nbsp;Delete</a>';
                    return $btn;
                })
                ->rawColumns(['nama_lengkap','kode_matakuliah', 'nama_matakuliah', 'kelas', 'aksi'])
                ->make(true);
        }

        return view(
            'mahasiswa.list',
            [
                "user" => $user,
                "praktikum" => $praktikum
            ]
        );
    }



    public function store(Request $request)
    {
        if (!empty($request->id)) {
            $data = Mahasiswa::find($request->id);
            $data->nrp = $request->nrp;
            $data->id_user = $request->id_user;
            $data->kode_praktikum = $request->kode_praktikum;
            $data->save();
        } else {
           
            Mahasiswa::create([
                'nrp' => $request->nrp,
                'id_user' => $request->id_user,
                'kode_praktikum' => $request->kode_praktikum,
            ]);
        }

        return response()->json(['success' => 'Data saved successfully.']);
    }

    public function edit($id)
    {
        $aslab = Mahasiswa::find($id);
        return response()->json($aslab);
    }

    public function destroy($id)
    {
        Mahasiswa::find($id)->delete();
        return response()->json(['success' => 'Data deleted successfully.']);
    }

}
