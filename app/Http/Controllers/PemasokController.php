<?php

namespace App\Http\Controllers;

use App\Models\Pemasok;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class PemasokController extends Controller
{

    public function index(Request $request)
    {
        $data = Pemasok::all();
        if ($request->ajax()) {
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('aksi', function ($row) {
                    $btn = '<a href="javascript:void(0)" data-id="' . $row->id . '" class="edit btn btn-warning btn-sm editData"><i class="fa fa-edit"></i>&nbsp;Edit</a>';

                    $btn .= ' <a href="javascript:void(0)" data-id="' . $row->id . '" class="btn btn-danger btn-sm deleteData" data-url="' . route('pemasok.store') . '"><i class="fa fa-trash"></i>&nbsp;Delete</a>';

                    return $btn;
                })
                ->rawColumns(['aksi'])
                ->make(true);
        }

        return view('pemasok.list');
    }

    public function store(Request $request)
    {
        if (!empty($request->id)) {
            $data = Pemasok::find($request->id);
            $data->nama_pemasok = $request->nama_pemasok;
            $data->alamat_pemasok = $request->alamat_pemasok;
            $data->no_telp_pemasok = $request->no_telp_pemasok;
            $data->save();
        } else {
            Pemasok::create([
                'nama_pemasok' => $request->nama_pemasok,
                'alamat_pemasok' => $request->alamat_pemasok,
                'no_telp_pemasok' => $request->no_telp_pemasok,
            ]);
        }

        return response()->json(['success' => 'Data saved successfully.']);
    }

    public function edit($id)
    {
        $pemasok = Pemasok::find($id);
        return response()->json($pemasok);
    }

    public function destroy($id)
    {
        Pemasok::find($id)->delete();
        return response()->json(['success' => 'Data deleted successfully.']);
    }
}
