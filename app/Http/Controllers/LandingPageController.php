<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Jadwal;
use App\Models\Praktikum;

class LandingPageController extends Controller
{
	public function index()
	{
		// Mengatur zona waktu khusus (Asia/Jakarta)
		$now = Carbon::now('Asia/Jakarta');

		// Mendapatkan hari dalam bahasa Indonesia
		$dayInIndonesian = [
			'Sunday'    => 'Minggu',
			'Monday'    => 'Senin',
			'Tuesday'   => 'Selasa',
			'Wednesday' => 'Rabu',
			'Thursday'  => 'Kamis',
			'Friday'    => 'Jumat',
			'Saturday'  => 'Sabtu',
		][$now->englishDayOfWeek];

		// Ambil praktikum berdasarkan hari
		$praktikumHariIni = Praktikum::join('matakuliah', 'praktikum.kode_matakuliah', '=', 'matakuliah.kode_matakuliah')
			->join('jadwal', 'praktikum.kode_praktikum', '=', 'jadwal.kode_praktikum')
			->select(
				'praktikum.kode_praktikum',
				'praktikum.kode_matakuliah',
				'praktikum.kelas',
				'matakuliah.nama_matakuliah',
				'jadwal.jam_mulai',
				'jadwal.jam_berakhir',
				'jadwal.hari',
				'jadwal.ruang'
			)
			->where('hari',$dayInIndonesian)
			->orderBy('jadwal.jam_mulai', 'ASC')
			->get();
		return view('landingpage.index', compact('praktikumHariIni'));
	}

	public function about()
	{
		return view('landingpage.about');
	}

	public function contact()
	{
		return view('landingpage.contact');
	}
}
