<?php

namespace App\Http\Controllers;

use App\Models\Dosen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class DosenController extends Controller
{
    public function index(Request $request)
    {
        $data = Dosen::all();
        if ($request->ajax()) {
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('aksi', function ($row) {
                    $btn = '<a href="javascript:void(0)" data-id="' . $row->id . '" class="edit btn btn-warning btn-sm editData"><i class="fa fa-edit"></i>&nbsp;Edit</a>';

                    $btn .= ' <a href="javascript:void(0)" data-id="' . $row->id . '" class="btn btn-danger btn-sm deleteData" data-url="' . route('dosen.store') . '"><i class="fa fa-trash"></i>&nbsp;Delete</a>';

                    return $btn;
                })
                ->rawColumns(['aksi'])
                ->make(true);
        }

        return view('dosen.list');
    }

    public function store(Request $request)
    {
        if (!empty($request->id)) {
            $data = Dosen::find($request->id);
            $data->nip = $request->nip;
            $data->nama_dosen = $request->nama_dosen;
            $data->email = $request->email;
            $data->save();
        } else {
            Dosen::create([
                'nip' => $request->nip,
                'nama_dosen' => $request->nama_dosen,
                'email' => $request->email,
            ]);
        }

        return response()->json(['success' => 'Data saved successfully.']);
    }

    public function edit($id)
    {
        $dosen = Dosen::find($id);
        return response()->json($dosen);
    }

    public function destroy($id)
    {
        Dosen::find($id)->delete();
        return response()->json(['success' => 'Data deleted successfully.']);
    }
}
