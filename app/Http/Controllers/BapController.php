<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Bap;
use App\Models\AsistenLab;
use App\Models\Praktikum;
use App\Models\AsistenPraktikum;
use App\Models\Mahasiswa;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;


class BapController extends Controller
{
    public function index(Request $request)
    {
        $userId = Auth::id();
        $role = auth()->user()->peran_pengguna;

        if ($userId && $role == 'ASISTEN') {
            $kodePraktikum = AsistenLab::join('users', 'asisten_lab.id_user', '=', 'users.id')
                ->join('asisten_praktikum', 'asisten_lab.nrp', '=', 'asisten_praktikum.nrp')
                ->join('praktikum', 'asisten_praktikum.kode_praktikum', '=', 'praktikum.kode_praktikum')
                ->join('matakuliah', 'praktikum.kode_matakuliah', '=', 'matakuliah.kode_matakuliah')
                ->select('praktikum.kode_praktikum', 'matakuliah.kode_matakuliah', 'matakuliah.nama_matakuliah', 'praktikum.kelas')
                ->where('asisten_lab.id_user', $userId)
                ->get();
        } elseif ($role == 'KEPALA' || $role == 'TEKNISI' || $role == 'KOORDINATOR') {
            $kodePraktikum = Praktikum::join('matakuliah', 'praktikum.kode_matakuliah', '=', 'matakuliah.kode_matakuliah')
                ->select('praktikum.kode_praktikum', 'matakuliah.kode_matakuliah', 'matakuliah.nama_matakuliah', 'praktikum.kelas')
                ->get();
        }elseif($role == 'MAHASISWA'){
            $kodePraktikum = Mahasiswa::join('users', 'mahasiswa.id_user', '=', 'users.id')
            ->join('praktikum', 'mahasiswa.kode_praktikum', '=', 'praktikum.kode_praktikum')
            ->join('matakuliah', 'praktikum.kode_matakuliah', '=', 'matakuliah.kode_matakuliah')
            ->select('praktikum.kode_praktikum', 'matakuliah.kode_matakuliah', 'matakuliah.nama_matakuliah', 'praktikum.kelas')
            ->where('mahasiswa.id_user', $userId)
            ->get();

        }
        
        return view('bap.list', compact('kodePraktikum'));
    }


    public function list(Request $request, $encryptedKodePraktikum)
    {
        $kode_praktikum = decrypt($encryptedKodePraktikum);
        $carbonUTC = Carbon::now();
        $tanggalSekarang= $carbonUTC->tz('Asia/Jakarta');
        $praktikum = Praktikum::join('matakuliah', 'praktikum.kode_matakuliah', '=', 'matakuliah.kode_matakuliah')
            ->where('praktikum.kode_praktikum', $kode_praktikum)
            ->select('praktikum.kode_praktikum', 'praktikum.kode_matakuliah', 'praktikum.kelas', 'matakuliah.nama_matakuliah')
            ->first();

        $Asisten = AsistenLab::join('users', 'asisten_lab.id_user', '=', 'users.id')
            ->join('asisten_praktikum', 'asisten_lab.nrp', '=', 'asisten_praktikum.nrp')
            ->join('praktikum', 'asisten_praktikum.kode_praktikum', '=', 'praktikum.kode_praktikum')
            ->join('matakuliah', 'praktikum.kode_matakuliah', '=', 'matakuliah.kode_matakuliah')
            ->where('praktikum.kode_praktikum', $kode_praktikum)
            ->select('asisten_praktikum.nrp', 'users.nama_lengkap')
            ->get();
        $nrpAsisten = $Asisten->pluck('nrp', 'nama_pengguna')->toArray();
        // NRP pengguna yang sedang login
        $user = auth()->user();
        $nrpPengguna = AsistenLab::where('id_user', $user->id)->value('nrp');
        $statusPengguna = AsistenLab::join('users', 'asisten_lab.id_user', '=', 'users.id')
            ->join('asisten_praktikum', 'asisten_lab.nrp', '=', 'asisten_praktikum.nrp')
            ->join('praktikum', 'asisten_praktikum.kode_praktikum', '=', 'praktikum.kode_praktikum')
            ->join('bap', 'praktikum.kode_praktikum', '=', 'bap.kode_praktikum')
            ->where('asisten_lab.id_user', $user->id)
            ->select('bap.asisten1', 'bap.asisten2', 'bap.asisten3')
            ->first();

            $bap = Bap::where('kode_praktikum', $kode_praktikum)->first();
        

        if ($request->ajax()) {
            $data = Bap::where('kode_praktikum', $kode_praktikum)
                ->latest()
                ->get();

            return datatables()
                ->of($data)
                ->addIndexColumn()

                ->addColumn('validasi', function ($row) {
                    if (auth()->user()->peran_pengguna == 'ASISTEN') {
                        $buttonOrLabel = ''; // Inisialisasi tombol atau label
                        $user = auth()->user(); // id_user pengguna yang sedang login
                        $nrpPengguna = AsistenLab::join('users', 'asisten_lab.id_user', '=', 'users.id')
                            ->join('asisten_praktikum', 'asisten_lab.nrp', '=', 'asisten_praktikum.nrp')
                            ->where('asisten_lab.id_user', $user->id)
                            ->value('asisten_praktikum.nrp');

                        $asistenValues = [$row->asisten1, $row->asisten2, $row->asisten3];

                        // Memeriksa apakah $nrpPengguna ada dalam daftar nilai kolom asisten
                        if (in_array($nrpPengguna, $asistenValues)) {
                            // Menentukan nomor asisten berdasarkan indeks nilai yang sesuai
                            $nomorAsisten = array_search($nrpPengguna, $asistenValues) + 1;

                            // Membuat tombol validasi sesuai dengan nomor asisten
                            $buttonOrLabel = '<a href="#" class="btn btn-primary btn-sm validateAsisten" data-id="' . $row->id . '" data-column="asisten' . $nomorAsisten . '">Konfirmasi BAP ' . $nomorAsisten . '</a>';
                        } else{
                            $buttonOrLabel = '<span class="badge bg-success">Hadir</span>';
                          
                        }

                        return $buttonOrLabel;
                    } elseif (auth()->user()->peran_pengguna == 'KEPALA' || auth()->user()->peran_pengguna == 'TEKNISI' || auth()->user()->peran_pengguna == 'KOORDINATOR') {
                      
                            // return $row->bap->asisten1 ?? '';
                            // return $row->bap->asisten2 ?? '';
                            // return $row->bap->asisten3 ?? '';
                    }elseif(auth()->user()->peran_pengguna == 'MAHASISWA'){
                       
                    }
                })
                ->addColumn('validasiMhs', function ($row) {
                    $data = $row->mahasiswa;
                    if ($data === 'HADIR') {
                        $btn = '<span class="badge bg-success">HADIR</span>';
                    } else if ($data === 'TIDAK HADIR') {
                        $btn = '<span class="badge bg-danger">TIDAK HADIR</span>';
                    } else {
                        $btn = '<a href="#" class="btn btn-primary btn-sm validateMahasiswa" data-id="' . $row->id . '"><i class="fa fa-check"></i>&nbsp;Konfirmasi BAP</a>';
                    }
                   

                    return $btn;
                })
                ->addColumn('aksi', function ($row) {
                    $btn = '<a href="javascript:void(0)" data-id="' . $row->id . '" class="edit btn btn-warning mr-2 btn-sm editData"><i class="fa fa-edit"></i>&nbsp;Edit</a>';
                    $btn .= '<a href="#" class="btn btn-danger btn-sm deleteData" data-id="' . $row->id . '"><i class="fa fa-trash"></i>&nbsp;Delete</a>';

                    return $btn;
                })
                ->rawColumns(['validasi', 'validasiMhs', 'aksi'])
                ->make(true);
        }

        return view('bap.list-topik', [
            "praktikum" => $praktikum,
            'kode_praktikum' => $kode_praktikum,
            'nrpAsisten' => $nrpAsisten,
            'nrpPengguna' => $nrpPengguna,
            'Asisten' => $Asisten,
            'statusPengguna' => $statusPengguna,
            'tanggalSekarang' => $tanggalSekarang,
            'bap' => $bap
        ]);
    }


    public function store(Request $request)
    {
        if (!empty($request->id)) {
            $data = Bap::find($request->id);
            // $data->kode_praktikum = $request->kode_praktikum;

            if (auth()->user()->peran_pengguna == 'ASISTEN') {
                $data->topik = $request->topik;
            } elseif (auth()->user()->peran_pengguna == 'KEPALA' || auth()->user()->peran_pengguna == 'TEKNISI' || auth()->user()->peran_pengguna == 'KOORDINATOR') {
                $data->topik = $request->topik;
                $data->asisten1 = $request->asisten1;
                $data->asisten2 = $request->asisten2;
                $data->asisten3 = $request->asisten3;
                $data->mahasiswa = $request->mahasiswa;
            }
           

            $data->save();
        } else {
        
        // Ambil daftar asisten yang terdaftar pada kode_praktikum tertentu
        $asisten = AsistenPraktikum::where('kode_praktikum', $request->kode_praktikum)->get();

        if ($asisten->isEmpty()) {
            return response()->json(['error' => 'Tidak cukup asisten terdaftar pada kelas praktikum ini'], 422);
        }

        // Mengisi kolom asisten1, asisten2, dan asisten3 dengan NRP asisten yang terdaftar
        $asistenCount = count($asisten);
        $tanggalSekarang = Carbon::now('Asia/Jakarta')->format('Y-m-d');
       
        $bapData = [
            'kode_praktikum' => $request->kode_praktikum,
            'tanggal' => $tanggalSekarang,
            'topik' => $request->topik,
        ];

        if ($asistenCount >= 1) {
            $bapData['asisten1'] = $asisten[0]->nrp;
        }

        if ($asistenCount >= 2) {
            $bapData['asisten2'] = $asisten[1]->nrp;
        }

        if ($asistenCount >= 3) {
            $bapData['asisten3'] = $asisten[2]->nrp;
        }

        Bap::create($bapData);
    }

        return response()->json(['success' => 'Data stored successfully.']);
    
    }

    public function edit($id)
    {
        $bap = Bap::find($id);
        return response()->json($bap);
    }

    public function destroy($id)
    {
        Bap::find($id)->delete();
        return response()->json(['success' => 'Data deleted successfully.']);
    }

    public function validateBap(Request $request)
    {
        $id = $request->input('id');
        $column = $request->input('column');

        // Retrieve the BAP record by ID
        $bap = Bap::find($id);

        if (!$bap) {
            return response()->json(['error' => 'BAP record not found'], 404);
        }

        // Perform the validation logic based on the column name
        switch ($column) {
            case 'asisten1':
                // Validation logic for asisten1 column
                $bap->asisten1 = 'HADIR';
                break;
            case 'asisten2':
                // Validation logic for asisten2 column
                $bap->asisten2 = 'HADIR';
                break;
            case 'asisten3':
                // Validation logic for asisten3 column
                $bap->asisten3 = 'HADIR';
                break;
            default:
                return response()->json(['error' => 'Invalid column name'], 400);
        }

        // Save the updated BAP record
        $bap->save();

        return response()->json(['success' => 'Data validated successfully']);
    }

    public function validateMahasiswa(Request $request)
    {
        try {
            $id = $request->input('id');
            $bap = Bap::find($id);

            if (!$bap) {
                return response()->json(['error' => 'Data BAP tidak ditemukan'], 404);
            }

            // Lakukan validasi mahasiswa di sini, misalnya mengubah kolom "mahasiswa" menjadi "Valid" atau "Tervalidasi"
            $bap->mahasiswa = 'HADIR'; // Ubah sesuai dengan logika validasi Anda

            // Simpan perubahan
            $bap->save();

            return response()->json(['success' => 'Validasi berhasil']);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Terjadi kesalahan saat validasi mahasiswa'], 500);
        }
    }

    public function getCustomPdfData(Request $request)
    {
        // Retrieve the encrypted kode_praktikum from the request
        $encryptedKodePraktikum = $request->input('kode_praktikum');
        
        // Decrypt the kode_praktikum (You should have the decryption logic)
        $kodePraktikum = decrypt($encryptedKodePraktikum);
        
        // Customize your data retrieval logic here based on $kodePraktikum
        // Fetch the data for PDF export
        $data = Bap::where('kode_praktikum', $kodePraktikum)
        ->latest()
        ->get();
        // For example, you can use DataTables to handle data retrieval
        return DataTables::of($data)
            ->addColumn('tanggal', function ($row) {
                // Customize any additional columns here if needed
                return $row->tanggal;
            })
            ->addColumn('topik', function ($row) {
                // Customize any additional columns here if needed
                return $row->topik;
            })
            ->addColumn('asisten1', function ($row) {
                // Customize any additional columns here if needed
                return $row->asisten1;
            })
            ->addColumn('asisten2', function ($row) {
                // Customize any additional columns here if needed
                return $row->asisten2;
            })
            ->addColumn('asisten3', function ($row) {
                // Customize any additional columns here if needed
                return $row->asisten3;
            })
            ->addColumn('mahasiswa', function ($row) {
                // Customize any additional columns here if needed
                return $row->asisten3;
            })
            ->make(true);
    }
}
