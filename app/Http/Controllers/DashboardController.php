<?php

namespace App\Http\Controllers;

use App\Models\AsistenLab;
use App\Models\Dosen;
use App\Models\Pemasok;
use App\Models\PengajuanJadwal;
use App\Models\Praktikum;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class DashboardController extends Controller
{
	public function index(){
		$Praktikum = Praktikum::count();
    $Asisten = AsistenLab::count();
    $Pengajuan= PengajuanJadwal::where('status_pengajuan', '=', 'menunggu')->count();
    $Koordinator = Dosen::count();
    
	return view('dashboard.index', compact('Praktikum', 'Asisten', 'Pengajuan','Koordinator'));
	}
}
