<?php

namespace App\Http\Controllers;

use App\Models\AsistenLab;
use App\Models\AsistenPraktikum;
use App\Models\Praktikum;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str; // Import class Str
use Yajra\DataTables\Contracts\DataTable;
use Yajra\DataTables\Facades\DataTables;

class AsistenList extends Controller
{
    public function index(Request $request)
    {
        $data = DB::table('asisten_praktikum')
            ->join('asisten_lab', 'asisten_praktikum.nrp', '=', 'asisten_lab.nrp')
            ->join('users', 'asisten_lab.id_user', '=', 'users.id')
            ->join('praktikum', 'asisten_praktikum.kode_praktikum', '=', 'praktikum.kode_praktikum')
            ->join('matakuliah', 'praktikum.kode_matakuliah', '=', 'matakuliah.kode_matakuliah')
            ->select('asisten_lab.nrp', 'users.nama_lengkap', 'praktikum.kode_praktikum', 'praktikum.kode_matakuliah', 'praktikum.kelas', 'matakuliah.nama_matakuliah', 'matakuliah.tahun_ajaran')
            ->get();

        // Mengelompokkan data berdasarkan 'nama_matakuliah' dan 'kelas'
        $groupedData = $data->groupBy(['kode_praktikum']);

        // Memotong nama_matakuliah jika terlalu panjang
        foreach ($groupedData as $kodePraktikum => $group) {
            foreach ($group as $item) {
                $item->nama_matakuliah = Str::limit($item->nama_matakuliah, 20); // Ubah angka 20 sesuai kebutuhan
            }
        }

        return view('praktikum.asisten-list', [
            "groupedData" => $groupedData,
        ]);
    }
}
