<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\KategoryBarang;
use App\Models\Pemasok;
use App\Models\DetailKondisi;
use App\Models\Kondisi;
use App\Models\MasterBarang;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Storage;

class InventoryController extends Controller
{
	public function index(Request $request){
                $id_kategori = $request->query()["kategori"];
                $kategori = KategoryBarang::where('id', $id_kategori)->first();
                $data = MasterBarang::where('id_kategori_barang', $id_kategori)
                ->selectRaw('(SELECT count(*) FROM kondisi WHERE id_master_barang = master_barang.id) as kuantitas, master_barang.*')
                ->get();
                if ($request->ajax()) {
                    return Datatables::of($data)
                        ->addIndexColumn()
                        ->addColumn('aksi', function ($row) {
                            $btn = '<a href="'.url("detail-barang/$row->id").'" class="btn btn-info btn-sm"><i class="fa fa-eye"></i>&nbsp;Detail</a>&nbsp;';

                            $btn .= '<a href="'.url("edit-barang/$row->id").'" class="edit btn btn-success btn-sm"><i class="fa fa-edit"></i>&nbsp;Edit</a>&nbsp;';
        
                            $btn .= '<a href="javascript:void(0)" data-id="' . $row->id . '" class="btn btn-danger btn-sm deleteData"><i class="fa fa-trash"></i>&nbsp;Delete</a>';

        
                            return $btn;
                        })
                        ->rawColumns(['aksi'])
                        ->make(true);
                }
                return view('inventory.list', ["data" => $data, "kategori" => $kategori]);
	}

    public function add(Request $request) {
                $kategori = null;
                $options_pemasok = Pemasok::pluck('nama_pemasok', 'id')->toArray();
                $options_kondisi = DetailKondisi::pluck('nama_kondisi', 'id')->toArray();
                
                if (isset($request->query()["kategori"]) && array_key_exists("kategori", $request->query())) {
                    $id_kategori = $request->query()["kategori"];
                    $kategori = KategoryBarang::where('id', $id_kategori)->first();
                }
                return view(
                    'inventory.pages.create',
                    [
                        "kategori" => $kategori,
                        "options_kondisi" => $options_kondisi,
                        "options_pemasok" => $options_pemasok,
                    ]
                );
            }


    public function edit(string $id) {
                $kategori = null;
                $options_pemasok = Pemasok::pluck('nama_pemasok', 'id')->toArray();
                $options_kondisi = DetailKondisi::pluck('nama_kondisi', 'id')->toArray();
                $data_master_barang = MasterBarang::select(
                    "id",
                    "id_kategori_barang",
                    "id_pemasok",
                    "nama_barang",
                    "deskripsi_barang",
                    "harga",
                    "spesifikasi",
                    "merk",
                    \DB::raw("CONCAT(YEAR(tahun_masuk), '-', LPAD(MONTH(tahun_masuk), 2, '0')) AS tahun_masuk"),
                    "gambar_barang"
                )->find($id);
                $kategori = KategoryBarang::where('id', $data_master_barang->id_kategori_barang)->first();
                
                return view(
                    'inventory.pages.edit',
                    [
                        "master_barang" => $data_master_barang,
                        "kategori" => $kategori,
                        "options_kondisi" => $options_kondisi,
                        "options_pemasok" => $options_pemasok,
                    ]
                );
            }
        

    public function save(Request $request)
    {
        $gambar_barang = $request->file("gambar_barang");
        $kuantitas = (int) $request->kuantitas ?? 0;

        if ($request->hasFile('gambar_barang') && $request->file('gambar_barang')->isValid()) {
            $folderPath = 'images/uploads';
            if (!Storage::exists($folderPath)) {
                Storage::makeDirectory($folderPath);
            }
            $gambar_path = $request->file('gambar_barang')->store($folderPath, 'public');
        } else {
            $gambar_path = null;
        }

        if (!empty($request->id)) {
            $data = MasterBarang::find($request->id);
            $data->id_kategori_barang = $request->id_kategori_barang;
            $data->id_pemasok = $request->id_pemasok;
            $data->nama_barang = $request->nama_barang;
            $data->deskripsi_barang = $request->deskripsi_barang;
            $data->harga = $request->harga;
            $data->spesifikasi = $request->spesifikasi;
            $data->merk = $request->merk;
            $data->tahun_masuk = $request->tahun_masuk.'-1';
            if (!empty($gambar_path)) $data->gambar_barang = $gambar_path;

            $data->save();
        } else {
            $master_barang = MasterBarang::create([
                'id_kategori_barang' => $request->id_kategori_barang,
                'id_pemasok' => $request->id_pemasok,
                'nama_barang' => $request->nama_barang,
                'deskripsi_barang' => $request->deskripsi_barang,
                'harga' => $request->harga,
                'spesifikasi' => $request->spesifikasi,
                'merk' => $request->merk,
                'tahun_masuk' => $request->tahun_masuk.'-1',
                'gambar_barang' => $gambar_path
            ]);

            $master_barang_id = $master_barang->id;

            if ($kuantitas > 0) {
                $data_bulk = [];
                
                for ($i = 0; $i < $kuantitas; $i++) {
                    $data_bulk[] = [
                        "id_master_barang" => $master_barang_id,
                        "id_detail_kondisi" => $request->id_detail_kondisi,
                        "status" => $request->status,
                    ];
                }
    
                Kondisi::insert($data_bulk);
            }
        }

        return response()->json(['success' => 'Data saved successfully.']);
    }
    
    public function delete($id)
    {
        Kondisi::where('id_master_barang', $id)->delete();
        MasterBarang::find($id)->delete();
        return response()->json(['success'=>'Data deleted successfully.']);
    }

    public function detail(string $id) {
                $kategori = null;
                $options_pemasok = Pemasok::pluck('nama_pemasok', 'id')->toArray();
                $options_kondisi = DetailKondisi::pluck('nama_kondisi', 'id')->toArray();
                $data_master_barang = MasterBarang::select(
                    "master_barang.id",
                    "master_barang.id_kategori_barang",
                    "kategory_barang.nama_kategori_barang",
                    "pemasok.nama_pemasok",
                    "master_barang.id_pemasok",
                    "master_barang.nama_barang",
                    "master_barang.deskripsi_barang",
                    "master_barang.harga",
                    "master_barang.spesifikasi",
                    "master_barang.merk",
                    \DB::raw("CONCAT(YEAR(master_barang.tahun_masuk), '-', LPAD(MONTH(master_barang.tahun_masuk), 2, '0')) AS tahun_masuk"),
                    \DB::raw("(SELECT count(*) FROM kondisi WHERE kondisi.id_master_barang = master_barang.id) as kuantitas"),
                    "gambar_barang"
                )
                ->leftJoin('kategory_barang', 'master_barang.id_kategori_barang', '=', 'kategory_barang.id')
                ->leftJoin('pemasok', 'master_barang.id_pemasok', '=', 'pemasok.id')
                ->find($id);

                $data_kondisi = Kondisi::select('kondisi.id', 'kondisi.status', 'detail_kondisi.nama_kondisi', \DB::raw('COUNT(detail_kondisi.id) as kuantitas'))
                ->leftJoin('detail_kondisi', 'kondisi.id_detail_kondisi', '=', 'detail_kondisi.id')
                ->where('kondisi.id_master_barang', $id)
                ->groupBy('detail_kondisi.id')
                ->get();
            
                return view(
                    'inventory.pages.detail',
                    [
                        "master_barang" => $data_master_barang,
                        "data_kondisi" => $data_kondisi,
                        "options_kondisi" => $options_kondisi,
                    ]
                );
            }

    public function detail_kondisi(Request $request, string $id) {        
        $data = Kondisi::select('kondisi.id', 'kondisi.status', 'kondisi.id_detail_kondisi', 'detail_kondisi.nama_kondisi', 'detail_kondisi.keterangan_kondisi', \DB::raw('COUNT(detail_kondisi.id) as kuantitas'))
        ->leftJoin('detail_kondisi', 'kondisi.id_detail_kondisi', '=', 'detail_kondisi.id')
        ->where('kondisi.id_master_barang', $id)
        ->groupBy('detail_kondisi.id', 'kondisi.status')
        ->get();

        if ($request->ajax()) {
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('aksi', function ($row) {
                    // $btn = '<a href="'.url("edit-barang/$row->id").'" class="edit btn btn-success btn-sm"><i class="fa fa-edit"></i>&nbsp;Edit</a>&nbsp;';

                    $btn = '<a href="javascript:void(0)" data-id="' . $row->id_detail_kondisi . '" class="btn btn-danger btn-sm deleteData"><i class="fa fa-trash"></i>&nbsp;Delete</a>';

                    return $btn;
                })
                ->rawColumns(['aksi'])
                ->make(true);
        }
    }

    public function add_detail_kondisi(Request $request, string $id) {
        $kuantitas = (int) $request->kuantitas ?? 0;
        $data_bulk = [];
        for ($i = 0; $i < $kuantitas; $i++) {
            $data_bulk[] = [
                "id_master_barang" => $id,
                "id_detail_kondisi" => $request->id_detail_kondisi,
                "status" => $request->status,
            ];
        }

        Kondisi::insert($data_bulk);
        return response()->json(['success' => 'Data saved successfully.']);
    }

    public function delete_all_detail_kondisi(string $id_barang, string $id) {  
        Kondisi::where([
            ['id_master_barang', '=', $id_barang],
            ['id_detail_kondisi', '=', $id],
        ])->delete();
        return response()->json(['success'=>'Data deleted successfully.']);
    }
}
