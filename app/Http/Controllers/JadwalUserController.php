<?php

namespace App\Http\Controllers;

use App\Models\Matakuliah;
use App\Models\Jadwal;
use App\Models\Praktikum;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Str;
use Carbon\Carbon;

class JadwalUserController extends Controller
{
    public function index(Request $request)
    {

        // // Mengatur zona waktu khusus (Asia/Jakarta)
        // $now = Carbon::now('Asia/Jakarta');

        // // Mendapatkan hari dalam bahasa Indonesia
        // $dayInIndonesian = [
        //     'Sunday'    => 'Minggu',
        //     'Monday'    => 'Senin',
        //     'Tuesday'   => 'Selasa',
        //     'Wednesday' => 'Rabu',
        //     'Thursday'  => 'Kamis',
        //     'Friday'    => 'Jumat',
        //     'Saturday'  => 'Sabtu',
        // ][$now->englishDayOfWeek];

        // // // Tampilkan hasil pengaturan waktu dan hari
        // // dd($now, $dayInIndonesian);

        // // Ambil praktikum berdasarkan hari
        // $praktikumHariIni = Praktikum::join('matakuliah', 'praktikum.kode_matakuliah', '=', 'matakuliah.kode_matakuliah')
        //     ->join('jadwal', 'praktikum.kode_praktikum', '=', 'jadwal.kode_praktikum')
        //     ->select('praktikum.kode_praktikum', 'praktikum.kode_matakuliah', 'praktikum.kelas', 
        //     'matakuliah.nama_matakuliah', 'jadwal.jam_mulai', 'jadwal.hari', 'jadwal.ruang')
        //     ->get();
        if ($request->ajax()) {
            $data = Jadwal::with(['praktikum.matakuliah'])
                ->orderBy('hari', 'DESC') // Mengurutkan berdasarkan kolom 'hari' secara ascending (A-Z)
                ->latest()
                ->get();

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('kode_matakuliah', function ($row) {
                    return $row->praktikum->kode_matakuliah ?? '';
                })
                ->addColumn('kelas', function ($row) {
                    return $row->praktikum->kelas ?? '';
                })

                ->addColumn('nama_matakuliah', function ($row) {
                    return $row->praktikum->matakuliah->nama_matakuliah ?? '';
                })

                ->rawColumns(['kode_matakuliah', 'nama_matakuliah', 'kelas'])
                ->make(true);
        }

        return view('jadwal.list-for-user');
    }
}
