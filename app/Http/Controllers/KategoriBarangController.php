<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\KategoryBarang;
use App\Models\MasterBarang;
use Illuminate\Support\Facades\Session;

class KategoriBarangController extends Controller
{
    public function list_categories(Request $request)
    {
        $data_kategori = KategoryBarang::all();

        if ($request->ajax()) {
            return response()->json($data_kategori);
        }

        return view('inventory.categories');
    }

    public function index(Request $request)
    {
        $this->list_categories($request);
    }

    public function store(Request $request)
    {
        if (!empty($request->id)) {
            $data = KategoryBarang::find($request->id);
            $data->nama_kategori_barang = $request->nama_kategori_barang;
            $data->deskripsi_kategori_barang = $request->deskripsi_kategori_barang;
            $data->save();
        } else {
            KategoryBarang::create([
                'nama_kategori_barang' => $request->nama_kategori_barang,
                'deskripsi_kategori_barang' => $request->deskripsi_kategori_barang,
            ]);
        }

        return response()->json(['success' => 'Data saved successfully.']);
    }

    public function edit($id)
    {
        $data_kategori = KategoryBarang::find($id);
        return response()->json($data_kategori);
    }

    public function destroy($id)
    {
        KategoryBarang::find($id)->delete();
        return response()->json(['success' => 'Data deleted successfully.']);
    }
}
