<?php

namespace App\Http\Controllers;

use App\Models\Matakuliah;
use App\Models\Jadwal;
use App\Models\AsistenLab;
use App\Models\User;
use App\Models\PengajuanJadwal;
use App\Models\Praktikum;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Str;
use Carbon\Carbon;

class PengajuanJadwalController extends Controller
{
    // UserController.php

    public function getStatusAsisten()
    {
        // Mendapatkan id_user dari pengguna yang sudah login
        $userId = Auth::id();

        if ($userId) {
            $kodePraktikum = AsistenLab::join('users', 'asisten_lab.id_user', '=', 'users.id')
                ->join('asisten_praktikum', 'asisten_lab.nrp', '=', 'asisten_praktikum.nrp')
                ->join('praktikum', 'asisten_praktikum.kode_praktikum', '=', 'praktikum.kode_praktikum')
                ->join('matakuliah', 'praktikum.kode_matakuliah', '=', 'matakuliah.kode_matakuliah')
                ->select('asisten_praktikum.kode_praktikum', 'matakuliah.kode_matakuliah', 'matakuliah.nama_matakuliah', 'praktikum.kelas', 'users.nama_lengkap')
                ->where('asisten_lab.id_user', $userId)
                ->get();

            // if ($kodePraktikum) {
            //     $kodePraktikum = $kodePraktikum->kode_praktikum;
            //     // Lakukan sesuatu dengan $kodePraktikum
            // } else {
            //     return 'tidak ditemukan';
            // }

            // // Debugging: Tampilkan nilai $kodePraktikum
            // dd($kodePraktikum);
        } else {
            return 'tidak ditemukan';
        }

        // Kembalikan nilai $kodePraktikum jika diperlukan
        return $kodePraktikum;
    }


    public function index(Request $request)
    {
        $praktikum = Praktikum::join('matakuliah', 'praktikum.kode_matakuliah', '=', 'matakuliah.kode_matakuliah')
            ->select('praktikum.kode_praktikum', 'praktikum.kode_matakuliah', 'praktikum.kelas', 'matakuliah.nama_matakuliah')
            ->get()
            ->toArray();
        $existingKodePraktikum = PengajuanJadwal::pluck('kode_praktikum')->toArray();
        $id_user = Auth::id(); // Ambil ID akun yang terautentikasi
        $peran_pengguna = Auth::user()->peran_pengguna; // Ambil peran pengguna yang terautentikasi

        $kodePraktikum = $this->getStatusAsisten();
        if ($request->ajax()) {
            $data = PengajuanJadwal::with(['praktikum.matakuliah'])->latest();

            // Filter data sesuai peran_pengguna
            if ($peran_pengguna === 'ASISTEN') {
                $data = $data->where('id_user', $id_user);
            }

            $data = $data->get();
            // $data = PengajuanJadwal::with(['praktikum.matakuliah'])->latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('kode_matakuliah', function ($row) {
                    return $row->praktikum->kode_matakuliah ?? '';
                })
                ->addColumn('kelas', function ($row) {
                    return $row->praktikum->kelas ?? '';
                })

                ->addColumn('nama_matakuliah', function ($row) {
                    return $row->praktikum->matakuliah->nama_matakuliah ?? '';
                })

                ->addColumn('aksi', function ($row) {

                    $btn = ' <a href="javascript:void(0)" data-id="' . $row->id . '" class="btn btn-danger btn-sm deleteData" data-url="' . route('pengajuan-jadwal.store') . '"><i class="fa fa-trash"></i>&nbsp;Delete</a>';

                    // Cek peran pengguna ASISTEN
                    if (Auth::user()->peran_pengguna === 'ASISTEN') {
                        if ($row->status_pengajuan === 'diterima') {
                            // Jika status_pengajuan adalah 'diterima', kosongkan tombol delete
                            $btn = '';
                        }
                    }

                    // // Cek peran pengguna ASISTEN
                    // if (Auth::user()->peran_pengguna === 'ASISTEN' && $row->status_pengajuan !== 'diterima') {
                    //     // Jika status_pengajuan bukan 'diterima', tampilkan tombol delete
                    //     $btn .= ' <a href="javascript:void(0)" data-id="' . $row->id . '" class="btn btn-danger btn-sm deleteData" data-url="' . route('pengajuan-jadwal.store') . '"><i class="fa fa-trash"></i>&nbsp;Delete</a>';
                    // }

                    // Cek peran pengguna TEKNISI dan KEPALA
                    if (Auth::user()->peran_pengguna === 'TEKNISI' || Auth::user()->peran_pengguna === 'KEPALA') {
                        if ($row->status_pengajuan === 'menunggu') {
                            $btn .= ' <a href="javascript:void(0)" data-id="' . $row->id . '" class="btn btn-success btn-sm terimaPengajuan"><i class="fa fa-check"></i>&nbsp;Terima</a>';
                            $btn .= ' <a href="javascript:void(0)" data-id="' . $row->id . '" class="btn btn-danger btn-sm tolakPengajuan"><i class="fa fa-times"></i>&nbsp;Tolak</a>';
                        } else if ($row->status_pengajuan === 'diterima') {
                            // Cek apakah kode_praktikum sudah ada dalam tabel Jadwal
                            $existingJadwal = Jadwal::where('kode_praktikum', $row->kode_praktikum)->first();

                            if (!$existingJadwal) {
                                // $btn .= ' <a href="javascript:void(0)" data-id="' . $row->id . '" class="btn btn-success btn-sm tambahJadwal"><i class="fa fa-plus"></i>&nbsp;Tambah Ke Jadwal</a>';
                            }
                        }
                    }

                    return $btn;
                })
                ->rawColumns(['kode_matakuliah', 'nama_matakuliah', 'kelas', 'aksi'])
                ->make(true);
        }

        return view('jadwal.pengajuan-jadwal', ["praktikum" => $praktikum,  "kodePraktikum" => $kodePraktikum]);
    }

    public function store(Request $request)
    {
        $kodePengajuan = $this->generatePengajuanId();
        $hari = $request->hari;
        $ruang = $request->ruang;
        $jamMulai = $request->jam_mulai;
        $jamBerakhir = $this->calculateEndTime($jamMulai, $request->durasi);

        if (!empty($request->id)) {
            $data = PengajuanJadwal::find($request->id);
            $data->kode_praktikum = $request->kode_praktikum;
            $data->hari = $request->hari;
            $data->jam_mulai = $request->jam_mulai;
            $data->jam_berakhir = $this->calculateEndTime($request->jam_mulai, $request->durasi);
            $data->ruang = $request->ruang;
            $data->durasi = $request->durasi;
            $data->status_pengajuan = $request->status_pengajuan;
            $data->id_user = $request->id_user;
            $data->save();
            // $pesanError = $this->cekJadwalBentrok($jamMulai, $jamBerakhir, $hari);
            // if ($pesanError) {
            //     return response()->json(['error' => $pesanError], 400);
            // }
        } else {
            // Memeriksa apakah kode_praktikum sudah ada di tabel PengajuanJadwal
            $existingDataPengajuan = PengajuanJadwal::where('kode_praktikum', $request->kode_praktikum)
                ->orWhere('kode_praktikum', $request->kode_praktikum)
                ->first();

            // Memeriksa apakah kode_praktikum sudah ada di tabel Jadwal
            $existingDataJadwal = Jadwal::where('kode_praktikum', $request->kode_praktikum)
                ->orWhere('kode_praktikum', $request->kode_praktikum)
                ->first();

            if ($existingDataPengajuan || $existingDataJadwal) {
                return response()->json(['error' => 'Jadwal Praktikum sudah ada/Pengajuan Sudah ada'], 422);
            }

            $cekBentrok = $this->cekJadwalBentrok($jamMulai, $jamBerakhir, $hari, $ruang);
            if ($cekBentrok) {
                return response()->json(['error' => $cekBentrok], 400);
            }

            PengajuanJadwal::create([
                'kode_pengajuan' => $kodePengajuan,
                'kode_praktikum' => $request->kode_praktikum,
                'hari' => $request->hari,
                'jam_mulai' => $request->jam_mulai,
                'jam_berakhir' => $jamBerakhir,
                'ruang' => $request->ruang,
                'durasi' => $request->durasi,
                'id_user' => $request->id_user,
            ]);
        }
        return response()->json(['success' => 'Data saved successfully.']);
    }

    public function edit($id)
    {
        $pengajuan = PengajuanJadwal::find($id);

        return response()->json($pengajuan);
    }

    public function destroy($id)
    {
        PengajuanJadwal::find($id)->delete();
        return response()->json(['success' => 'Data deleted successfully.']);
    }

    protected function generatePengajuanId()
    {
        $lastPengajuan = PengajuanJadwal::latest('kode_pengajuan')->first();

        if (!$lastPengajuan) {
            return 'PNG-001'; // If no previous supplier exists, start with SUP-001
        }

        $lastId = intval(substr($lastPengajuan->kode_pengajuan, 4)); // Extract the numeric portion of the last ID
        $newId = $lastId + 1;
        $paddedNewId = str_pad($newId, 3, '0', STR_PAD_LEFT); // Pad the new ID with leading zeros if necessary
        $generatedId = 'PNG-' . $paddedNewId;

        return $generatedId;
    }

    public function calculateEndTime($startTime, $duration)
    {
        $startTime = Carbon::createFromFormat('H:i', $startTime);
        $endTime = $startTime->copy()->addMinutes($duration);
        return $endTime->format('H:i');
    }

    public function cekJadwalBentrok($jamMulai, $jamBerakhir, $hari, $ruang)
    {
        $existingJadwal = PengajuanJadwal::where('hari', $hari)
            ->where('ruang', $ruang)
            ->where(function ($query) use ($jamMulai, $jamBerakhir) {
                $query->whereBetween('jam_mulai', [$jamMulai, $jamBerakhir])
                    ->orWhereBetween('jam_berakhir', [$jamMulai, $jamBerakhir])
                    ->orWhere(function ($subQuery) use ($jamMulai, $jamBerakhir) {
                        $subQuery->where('jam_mulai', '<=', $jamMulai)
                            ->where('jam_berakhir', '>=', $jamBerakhir);
                    });
            })
            ->first();

        if ($existingJadwal) {
            return "Jadwal bentrok";
        }

        $existingJadwal = Jadwal::where('hari', $hari)
            ->where('ruang', $ruang)
            ->where(function ($query) use ($jamMulai, $jamBerakhir) {
                $query->whereBetween('jam_mulai', [$jamMulai, $jamBerakhir])
                    ->orWhereBetween('jam_berakhir', [$jamMulai, $jamBerakhir])
                    ->orWhere(function ($subQuery) use ($jamMulai, $jamBerakhir) {
                        $subQuery->where('jam_mulai', '<=', $jamMulai)
                            ->where('jam_berakhir', '>=', $jamBerakhir);
                    });
            })
            ->first();

        if ($existingJadwal) {
            return "Jadwal bentrok";
        }

        return null; // Tidak ada bentrok
    }

    public function terimaPengajuan($id)
    {
        $pengajuan = PengajuanJadwal::findOrFail($id);

        // Check if the proposed schedule conflicts with existing schedules
        $jamMulai = $pengajuan->jam_mulai;
        $jamBerakhir = $pengajuan->jam_berakhir;
        $hari = $pengajuan->hari;
        $ruang = $pengajuan->ruang;

        $jadwal = new JadwalController();
        $pesanError = $jadwal->cekJadwalBentrok($jamMulai, $jamBerakhir, $hari, $ruang);

        // If there's a scheduling conflict, return an error response
        if ($pesanError) {
            return response()->json(['error' => $pesanError], 400);
        }

        // Check if the practical session already exists in the schedule
        $existingJadwal = Jadwal::where('kode_praktikum', $pengajuan->kode_praktikum)->first();

        // If it already exists, return an error response
        if ($existingJadwal) {
            return response()->json(['error' => 'Praktikum sudah ada dalam jadwal.'], 400);
        }

        // Generate a unique schedule ID
        $kodeJadwal = JadwalController::generateJadwalId();

        // Create a new schedule entry
        Jadwal::create([
            'kode_jadwal' => $kodeJadwal,
            'kode_praktikum' => $pengajuan->kode_praktikum,
            'hari' => $pengajuan->hari,
            'jam_mulai' => $pengajuan->jam_mulai,
            'jam_berakhir' => $pengajuan->jam_berakhir,
            'ruang' => $pengajuan->ruang,
            'durasi' => $pengajuan->durasi,
        ]);

        // Update the number of sessions for the associated practical if needed
        $jumlahPertemuan = 0;

        if ($pengajuan->durasi == 200) {
            $jumlahPertemuan = 14;
        } elseif ($pengajuan->durasi == 160) {
            $jumlahPertemuan = 16;
        } elseif ($pengajuan->durasi == 170) {
            $jumlahPertemuan = 15;
        }

        if ($jumlahPertemuan > 0) {
            $praktikum = Praktikum::where('kode_praktikum', $pengajuan->kode_praktikum)->first();
            if ($praktikum) {
                $praktikum->jumlah_week = $jumlahPertemuan;
                $praktikum->save();
            }
        }

        // Update the status of the scheduling request to 'diterima'
        $pengajuan->status_pengajuan = 'diterima';
        $pengajuan->save();

        return response()->json(['message' => 'Pengajuan telah diterima dan data telah ditambahkan ke Jadwal.']);
    }


    public function tolakPengajuan($id)
    {
        $pengajuan = PengajuanJadwal::findOrFail($id);
        $pengajuan->status_pengajuan = 'ditolak';
        $pengajuan->save();

        return response()->json(['message' => 'Pengajuan telah ditolak.']);
    }

    // public function tambahJadwal($id)
    // {
    //     $pengajuan = PengajuanJadwal::findOrFail($id);
    //     $kodeJadwal = JadwalController::generateJadwalId();
    //     $jadwal = new JadwalController();

    //     // Cek apakah jadwal bentrok sebelum menambahkan data ke tabel Jadwal
    //     $jamMulai = $pengajuan->jam_mulai;
    //     $jamBerakhir = $pengajuan->jam_berakhir;
    //     $hari = $pengajuan->hari;
    //     $ruang = $pengajuan->ruang;

    //     $pesanError = $jadwal->cekJadwalBentrok($jamMulai, $jamBerakhir, $hari, $ruang);

    //     if ($pesanError) {
    //         return response()->json(['error' => $pesanError], 400);
    //     }

    //     // $pengajuan->status_pengajuan = 'diterima';
    //     // $pengajuan->save();

    //     // Cek apakah kode_praktikum sudah ada dalam tabel Jadwal
    //     $existingJadwal = Jadwal::where('kode_praktikum', $pengajuan->kode_praktikum)->first();

    //     if ($existingJadwal) {
    //         return response()->json(['error' => 'Praktikum sudah ada dalam jadwal.'], 400);
    //     }
    //     // Jika status pengajuan telah diterima, tambahkan data ke tabel Jadwal
    //     Jadwal::create([
    //         'kode_jadwal' => $kodeJadwal,
    //         'kode_praktikum' => $pengajuan->kode_praktikum,
    //         'hari' => $pengajuan->hari,
    //         'jam_mulai' => $pengajuan->jam_mulai,
    //         'jam_berakhir' => $pengajuan->jam_berakhir,
    //         'ruang' => $pengajuan->ruang,
    //         'durasi' => $pengajuan->durasi,
    //     ]);

    //     $jumlahPertemuan = 0;

    //     if ($pengajuan->durasi == 200) {
    //         $jumlahPertemuan = 14;
    //     } elseif ($pengajuan->durasi == 160) {
    //         $jumlahPertemuan = 16;
    //     } elseif ($pengajuan->durasi == 170) {
    //         $jumlahPertemuan = 15;
    //     }

    //     if ($jumlahPertemuan > 0) {
    //         $praktikum = Praktikum::where('kode_praktikum', $pengajuan->kode_praktikum)->first();
    //         if ($praktikum) {
    //             $praktikum->jumlah_week = $jumlahPertemuan;
    //             $praktikum->save();
    //         }
    //     }

    //     return response()->json(['message' => 'Data telah ditambahkan ke Jadwal.']);
    // }
}
