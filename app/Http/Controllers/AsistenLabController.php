<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AsistenLab;
use App\Models\User;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class AsistenLabController extends Controller
{
    public function index(Request $request)
    {
        $user = User::where('peran_pengguna', 'ASISTEN')->pluck('nama_lengkap', 'id')->toArray();

        if ($request->ajax()) {
            $data = AsistenLab::with(['users'])->latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('nama_lengkap', function ($row) {
                    return $row->users->nama_lengkap ?? '';
                })
                ->addColumn('aksi', function ($row) {
                    $btn = '<a href="javascript:void(0)" data-id="' . $row->id . '" class="edit btn btn-warning btn-sm editData"><i class="fa fa-edit"></i>&nbsp;Edit</a>';
                    $btn .= ' <a href="javascript:void(0)" data-id="' . $row->id . '" class="btn btn-danger btn-sm deleteData" data-url="' . route('aslab.store') . '"><i class="fa fa-trash"></i>&nbsp;Delete</a>';
                    return $btn;
                })
                ->rawColumns(['nama_lengkap', 'aksi'])
                ->make(true);
        }

        return view(
            'aslab.list',
            [
                "user" => $user
            ]
        );
    }



    public function store(Request $request)
    {
        if (!empty($request->id)) {
            $data = AsistenLab::find($request->id);
            $data->nrp = $request->nrp;
            $data->id_user = $request->id_user;
            $data->no_telp = $request->no_telp;
            $data->no_rekening = $request->no_rekening;
            $data->nama_bank = $request->nama_bank;
            $data->save();
        } else {
            // Memeriksa apakah nrp asisten sudah terdaftar
            $existingData = AsistenLab::where('id_user', $request->id_user)
                ->orWhere('nrp', $request->nrp)
                ->first();

            if ($existingData) {
                return response()->json(['error' => 'Data Asisten Lab Sudah Ada'], 422);
            }
            AsistenLab::create([
                'nrp' => $request->nrp,
                'id_user' => $request->id_user,
                'no_telp' => $request->no_telp,
                'no_rekening' => $request->no_rekening,
                'nama_bank' => $request->nama_bank,
            ]);
        }

        return response()->json(['success' => 'Data saved successfully.']);
    }

    public function edit($id)
    {
        $aslab = AsistenLab::find($id);
        return response()->json($aslab);
    }

    public function destroy($id)
    {
        AsistenLab::find($id)->delete();
        return response()->json(['success' => 'Data deleted successfully.']);
    }

    
}
