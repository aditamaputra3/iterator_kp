<?php

namespace App\Http\Controllers;

use App\Models\ModelDataPengguna;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Auth;
use App\Exports\PenggunaExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\PenggunaImport;

class PenggunaController extends Controller
{
    public function index(Request $request)
    {
        $data = ModelDataPengguna::all();
        if ($request->ajax()) {
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('aksi', function ($row) {
                    $btn = '<a href="javascript:void(0)" data-id="' . $row->id . '" class="edit btn btn-warning btn-sm editData"><i class="fa fa-edit"></i>&nbsp;Edit</a>';

                    $btn .= ' <a href="javascript:void(0)" data-id="' . $row->id . '" class="btn btn-danger btn-sm deleteData" data-url="' . route('pengguna.store') . '"><i class="fa fa-trash"></i>&nbsp;Delete</a>';

                    return $btn;
                })
                ->rawColumns(['aksi'])
                ->make(true);
        }

        return view('pengguna.list');
    }

    public function store(Request $request)
    {
        if (!empty($request->id)) {
            $data = ModelDataPengguna::find($request->id);
            $data->nama_lengkap = $request->nama_lengkap;
            $data->nama_pengguna = $request->nama_pengguna;
            $data->peran_pengguna = $request->peran_pengguna;
            if (!empty($request->password)) {
                $data->password = bcrypt($request->password);
            }
            $data->save();
        } else {
            ModelDataPengguna::create([
                'nama_lengkap' => $request->nama_lengkap,
                'nama_pengguna' => $request->nama_pengguna,
                'password' => bcrypt($request->password),
                'peran_pengguna' => $request->peran_pengguna,
            ]);
        }

        return response()->json(['success' => 'Data saved successfully.']);
    }

    public function edit($id)
    {
        $data_pengguna = ModelDataPengguna::find($id);
        return response()->json($data_pengguna);
    }

    public function destroy($id)
    {
        ModelDataPengguna::find($id)->delete();
        return response()->json(['success' => 'Data deleted successfully.']);
    }

    public function export()
    {
        return Excel::download(new PenggunaExport, 'pengguna.xlsx');
    }

    public function import(Request $request) 
    {
        // Validasi tipe file yang diizinkan
        $request->validate([
            'file' => 'required|mimes:csv,xls,xlsx',
        ]);
            
                // menangkap file excel
        $file = $request->file('file');
        
        // // membuat nama file unik
        // $nama_file = rand().$file->getClientOriginalName();
        
        // // upload ke folder file_siswa di dalam folder public
        // $file->move('file_pengguna',$nama_file);
        
        // // import data
        // Excel::import(new PenggunaImport, public_path('/file_pengguna/'.$nama_file));
        
    
        Excel::import(new PenggunaImport, $file);
    
        return response()->json(['success' => 'Data imported successfully.']);
    }
    
}
