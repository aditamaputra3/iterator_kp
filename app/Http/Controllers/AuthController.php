<?php

namespace App\Http\Controllers;

use App\Models\ModelDataPengguna;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Hashing\Hasher;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

    public function login(Request $request)
    {
        $credentials = $request->validate([
            'nama_pengguna' => 'required',
            'password' => 'required',
        ]);

        if (Auth::attempt($credentials)) {
            $user = Auth::user();

            if ($user->peran_pengguna == 'MAHASISWA') {
                session()->flash('success', 'Selamat Datang');
                return redirect()->intended('/bap'); 
            }elseif($user->peran_pengguna == 'ASISTEN'){
                session()->flash('success', 'Selamat Datang');
                return redirect()->intended('/bap'); 
            }elseif($user->peran_pengguna == 'KOORDINATOR'){
                session()->flash('success', 'Selamat Datang');
                return redirect()->intended('/bap'); 
            } else {
                session()->flash('success', 'Selamat Datang');
                return redirect()->intended('/dashboard'); 
            }
        }

        return back()->withErrors(['nama_pengguna' => 'Nama Pengguna atau password salah'])->onlyInput('nama_pengguna');
    }

    public function logout()
    {
        Auth::logout();
        session()->flash('success', 'Anda berhasil keluar.');
        return redirect('login');
    }
}
