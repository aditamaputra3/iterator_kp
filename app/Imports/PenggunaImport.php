<?php

namespace App\Imports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class PenggunaImport implements ToModel
{
    public function model(array $row)
    {
        // Validasi nilai peran_pengguna
        $validator = Validator::make([$row[4]], [
            'peran_pengguna' => Rule::in(['TEKNISI','KEPALA','ASISTEN','MAHASISWA','KOORDINATOR']), // Ganti dengan nilai-nilai ENUM yang sesuai
        ]);

        // Jika validasi gagal, kembalikan null
        if ($validator->fails()) {
            return null;
        }

        // Jika validasi berhasil, buat dan kembalikan model User
        return new User([
            'nama_lengkap' => $row[1],
            'nama_pengguna' => $row[2],
            'password' => bcrypt($row[3]),
            'peran_pengguna' => $row[4],
        ]);
    }
}
