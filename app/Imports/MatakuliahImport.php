<?php

namespace App\Imports;

use App\Models\Matakuliah;
use Maatwebsite\Excel\Concerns\ToModel;

class MatakuliahImport implements ToModel
{
    /**
     * @param Collection $collection
     */
    public function model(array $row)
    {
        return new Matakuliah([
            'kode_matakuliah' => $row[1],
            'nama_matakuliah' => $row[2],
            'sks' => $row[3],
            'semester' => $row[4],
            'tahun_ajaran' => $row[5],
        ]);
    }
}
