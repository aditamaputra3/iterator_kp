<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AsistenPraktikum extends Model
{
    use HasFactory;
    protected $table = "asisten_praktikum";
    protected $fillable = [
        'kode_praktikum',
        'nrp',
    ];

        // AsistenLab.php
    public function praktikum()
    {
        return $this->belongsToMany(Praktikum::class, 'kode_praktikum', 'nrp');
    }

    // Relationship with AsistenLab (Many-to-One)
    public function asistenLab()
    {
        return $this->belongsTo(AsistenLab::class, 'nrp', 'nrp');
    }
}
