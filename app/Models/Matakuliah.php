<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Matakuliah extends Model
{
    use HasFactory;
    protected $table = "matakuliah";
    protected $fillable = [
        'kode_matakuliah',
        'nama_matakuliah',
        'sks',
        'semester',
        'tahun_ajaran'
    ];

    // public function dosen()
    // {
    //     return $this->belongsTo(Dosen::class, 'nip', 'nip');
    // }

    // public function praktikum()
    // {
    //     return $this->hasMany(Praktikum::class);
    // }

    public function jadwal()
    {
        return $this->hasMany(Jadwal::class, 'kode_jadwal', 'kode_jadwal');
    }
    
}
