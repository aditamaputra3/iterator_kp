<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Casts\Attribute;


class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'nama_lengkap',
        'nama_pengguna',
        'password',
        'peran_pengguna',
    ];

    // protected function peran_pengguna(): Attribute
    // {
    //     return new Attribute(
    //         get: fn ($value) =>  ["TEKNISI", "KEPALA", "ASISTEN","KOORDINATOR"][$value],
    //     );
    // }

    // public function hasAnyRole($roles)
    // {
    //     return $this->whereIn('peran_pengguna', $roles)->exists();
    // }

    public function asistenLab()
    {
        return $this->hasMany(AsistenLab::class, 'id_user');
    }

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    // protected $hidden = [
    //     'password',
    // ];

    // /**
    //  * The attributes that should be cast.
    //  *
    //  * @var array<string, string>
    //  */
    // protected $casts = [
    //     'email_verified_at' => 'datetime',
    // ];
}
