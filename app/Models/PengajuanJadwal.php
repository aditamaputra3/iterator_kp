<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PengajuanJadwal extends Model
{
    use HasFactory;
    protected $table = "pengajuan_jadwal";
    protected $fillable=['kode_pengajuan','kode_praktikum','hari','jam_mulai','jam_berakhir','ruang','durasi','status_pengajuan','id_user'];

    public function praktikum()
    {
        return $this->belongsTo(Praktikum::class, 'kode_praktikum','kode_praktikum');
    }
}
