<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BapPraktikum extends Model
{
    use HasFactory;
    protected $table = 'bap'; 

    protected $fillable = [
        'kode_praktikum',
        'tanggal',
        'materi_praktikum',
    ];

    public function praktikum()
    {
        return $this->belongsTo(Praktikum::class, 'kode_praktikum', 'kode_praktikum');
    }
}
