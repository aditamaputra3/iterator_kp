<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AsistenLab extends Model
{
    use HasFactory;
    protected $table = "asisten_lab";
    protected $fillable = [
        'nrp',
        'id_user',
        'no_telp',
        'no_rekening',
        'nama_bank',
    ];

    // Relationship with User (Many-to-One)
    public function users()
    {
        return $this->belongsTo(User::class, 'id_user');
    }

    // Relationship with AsistenPraktikum (One-to-Many)
    public function asistenPraktikums()
    {
        return $this->hasMany(AsistenPraktikum::class, 'nrp', 'nrp');
    }
}
