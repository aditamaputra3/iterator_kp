<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModelDataPengguna extends Model
{
    use HasFactory;
    protected $table = "users";
    protected $fillable = [
        'nama_lengkap',
        'nama_pengguna',
        'password',
        'peran_pengguna',
    ];

    // protected $hidden = [
    //     'password',
    // ];
}
