<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterBarang extends Model
{
    use HasFactory;
    protected $table = "master_barang";
    protected $fillable = [
        'id_kategori_barang',
        'id_pemasok',
        'nama_barang',
        'deskripsi_barang',
        'harga',
        'spesifikasi',
        'merk',
        'tahun_masuk',
        'gambar_barang'
    ];
}
