<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Events\PraktikumCreated;

class Praktikum extends Model
{
    use HasFactory;
    protected $table = "praktikum";
    
    protected $fillable = ['kode_praktikum', 'kode_matakuliah', 'kelas', 'jumlah_peserta', 'jumlah_week', 'nip'];

    public function jadwal()
    {
        return $this->hasMany(Praktikum::class, 'kode_praktikum', 'kode_praktikum');
    }

    public function matakuliah()
    {
        return $this->belongsTo(Matakuliah::class, 'kode_matakuliah', 'kode_matakuliah');
    }

    public function dosen()
    {
        return $this->belongsTo(Dosen::class, 'nip', 'nip');
    }

    public function bap()
    {
        return $this->hasMany(Bap::class, 'kode_praktikum', 'kode_praktikum');
    }

    public function mahasiswa()
    {
        return $this->hasMany(Mahasiswa::class, 'kode_praktikum', 'kode_praktikum');
    }

    public function asistenPraktikums()
    {
        return $this->belongsTo(AsistenPraktikum::class, 'nrp', 'nrp');
    }


    // // Praktikum.php
    // public function asistenPraktikum()
    // {
    //     return $this->belongsToMany(AsistenPraktikum::class, 'kode_praktikum', 'kode_praktikum');
    // }
}
