<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kondisi extends Model
{
    use HasFactory;
    public $timestamps = true;
    protected $table = "kondisi";
    protected $fillable = [
        'id_master_barang',
        'id_detail_kondisi',
        'status',
    ];
}
