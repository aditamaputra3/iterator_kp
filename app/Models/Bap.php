<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bap extends Model
{
    use HasFactory;
    protected $table = 'bap'; 

    protected $fillable = [
        'kode_praktikum',
        'tanggal',
        'topik',
        'asisten1',
        'asisten2',
        'asisten3',
        'mahasiswa',
    ];

    public function praktikum()
    {
        return $this->belongsTo(Praktikum::class, 'kode_praktikum', 'kode_praktikum');
    }
}
