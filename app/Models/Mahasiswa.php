<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    use HasFactory;
    protected $table = "mahasiswa";
    protected $fillable = [
        'nrp',
        'id_user',
        'kode_praktikum',
    ];

    // Relationship with User (Many-to-One)
    public function users()
    {
        return $this->belongsTo(User::class, 'id_user');
    }

    public function praktikum()
    {
        return $this->belongsTo(Praktikum::class, 'kode_praktikum','kode_praktikum');
    }

    public function matakuliah()
    {
        return $this->belongsTo(Matakuliah::class, 'kode_matakuliah', 'kode_matakuliah');
    }
}
