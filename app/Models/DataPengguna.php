<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModelDataPengguna extends Model
{
    use HasFactory;
    protected $table = "data_pengguna";
    protected $fillable = [
        'nama_lengkap',
        'nama_pengguna',
        'sandi_pengguna',
        'peran_pengguna',
    ];
}
