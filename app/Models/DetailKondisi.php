<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailKondisi extends Model
{
    use HasFactory;
    protected $table = "detail_kondisi";
    protected $fillable = [
        'nama_kondisi',
        'keterangan_kodisi',
    ];

}
