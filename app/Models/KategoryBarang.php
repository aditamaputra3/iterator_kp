<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KategoryBarang extends Model
{
    use HasFactory;
    protected $table = "kategory_barang";
    protected $fillable = [
        'nama_kategori_barang',
        'deskripsi_kategori_barang',
    ];
}

