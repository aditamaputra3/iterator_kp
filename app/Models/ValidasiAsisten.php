<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ValidasiAsisten extends Model
{
    use HasFactory;
    protected $table = 'validasi_asisten'; 

    protected $fillable = [
        'bap_id',
        'nrp',
        'status_kehadiran',
    ];
}
