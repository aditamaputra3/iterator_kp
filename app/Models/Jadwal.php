<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Jadwal extends Model
{
    use HasFactory;
    protected $table = "jadwal";
    protected $fillable=['kode_jadwal','kode_praktikum','hari','jam_mulai','jam_berakhir','ruang','durasi'];

    public function praktikum()
    {
        return $this->belongsTo(Praktikum::class, 'kode_praktikum','kode_praktikum');
    }

    public function matakuliah()
    {
        return $this->belongsTo(Matakuliah::class, 'kode_matakuliah', 'kode_matakuliah');
    }
}
