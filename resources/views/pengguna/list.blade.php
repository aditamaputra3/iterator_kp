@extends('layout.layout-admin')

@section('title')
    {{ 'User' }}
@endsection

@section('content')
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="modal-form">
        <div class="modal-dialog" role="document">
            <form class="form-horizontal">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title"></h4>
                        <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="id" id="id" class="form-control">
                        <div class="form-group">
                            <label for="name">Nama Lengkap</label>
                            <input type="text" name="nama_lengkap" id="nama_lengkap" class="form-control" autofocus>
                            <span class="text-danger" id="error-nama_lengkap"></span>
                        </div>
                        <div class="form-group">
                            <label for="name">Nama Pengguna</label>
                            <input type="text" name="nama_pengguna" id="nama_pengguna" class="form-control" autofocus>
                            <span class="text-danger" id="error-nama_pengguna"></span>
                        </div>
                        <div class="form-group">
                            <label for="name">Sandi</label>
                            <input type="password" name="password" id="password" class="form-control" autofocus>
                            <span class="text-danger" id="error-password"></span>
                        </div>
                        <div class="form-group">
                            <label for="name">Peran Pengguna</label>
                            <select name="peran_pengguna" id="peran_pengguna" class="form-control" required>
                                <option value="MAHASISWA">MAHASISWA</option>
                                <option value="KEPALA">KEPALA LAB</option>
                                <option value="ASISTEN">ASISTEN PRAKTIKUM</option>
                                <option value="TEKNISI">TEKNISI</option>
                                <option value="KOORDINATOR">KOORDINATOR PRAKTIKUM</option>
                            </select>
                            <span class="text-danger" id="error-peran_pengguna"></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-warning close-btn" data-dismiss="modal"><i
                                class="fa fa-arrow-circle-left"></i> Cancel</button>
                        <button type="submit" class="btn btn-sm btn-primary" id="saveBtn"><i class="fa fa-save"></i>
                            Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

<!-- Import Modal -->
<div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-labelledby="importModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form id="importForm" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="importModalLabel">Import Data Pengguna</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="file">Pilih File Excel:</label>
                        <input type="file" class="form-control-file" id="file" name="file" accept=".csv,.xls,.xlsx" required>
                    </div>
                    <p>Catatan: File Excel harus memiliki format .xls, .xlsx, atau .csv. File hanya berisi data dan tidak usah memasukan judul dari setiap kolom. Kolom-kolom dalam file Excel harus mengikuti format berikut:</p>
                    <img src="{{ asset('images/tutor1.png') }}" alt="Deskripsi Gambar" width="450" height="200">


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="importBtn">Import</button>
                </div>
            </div>
        </form>
    </div>
</div>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-outline card-warning">
                        <div class="card-body">
                            <div class="d-flex justify-content-between mb-4">
                                <div class="d-flex justify-content-start" id="print">
                                    <!-- Second div content (if any) -->
                                    <button type="button" class="btn btn-info mr-2" data-toggle="modal" data-target="#importModal">
                                        <i class="fa fa-upload"></i> Import
                                    </button>
                                </div>
                                <div class="d-flex justify-content-end">
                                    <button type="button" onclick="addForm()" class="btn btn-warning">
                                        <i class="fa fa-plus"></i> Tambah User
                                    </button>
                                    {{-- <a href="{{ route('pengguna.export') }}" class="btn btn-success">
                                        <i class="fa fa-download"></i> Export
                                    </a> --}}
                                    
                                </div>

                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped custom-table mb-0 no-footer" role="grid">
                                    <thead>
                                        <tr role="row">
                                            <th>#</th>
                                            <th>Nama Lengkap</th>
                                            <th>Nama Pengguna</th>
                                            <th>Peran</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

   

@endsection

@section('script')
    <script>
        $(document).ready(function () {
        $('#importForm').submit(function (e) {
            e.preventDefault(); // Menghentikan aksi default formulir

            var formData = new FormData(this); // Membuat objek FormData dari formulir

            $.ajax({
                url: '{{ route("pengguna.import") }}',
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function (data) {
                    $('.modal').modal('hide');
                        $('.table').DataTable().draw();

                        // Tampilkan pesan 'success' dari response JSON
                        if (data.success) {
                            showSuccessToast(data.success); // Gunakan response JSON sebagai judul toast
                        }
                },
                error: function(data) {
                    console.log('Error:', data);
                        $('#importBtn').html('Save');
                        // Tampilkan pesan 'error' dari response JSON jika ada
                        if (data.responseJSON && data.responseJSON.error) {
                            showErrorToast(data.responseJSON.error);
                        } else {
                            // Tampilkan pesan error kustom jika tidak ada pesan error dalam response JSON
                            showErrorToast('Gagal Import Data, Mohon Cek Kembali.');
                        }
                }
            });
        });
    });

        let routeUrl = "{{ route('pengguna.index') }}";

        let columns = [{
                data: 'DT_RowIndex',
                searchable: false,
                orderable: true
            },
            {
                data: 'nama_lengkap',
                name: 'nama_lengkap',
            },
            {
                data: 'nama_pengguna',
                name: 'nama_pengguna',
            },
            {
        data: 'peran_pengguna',
        name: 'peran_pengguna',
        render: function(data, type, row) {
            let labelClass = '';
            switch (data) {
                case 'MAHASISWA':
                    labelClass = 'badge-secondary';
                    break;
                case 'ASISTEN':
                    labelClass = 'badge-primary';
                    break;
                case 'KEPALA':
                    labelClass = 'badge-success';
                    break;
                case 'KOORDINATOR':
                    labelClass = 'badge-warning';
                    break;
                case 'TEKNISI':
                    labelClass = 'badge-danger';
                    break;
                default:
                    labelClass = 'badge-dark';
            }
            return '<span class="badge ' + labelClass + '">' + data + '</span>';
        }
    },
            {
                data: 'aksi',
                name: 'aksi',
                orderable: false,
                searchable: false
            }
        ];

        let table = initializeDataTables(routeUrl, columns);

        $('body').on('click', '.editData', function() {
            var id = $(this).data('id');
            $.get("{{ route('pengguna.index') }}" + '/' + id + '/edit', function(data) {
                $('.modal-title').text('Edit Data');
                $('#modal-form').modal('show');
                $('#id').val(data.id);
                $('#nama_lengkap').val(data.nama_lengkap);
                $('#nama_pengguna').val(data.nama_pengguna);
                $('#password').val("");
                $('#peran_pengguna').val(data.peran_pengguna);
            })
        });

        function addForm() {
            $("#modal-form").modal('show');
            $('#id').val('');
            $('.modal-title').text('Tambah Data');
            $('#modal-form form')[0].reset();
            $('#modal-form [name=nama_lengkap').focus();
            $('#modal-form [name=nama_pengguna').focus();
            $('#modal-form [name=password').focus();
            $('#modal-form [name=peran_pengguna').focus();
        }

        function validation(data, isCreate) {
            let formIsValid = true;
            $('span[id^="error"]').text('');
            if (!data.nama_lengkap) {
                formIsValid = false;
                $("#error-nama_lengkap").text('Nama lengkap wajib diisi.')
            }

            if (!data.nama_pengguna) {
                formIsValid = false;
                $("#error-nama_pengguna").text('Nama pengguna wajib diisi.')
            }

            if (!!isCreate) {
                // if (!data.password) {
                //   formIsValid = false;
                //   $("#error-password").text('Sandi pengguna wajib diisi.')
                // }
            }
            return formIsValid;
        }
    </script>
@endsection
