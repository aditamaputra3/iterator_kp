@extends('layout.layout-admin')

@section('title')
    {{ 'Mahasiswa' }}
@endsection

@section('content')
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="modal-form">
        <div class="modal-dialog" role="document">
            <form class="form-horizontal">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title"></h4>
                        <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="id" id="id" class="form-control">
                        <div class="form-group">
                            <label for="name">NRP</label>
                            <input type="number" name="nrp" id="nrp" class="form-control" autofocus>
                            <span class="text-danger" id="error-nrp"></span>
                        </div>
                        <div class="form-group">
                            <label for="name">Nama Mahasiswa</label>
                            <select name="id_user" id="id_user" class="form-control" required>
                                <option>-- Pilih Mahasiswa--</option>
                                @foreach ($user as $id => $nama_lengkap)
                                    <option value="{{ $id }}">{{ $nama_lengkap }}
                                    </option>
                                @endforeach
                            </select>
                            <span class="text-danger" id="error-nama"></span>
                        </div>
                        <div class="form-group">
                            <label for="name">Pilih Praktikum</label>
                            <select name="kode_praktikum" id="kode_praktikum" class="form-control" required>
                                <option>-- Pilih Praktikum --</option>
                                @foreach ($praktikum as $data)
                                    {{-- @if (!in_array($data['kode_praktikum'], $existingKodePraktikum)) --}}
                                    <option value="{{ $data['kode_praktikum'] }}">
                                        {{ $data['kode_matakuliah'] }} - {{ $data['nama_matakuliah'] }} -
                                        {{ $data['kelas'] }}</option>
                                    {{-- @endif --}}
                                @endforeach
                            </select>
                            <span class="text-danger" id="error-kode_praktikum"></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-warning close-btn" data-dismiss="modal"><i
                                class="fa fa-arrow-circle-left"></i> Cancel</button>
                        <button type="submit" class="btn btn-sm btn-primary" id="saveBtn"><i class="fa fa-save"></i>
                            Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-outline card-warning">
                        <div class="card-body">
                            <div class="d-flex justify-content-between mb-4">
                                <div class="d-flex justify-content-start" id="print">
                                    <!-- Second div content (if any) -->
                                </div>
                                <div class="d-flex justify-content-end">
                                    <button type="button" onclick="addForm()" class="btn btn-warning">
                                        <i class="fa fa-plus"></i> Tambah Mahasiswa
                                    </button>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped custom-table mb-0 no-footer" role="grid">
                                    <thead>
                                        <tr role="row">
                                            <th>#</th>
                                            <th>NRP</th>
                                            <th>Nama Mahasiswa</th>
                                            <th>Matakuliah</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        let routeUrl = "{{ route('mahasiswa.index') }}";

            let columns = [{
                    data: 'DT_RowIndex',
                    searchable: false,
                    orderable: true,
                },
                {
                    data: 'nrp',
                    name: 'nrp',
                },
                {
                    data: 'nama_lengkap',
                    name: 'nama_lengkap',
                },
                {
                data: null,
                render: function(data, type, row) {
                    // Gabungkan data 'nama_matakuliah', 'kode_praktikum', dan 'kode_matakuliah' dalam satu kolom
                    return data.kode_matakuliah + ' - ' + data.nama_matakuliah + ' - ' + data.kelas + '';
                },
                searchable: true, // Aktifkan pencarian pada kolom hasil render
                name: 'kode_matakuliah', // Nama kolom yang digunakan untuk pencarian
                name: 'nama_matakuliah',
                searchable: true // Aktifkan pencarian pada kolom nama_matakuliah
            },
                {
                    data: 'aksi',
                    name: 'aksi',
                    orderable: false,
                    searchable: false
                }
             ];

            let table = initializeDataTables(routeUrl, columns);


        $('body').on('click', '.editData', function() {
            var id = $(this).data('id');
            $.get("{{ route('mahasiswa.index') }}" + '/' + id + '/edit', function(data) {
                $('.modal-title').text('Edit Data');
                $('#modal-form').modal('show');
                $('#id').val(data.id);

                $('#id_user').val(data.id_user);
                $('#kode_praktikum').val(data.kode_praktikum);
                
                 // Tampilkan form kode_praktikum pada mode edit
                 $('#nrp').prop('readonly', true).val(data.nrp);
            })
        });

        function addForm() {
            $("#modal-form").modal('show');
            $('#id').val('');
            $('.modal-title').text('Tambah Data');
            $('#modal-form form')[0].reset();
            $('#modal-form [name=nrp').focus();
            $('#modal-form [name=id_user').focus();
            $('#modal-form [name=kode_praktikum').focus();

              // Tampilkan form kode_praktikum pada mode edit
              $('#nrp').prop('readonly', false).val(data.nrp);
        }

        function validation(data, isCreate) {
            let formIsValid = true;
            $('span[id^="error"]').text('');
            if (!data.nrp) {
                formIsValid = false;
                $("#error-nrp").text('NRP mahasiswa wajib diisi.')
            }
            return formIsValid;
        }
    </script>
@endsection
