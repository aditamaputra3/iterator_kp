@extends('layout.layout-front')

@section('title')
    {{ 'Jadwal Praktikum' }}
@endsection

@section('content')
<div class="container mt-5">
    <div class="row">
        <div class="col">
            <div class="card mb-4">
                {{-- <div class="card-header">
                    <h5 class="card-title">Jadwal Praktikum</h5>
                </div> --}}
                <div class="card-body">
                    <h5 class="mb-4">Jadwal Praktikum</h5>
                    <table class="table table-striped custom-table mb-0 no-footer" role="grid">
                        <thead>
                            <tr role="row">
                                <th>No.</th>
                                <th>Matakuliah Praktikum</th>
                                <th>Kelas</th>
                                <th>Hari</th>
                                <th>Jam</th>
                                <th>Ruang</th>
                                <th>Durasi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Isi tabel disini -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    // AJAX route for DataTables
    let routeUrl = "{{ route('jadwal-praktikum.index') }}";

    // Column configuration for DataTables
    let columns = [{
            data: 'DT_RowIndex',
            searchable: false
        },
        {
            data: null,
            render: function(data, type, row) {

                return data.kode_matakuliah + ' - ' + data.nama_matakuliah + '';
            },
            name: 'nama_matakuliah',
            searchable: true
        },
        {
            data: 'kelas',
            name: 'kelas'
        },
        {
            data: 'hari',
            name: 'hari'
        },
        {
            data: null,
            render: function(data, type, row) {

                return data.jam_mulai + ' - ' + data.jam_berakhir + '';
            },
            name: 'jam_mulai',
            searchable: true
        },
        {
            data: 'ruang',
            name: 'ruang'
        },
        {
            data: null,
            render: function(data, type, row) {
                // Tambahkan teks "menit" setelah nilai durasi
                return data.durasi + ' Menit';
            }
        }
    ];

    let table = initializeDataTables(routeUrl, columns);

</script>
@endsection
