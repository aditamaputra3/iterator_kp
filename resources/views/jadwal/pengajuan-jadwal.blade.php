@extends('layout.layout-admin')

@section('title')
    {{ 'Pengajuan Jadwal' }}
@endsection

@section('content')
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="modal-form">
        <div class="modal-dialog" role="document">
            <form class="form-horizontal">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title"></h4>
                        <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="id" id="id" class="form-control">
                        <input type="hidden" name="id_user" id="id_user" value="" class="form-control">
                        <div class="form-group">
                            <label for="kode_pengajuan">Kode Pengajuan Jadwal</label>
                            <input type="text" name="kode_pengajuan" id="kode_pengajuan" class="form-control" readonly>
                            <span class="text-danger" id="error-kode_pengajuan"></span>
                        </div>
                        <div class="form-group">
                            <label for="Praktikum">Praktikum</label>
                            @if ($kodePraktikum->count() > 1)
                                <select class="form-control" name="kode_praktikum">
                                    @foreach ($kodePraktikum as $ap)
                                        <option value="{{ $ap->kode_praktikum }}">{{ $ap->kode_matakuliah }} -
                                            {{ $ap->nama_matakuliah }} - {{ $ap->kelas }}</option>
                                    @endforeach
                                </select>
                            @elseif ($kodePraktikum->count() == 1)
                                @foreach ($kodePraktikum as $ap)
                                    <input type="text" class="form-control"
                                        value="{{ $ap->kode_matakuliah }} - {{ $ap->nama_matakuliah }} -  {{ $ap->kelas }}"
                                        readonly>
                                    <input type="text" id="kode_praktikum" name="kode_praktikum"
                                        value="{{ $ap->kode_praktikum }}" hidden>
                                @endforeach
                            @else
                                <p>Tidak ada data yang tersedia.</p>
                            @endif

                            <span class="text-danger" id="error-kode_praktikum"></span>
                        </div>
                        <div class="form-group">
                            <label for="hari">Hari</label>
                            <select name="hari" id="hari" class="form-control" style="width: 100%;" required>
                                <option>-- Pilih Hari --</option>
                                <option value="Senin">Senin</option>
                                <option value="Selasa">Selasa</option>
                                <option value="Rabu">Rabu</option>
                                <option value="Kamis">Kamis</option>
                                <option value="Jumat">Jumat</option>
                                <option value="Sabtu">Sabtu</option>
                                <option value="Minggu">Minggu</option>
                            </select>
                            <span class="text-danger" id="error-hari"></span>
                        </div>
                        <div class="form-group">
                            <label for="name">Jam Mulai Praktikum</label>
                            <div class="input-group date" id="timepicker" data-target-input="nearest">
                                <input type="text" id="jam_mulai" name="jam_mulai"
                                    class="form-control datetimepicker-input" data-target="#timepicker"
                                    placeholder="13:00" />
                                <div class="input-group-append" data-target="#timepicker" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="far fa-clock"></i></div>
                                </div>
                            </div>
                            <span class="text-danger" id="error-jam-mulai"></span>
                        </div>
                        <div class="form-group">
                            <label for="ruang">Ruang</label>
                            <select name="ruang" id="ruang" class="form-control" style="width: 100%;" required>
                                <option>-- Pilih Ruang --</option>
                                <option value="Lab MSI">LAB MSI</option>
                                <option value="Lab RPL">LAB RPL</option>
                            </select>
                            <span class="text-danger" id="error-ruang"></span>
                        </div>
                        <div class="form-group">
                            <label for="jumlah_pertemuan">Jumlah Pertemuan</label>
                            <select name="durasi" id="durasi" class="form-control" style="width: 100%;" required>
                                <option selected>-- Pilih Jumlah Pertemuan --</option>
                                <option value="160">16 Pertemuan</option>
                                <option value="170">15 Pertemuan</option>
                                <option value="200">14 Pertemuan</option>
                            </select>
                            <span class="text-danger" id="error-pertemuan"></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-warning close-btn" data-dismiss="modal"><i
                                class="fa fa-arrow-circle-left"></i> Cancel</button>
                        <button type="submit" class="btn btn-sm btn-primary" id="saveBtn"><i class="fa fa-save"></i>
                            Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-outline card-warning">
                        <div class="card-body">
                            <div class="d-flex justify-content-between mb-4">
                                <div class="d-flex justify-content-start" id="print">
                                    <!-- Second div content (if any) -->
                                </div>
                                <div class="d-flex justify-content-end">
                                    <button type="button" onclick="addForm()" class="btn btn-warning">
                                        <i class="fa fa-plus"></i> Tambah Pengajuan Jadwal
                                    </button>

                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped custom-table mb-0 no-footer" role="grid">
                                    <thead>
                                        <tr role="row">
                                            <th>#</th>
                                            <th>Kode Pengajuan</th>
                                            <th>Nama Praktikum</th>
                                            <th>Kelas</th>
                                            <th>Hari</th>
                                            <th>Jam</th>
                                            <th>Ruang</th>
                                            <th>Durasi</th>
                                            <th>Status Pengajuan</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        // Mengambil id_user yang terautentikasi dari objek Auth
        var idUser = {{ Auth::id() }};

        // Mengisi nilai input dengan id_user
        document.getElementById("id_user").value = idUser;
        
        $(function() {

            //Timepicker
            $('#timepicker').datetimepicker({
                format: 'HH:mm'
            })

        })
        // AJAX route for DataTables
        let routeUrl = "{{ route('pengajuan-jadwal.index') }}";

        // Column configuration for DataTables
        let columns = [{
                data: 'DT_RowIndex',
                searchable: false
            },
            {
                data: 'kode_pengajuan',
                name: 'kode_pengajuan'
            },
            {
                data: null,
                render: function(data, type, row) {
                    return data.kode_matakuliah + ' - ' + data.nama_matakuliah + '';
                },
                searchable: true, 
                name: 'kode_matakuliah', 

                searchable: true, // Aktifkan pencarian pada kolom nama_matakuliah
                name: 'nama_matakuliah'
            },
            {
                data: 'kelas',
                name: 'kelas'
            },
            {
                data: 'hari',
                name: 'hari'
            },
            {
            data: null,
            render: function(data, type, row) {

                return data.jam_mulai + ' - ' + data.jam_berakhir + '';
            },
            name: 'jam_mulai',
            searchable: true
        },
            {
                data: 'ruang',
                name: 'ruang'
            },
            {
                data: null,
                render: function(data, type, row) {
                    return data.durasi + ' Menit';
                }
            },
            {
                data: 'status_pengajuan',
                name: 'status_pengajuan',
                render: function(data, type, row) {
                    if (data === 'diterima') {
                        return '<span class="badge bg-success">Diterima</span>';
                    } else if (data === 'ditolak') {
                        return '<span class="badge bg-danger">Ditolak</span>';
                    } else if (data === 'menunggu') {
                        return '<span class="badge bg-warning">Menunggu Konfirmasi</span>';
                    }
                    return data; // Kembalikan nilai asli jika tidak cocok dengan kondisi di atas
                }
            },
            {
                data: 'aksi',
                name: 'aksi',
                orderable: false,
                searchable: false
            }
        ];

        let table = initializeDataTables(routeUrl, columns);


        $('body').on('click', '.editData', function() {
            var id = $(this).data('id');
            $.get("{{ route('pengajuan-jadwal.index') }}" + '/' + id + '/edit', function(data) {
                $('.modal-title').text('Edit Data');
                $('#modal-form').modal('show');
                $('#id').val(data.id);
                $('#kode_praktikum').val(data.kode_praktikum);
                $('#hari').val(data.hari);
                $('#jam_mulai').val(data.jam_mulai);
                $('#ruang').val(data.ruang);
                $('#durasi').val(data.durasi);

                // Tampilkan form kode_praktikum pada mode edit
                $('#kode_pengajuan').prop('hidden', false).val(data.kode_pengajuan);

                // // Fokus ke input kode_matakuliah saat mode edit
                // $('#modal-form [name=kode_matakuliah').focus();

                // Tampilkan label kode_praktikum pada mode tambah data
                $('#modal-form label[for="kode_pengajuan"]').show();
            });
        });

        function addForm() {
            $("#modal-form").modal('show');
            $('#id').val('');
            $('.modal-title').text('Tambah Data');
            $('#modal-form form')[0].reset();
            $('#modal-form [name=kode_praktikum').focus();
            $('#modal-form [name=hari').focus();
            $('#modal-form [name=jam_mulai').focus();
            $('#modal-form [name=ruang').focus();
            $('#modal-form [name=durasi').focus();

            // Sembunyikan label kode_praktikum pada mode edit
            $('#modal-form label[for="kode_pengajuan"]').hide();

            // Sembunyikan form kode_praktikum pada mode tambah data
            $('#kode_pengajuan').prop('hidden', true).val('');
        }

        function validation(data, isCreate) {
            let formIsValid = true;
            $('span[id^="error"]').text('');

            if (!data.kode_praktikum) {
                formIsValid = false;
                $("#error-kode_praktikum").text('Praktikum wajib dipilih.')
            }

            if (!data.hari || data.hari === "-- Pilih Hari --") {
                formIsValid = false;
                $("#error-hari").text('Hari wajib dipilih.');
            }

            if (!data.jam_mulai) {
                formIsValid = false;
                $("#error-jam-mulai").text('Jam mulai wajib diisi.');
            }

            if (!data.ruang || data.ruang === "-- Pilih Ruang --") {
                formIsValid = false;
                $("#error-ruang").text('Ruang wajib dipilih.');
            }

            if (!data.durasi || data.durasi === "-- Pilih Jumlah Pertemuan --") {
                formIsValid = false;
                $("#error-pertemuan").text('Pertemuan wajib dipilih.');
            }


            return formIsValid;
        }

        $('body').on('click', '.terimaPengajuan', function() {
            var id = $(this).data('id');
            if (confirm(
                    "Kamu yakin ingin terima pengajuan? Jadwal akan langsung ditambahkan ke jadwal praktikum") ==
                true
            ) {
                $.ajax({
                    type: 'POST',
                    url: "{{ route('terima.pengajuan', ['id' => ':id']) }}".replace(':id', id),
                    data: {
                        _token: '{{ csrf_token() }}',
                    },
                    success: function(data) {
                        $('.table').DataTable().draw();
                        // Tampilkan pesan 'success' dari response JSON
                        if (data.success) {
                            showSuccessToast(data.success); // Gunakan response JSON sebagai judul toast
                        }
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Save Changes');
                        // Tampilkan pesan 'error' dari response JSON jika ada
                        if (data.error) {
                            showErrorToast(data.error);
                        } else {
                            // Tampilkan pesan error kustom jika tidak ada pesan error dalam response JSON
                            showErrorToast('An error occurred');
                        }
                    }
                });
            }
        });

        $('body').on('click', '.tolakPengajuan', function() {
            var id = $(this).data('id');
            if (confirm("Kamu yakin ingin menolak pengajuan?") == true) {
                $.ajax({
                    type: 'POST',
                    url: "{{ route('tolak.pengajuan', ['id' => ':id']) }}".replace(':id', id),
                    data: {
                        _token: '{{ csrf_token() }}',
                    },
                    success: function(data) {
                        $('.table').DataTable().draw();
                        // Tampilkan pesan 'success' dari response JSON
                        if (data.success) {
                            showSuccessToast(data.success); // Gunakan response JSON sebagai judul toast
                        }
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Save Changes');
                        // Tampilkan pesan 'error' dari response JSON jika ada
                        if (data.error) {
                            showErrorToast(data.error);
                        } else {
                            // Tampilkan pesan error kustom jika tidak ada pesan error dalam response JSON
                            showErrorToast('An error occurred.');
                        }
                    }
                });
            }
        });
    </script>
@endsection
