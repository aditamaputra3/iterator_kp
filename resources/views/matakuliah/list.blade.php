@extends('layout.layout-admin')

@section('title')
    {{ 'Matakuliah' }}
@endsection

@section('content')
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="modal-form">
        <div class="modal-dialog" role="document">
            <form class="form-horizontal">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title"></h4>
                        <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="id" id="id" class="form-control">
                        <div class="form-group">
                            <label for="name">Kode Matakuliah</label>
                            <input type="text" name="kode_matakuliah" id="kode_matakuliah" class="form-control"
                                autofocus>
                            <span class="text-danger" id="error-kode-matakuliah"></span>
                        </div>
                        <div class="form-group">
                            <label for="name">Nama Matakuliah</label>
                            <input type="text" name="nama_matakuliah" id="nama_matakuliah" class="form-control"
                                autofocus>
                            <span class="text-danger" id="error-nama_matakuliah"></span>
                        </div>
                        <div class="form-group">
                            <label for="name">SKS</label>
                            <input type="number" name="sks" id="sks" class="form-control" autofocus>
                            <span class="text-danger" id="error-sks"></span>
                        </div>
                        <div class="form-group">
                            <label for="name">Semester</label>
                            <select name="semester" id="semester" class="form-control" required>
                                <option>-- Pilih Semester --</option>
                                <option value="Ganjil">Ganjil</option>
                                <option value="Genap">Genap</option>
                            </select>
                            <span class="text-danger" id="error-semester"></span>
                        </div>
                        <div class="form-group">
                            <label for="name">Tahun Ajaran</label>
                            <select name="tahun_ajaran" id="tahun_ajaran" class="form-control" required>
                                <option>-- Pilih Tahun Ajaran --</option>
                                @for ($year = date('Y'); $year >= date('Y') - 10; $year--)
                                    <option value="{{ $year }}/{{ $year + 1 }}">
                                        {{ $year }}/{{ $year + 1 }}</option>
                                @endfor
                            </select>
                            <span class="text-danger" id="error-id_pemasok"></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-warning close-btn" data-dismiss="modal"><i
                                class="fa fa-arrow-circle-left"></i> Cancel</button>
                        <button type="submit" class="btn btn-sm btn-primary" id="saveBtn"><i class="fa fa-save"></i>
                            Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- Import Modal -->
<div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-labelledby="importModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form id="importForm" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="importModalLabel">Import Data Matakuliah</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="file">Pilih File Excel:</label>
                        <input type="file" class="form-control-file" id="file" name="file" accept=".csv,.xls,.xlsx" required>
                    </div>
                    <p>Catatan: File Excel harus memiliki format .xls, .xlsx, atau .csv. File hanya berisi data dan tidak usah memasukan judul dari setiap kolom. Kolom-kolom dalam file Excel harus mengikuti format berikut:</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="importBtn">Import</button>
                </div>
            </div>
        </form>
    </div>
</div>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-outline card-warning">
                        <div class="card-body">
                            <div class="d-flex justify-content-between mb-4">
                                <div class="d-flex justify-content-start" id="print">
                                    <button type="button" class="btn btn-info mr-2" data-toggle="modal" data-target="#importModal">
                                        <i class="fa fa-upload"></i> Import
                                    </button>
                                </div>
                                <div class="d-flex justify-content-end">
                                    <button type="button" onclick="addForm()" class="btn btn-warning">
                                        <i class="fa fa-plus"></i> Tambah Matakuliah
                                    </button>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped custom-table mb-0 no-footer" role="grid">
                                    <thead>
                                        <tr role="row">
                                            <th>#</th>
                                            <th>Kode Matakuliah</th>
                                            <th>Nama Matakuliah</th>
                                            <th>SKS</th>
                                            <th>Semester</th>
                                            <th>Tahun Ajaran</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
           $(document).ready(function () {
        $('#importForm').submit(function (e) {
            e.preventDefault(); // Menghentikan aksi default formulir

            var formData = new FormData(this); // Membuat objek FormData dari formulir

            $.ajax({
                url: '{{ route("matakuliah.import") }}',
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function (data) {
                    $('.modal').modal('hide');
                        $('.table').DataTable().draw();

                        // Tampilkan pesan 'success' dari response JSON
                        if (data.success) {
                            showSuccessToast(data.success); // Gunakan response JSON sebagai judul toast
                        }
                },
                error: function(data) {
                    console.log('Error:', data);
                        $('#importBtn').html('Save');
                        // Tampilkan pesan 'error' dari response JSON jika ada
                        if (data.responseJSON && data.responseJSON.error) {
                            showErrorToast(data.responseJSON.error);
                        } else {
                            // Tampilkan pesan error kustom jika tidak ada pesan error dalam response JSON
                            showErrorToast('Gagal Import Data, Mohon Cek Kembali.');
                        }
                }
            });
        });
    });

        // AJAX route for DataTables
        let routeUrl = "{{ route('matakuliah.index') }}";

        // Column configuration for DataTables
        let columns = [{
                data: 'DT_RowIndex',
                searchable: false,
                orderable: true
            },
            {
                data: 'kode_matakuliah',
                name: 'kode_matakuliah'
            },
            {
                data: 'nama_matakuliah',
                name: 'nama_matakuliah'
            },
            {
                data: 'sks',
                name: 'sks'
            },
            {
                data: 'semester',
                name: 'semester'
            },
            {
                data: 'tahun_ajaran',
                name: 'tahun_ajaran'
            },
            {
                data: 'aksi',
                name: 'aksi',
                orderable: false,
                searchable: false
            }
        ];

        let table = initializeDataTables(routeUrl, columns);

        // Mendapatkan elemen input kode matakuliah
        var kodeMatakuliahInput = document.getElementById("kode_matakuliah");

        // Menambahkan event listener untuk mengubah nilai input
        kodeMatakuliahInput.addEventListener("input", function() {
            // Mengambil nilai yang dimasukkan oleh pengguna
            var userInput = kodeMatakuliahInput.value;

            // Menghapus karakter selain angka dari input
            var numericInput = userInput.replace(/\D/g, "");

            // Menambahkan "ISB-" di depan angka
            var formattedInput = "ISB-" + numericInput;

            // Memasukkan hasil formatted ke dalam input
            kodeMatakuliahInput.value = formattedInput;
        });

        $('body').on('click', '.editData', function() {
            var id = $(this).data('id');
            $.get("{{ route('matakuliah.index') }}" + '/' + id + '/edit', function(data) {
                $('.modal-title').text('Edit Data');
                $('#modal-form').modal('show');
                $('#id').val(data.id);
                $('#nama_matakuliah').val(data.nama_matakuliah);
                $('#sks').val(data.sks);
                $('#semester').val(data.semester);
                $('#tahun_ajaran').val(data.tahun_ajaran);

                $('#kode_matakuliah').prop('readonly', true);

                // Fokus ke input kode_matakuliah saat mode edit
                $('#modal-form [name=kode_matakuliah').focus().val(data.kode_matakuliah);
            })
        });

        function addForm() {
            $("#modal-form").modal('show');
            $('#id').val('');
            $('.modal-title').text('Tambah Data');
            $('#modal-form form')[0].reset();
            $('#modal-form [name=nama_matakuliah').focus();
            $('#modal-form [name=sks').focus();
            $('#modal-form [name=semester').focus();
            $('#modal-form [name=tahun_ajaran').focus();

            $('#kode_matakuliah').prop('readonly', false);
        }

        function validation(data, isCreate) {
            let formIsValid = true;
            $('span[id^="error"]').text('');
            if (!data.nama_matakuliah) {
                formIsValid = false;
                $("#error-nama_matakuliah").text('Nama matakuliah wajib diisi.')
            }
            return formIsValid;
        }
    </script>
@endsection
