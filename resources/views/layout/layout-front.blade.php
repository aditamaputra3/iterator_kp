<!DOCTYPE html>
<html lang="en">

<head>
    <!-- head -->
    @include('component.head-user')
    <!-- head -->
</head>

<body class="d-flex flex-column">
    <main class="flex-shrink-0">
        <!-- navbar -->
        @include('component.navbar-user')
        <!-- /.navbar -->

        @yield('content')

    </main>
    <!-- Footer -->
    @include('component.footer-user')
    <!-- /.Footer -->
    @include('component.script-user')
    <!-- /.Script -->
    @yield('script')

</body>
</html>
