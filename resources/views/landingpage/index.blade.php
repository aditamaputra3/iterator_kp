@extends('layout.layout-front')

@section('content')
    <div id="carouselExampleAutoplaying" class="carousel slide carousel-fade" data-bs-ride="carousel">
        <div class="carousel-caption">
            <h1>Selamat Datang</h1>
            <p>Seluruh Praktikan Laboratorium Sistem Informasi</p>
            <a class="btn btn-warning carousel-button" href="#features">Get Started</a>
        </div>
        <div class="carousel-inner">
            <div class="carousel-item drk active">
                <img src="/images/aslab2.JPG" class="d-block w-100" alt="aslab1">
            </div>
            <div class="carousel-item drk">
                <img src="/images/aslab.JPG" class="d-block w-100" alt="aslab2">
            </div>
            {{-- <div class="carousel-item">
        <img src="..." class="d-block w-100" alt="...">
      </div> --}}
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleAutoplaying"
            data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleAutoplaying"
            data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
    
<!-- Features section -->
<section class="py-5" id="features">
    <div class="container py-5">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="fw-bolder">Tata Tertib Praktikan</h2>
                <p class="lead fw-normal text-muted mb-5">Informasi terkait tata tertib yang harus dipatuhi oleh setiap praktikan</p>
            </div>
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-3 mb-4">
                        <div class="feature-card">
                            <div class="feature-icon bg-warning text-white">
                                <i class="bi bi-clock"></i>
                            </div>
                            <h3 class="h5">Waktu</h3>
                            <p class="mb-0">Praktikan hadir tepat waktu dan mengisi presensi kehadiran</p>
                        </div>
                    </div>
                    <div class="col-md-3 mb-4">
                        <div class="feature-card">
                            <div class="feature-icon bg-warning text-white">
                                <i class="bi bi-building"></i>
                            </div>
                            <h3 class="h5">Ruangan</h3>
                            <p class="mb-0">Seluruh Praktikan diharapkan untuk menjaga kebersihan dan wajib bertanggung
                                jawab terhadap penggunaan aset lab.</p>
                        </div>
                    </div>
                    <div class="col-md-3 mb-4">
                        <div class="feature-card">
                            <div class="feature-icon bg-warning text-white">
                                <i class="bi bi-toggles2"></i>
                            </div>
                            <h3 class="h5">Pakaian</h3>
                            <p class="mb-0">Praktikan menggunakan pakaian sopan dan rapi untuk hal ini menggunakan kemeja
                                dan memakai sepatu.</p>
                        </div>
                    </div>
                    <div class="col-md-3 mb-4">
                        <div class="feature-card">
                            <div class="feature-icon bg-warning text-white">
                                <i class="bi bi-toggles2"></i>
                            </div>
                            <h3 class="h5">Perangkat</h3>
                            <p class="mb-0">Praktikan dilarang mengunduh aplikasi yang tidak berkaitan dengan Praktikum di
                                semua perangkat lab</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container px-5 my-5">
        <div class="row gx-5 justify-content-center">
            <div class="col-lg-8 col-xl-6">
                <div class="text-center">
                    <h2 class="fw-bolder">Jadwal Praktikum</h2>
                    <p class="lead fw-normal text-muted mb-5">Informasi terkait Jadwal Praktikum Hari Ini</p>
                </div>
            </div>
        </div>
        <div class="row gx-5">
            @if (count($praktikumHariIni) > 0)
                <!-- Jika ada jadwal, tampilkan data jadwal -->
                @foreach ($praktikumHariIni as $praktikum)
                    <div class="col-lg-4 mb-5 mx-auto"> <!-- Tambahkan mx-auto untuk membuatnya menjadi center -->
                        <div class="card h-100 shadow border-0">
                            <!-- Isi card dengan informasi praktikum -->
                            <div class="card-body p-4">
                                <div class="badge bg-warning bg-gradient rounded-pill mb-2">Praktikum</div>
                                <a class="text-decoration-none link-dark stretched-link" href="{{route('jadwal-praktikum.index')}}">
                                    <h5 class="card-title mb-3">{{ $praktikum->kode_matakuliah }} - {{ $praktikum->nama_matakuliah }}</h5>
                                </a>
                                <p class="card-text mb-0">Kelas: {{ $praktikum->kelas }}</p>
                                <p class="card-text mb-0">Hari: {{ $praktikum->hari }}</p>
                                <p class="card-text mb-0">Jam: {{ $praktikum->jam_mulai }} - {{ $praktikum->jam_berakhir }}</p>
                                <p class="card-text mb-0">Ruang: {{ $praktikum->ruang }}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
                <!-- Jika tidak ada jadwal, tampilkan pesan "Tidak Ada Jadwal" -->
                <div class="col-lg-4 mb-5 mx-auto"> <!-- Tambahkan mx-auto untuk membuatnya menjadi center -->
                    <div class="card h-100 shadow border-0">
                        <!-- Isi card dengan informasi praktikum -->
                        <div class="card-body p-4">
                            <div class="badge bg-warning bg-gradient rounded-pill mb-2">Praktikum</div>
                            <p class="lead">Tidak Ada Jadwal Hari Ini</p>
                        </div>
                    
                    </div>
                </div>
            @endif
            <!-- Akhir dari tempatkan data praktikum -->
        </div>
    </div>
    

    {{-- <div class="py-5 bg-light">
        <div class="container px-5 my-5">
            <div class="row gx-5 justify-content-center">
                <div class="col-lg-10 col-xl-7">
                    <div class="text-center">
                        <div class="fs-4 mb-4 fst-italic">"Sukses selalu ITENAS, terima kasih telah memberikan banyak
                            pelajaran dan perbekalan perang berharga yang mampu membuat saya berhasil mencapai cita-cita
                            yang saya impikan. “</div>
                        <div class="d-flex align-items-center justify-content-center">
                            <img class="rounded-circle me-3" src="images/wulandari.png" />
                            <div class="fw-bold">
                                Wulandari,Mgr
                                <span class="fw-bold text-warning mx-1">/</span>
                                Human Capital Data Exposure, PT Telkom Indonesia
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div class="container px-5 my-5">
            <div class="row gx-5 justify-content-center">
                <div class="col-lg-8 col-xl-6">
                    <div class="text-center">
                        <h2 class="fw-bolder">Whats New?</h2>
                        <p class="lead fw-normal text-muted mb-5">Informasi terkait Laboratorium Sistem Informasi</p>
                    </div>
                </div>
            </div>
            <div class="row gx-5">
                <div class="col-lg-4 mb-5">
                    <div class="card h-100 shadow border-0">
                        <img class="card-img-top" src="images/1.png" />
                        <div class="card-body p-4">
                            <div class="badge bg-warning bg-gradient rounded-pill mb-2">News</div>
                            <a class="text-decoration-none link-dark stretched-link" href="#!">
                                <h5 class="card-title mb-3">Nilai Praktikum</h5>
                            </a>
                            <p class="card-text mb-0">Hello praktikan,silahkan akses nilai Praktikum berikut ini.</p>
                        </div>
                        <div class="card-footer p-4 pt-0 bg-transparent border-top-0">
                            <div class="d-flex align-items-end justify-content-between">
                                <div class="d-flex align-items-center">
                                    <img class="rounded-circle me-3" src="https://dummyimage.com/40x40/ced4da/6c757d" />
                                    <div class="small">
                                        <div class="fw-bold">Kelly Rowan</div>
                                        <div class="text-muted">March 12, 2022 &middot; 6 min read</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 mb-5">
                    <div class="card h-100 shadow border-0">
                        <img class="card-img-top" src="images/aslab2.JPG" />
                        <div class="card-body p-4">
                            <div class="badge bg-warning bg-gradient rounded-pill mb-2">Media</div>
                            <a class="text-decoration-none link-dark stretched-link" href="#!">
                                <h5 class="card-title mb-3">Meet The Assistant Laboratorium</h5>
                            </a>
                            <p class="card-text mb-0">Well hello there,we'are the Assistant for Class 2022/2023.</p>
                        </div>
                        <div class="card-footer p-4 pt-0 bg-transparent border-top-0">
                            <div class="d-flex align-items-end justify-content-between">
                                <div class="d-flex align-items-center">
                                    <img class="rounded-circle me-3" src="https://dummyimage.com/40x40/ced4da/6c757d"
                                        alt="..." />
                                    <div class="small">
                                        <div class="fw-bold">Josiah Barclay</div>
                                        <div class="text-muted">March 23, 2022 &middot; 4 min read</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 mb-5">
                    <div class="card h-100 shadow border-0">
                        <img class="card-img-top" src="images/2.png" />
                        <div class="card-body p-4">
                            <div class="badge bg-warning bg-gradient rounded-pill mb-2">News</div>
                            <a class="text-decoration-none link-dark stretched-link" href="#!">
                                <h5 class="card-title mb-3">Nilai Praktikum</h5>
                            </a>
                            <p class="card-text mb-0">Best Of Luck Fellas!</p>
                        </div>
                        <div class="card-footer p-4 pt-0 bg-transparent border-top-0">
                            <div class="d-flex align-items-end justify-content-between">
                                <div class="d-flex align-items-center">
                                    <img class="rounded-circle me-3" src="https://dummyimage.com/40x40/ced4da/6c757d"
                                        alt="..." />
                                    <div class="small">
                                        <div class="fw-bold">Evelyn Martinez</div>
                                        <div class="text-muted">April 2, 2022 &middot; 10 min read</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}
           
        </div>
    </section>
@endsection
