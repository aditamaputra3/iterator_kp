@extends('layout.layout-admin')

@section('title','Error 403')

@section('content')

        <section class="content">
            <div class="error-page">
                <h2 class="headline text-warning"> 403</h2>
                <div class="error-content">
                    <h3><i class="fas fa-exclamation-triangle text-warning"></i> Oops! Akses Ditolak.</h3>
                    <p>
                        Anda tidak memiliki izin untuk mengakses resource ini
                        <a href="dashboard">Kembali ke dashboard</a>
                    </p>
                </div>

            </div>

        </section>
@endsection
