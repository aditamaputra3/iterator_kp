@extends('layout.layout-admin')

@section('title','Data Belum Tersedia')

@section('content')

        <section class="content">
            <div class="error-page">
                <h2 class="headline text-warning"></h2>
                <div class="error-content">
                    <h3><i class="fas fa-exclamation-triangle text-warning"></i> Oops! Data Belum Tersedia, Mohon hubungi teknisi prodi</h3>
                    <p>
                        <a href="dashboard">Kembali ke dashboard</a>
                    </p>
                </div>

            </div>

        </section>
@endsection
