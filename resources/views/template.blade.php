<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <link rel="shortcut icon" type="image/x-icon" href="sip-logo.png">
    <title>Admin Panel</title>
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
    {{-- Felia Notes :
    aku nyisipin csrf token disini buat nanti dipake ngehit data di javascript
    btw komentar ini boleh dihapus kalo mengganggu    
    --}}
    <link rel="manifest" href="/manifest.json">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{-- Felia Notes :
    coba kalian cari tau tehnik laravel template, biar kodenya ga sepanjang ini jadi bisa lebih pendek ngodingnya
    itu bakal ngambil waktu banyak di awal tapi kedepannya ngoding bisa cepet karna kode jadi lebih sedikit    

    tapi ngambil waktu di awalnya juga ga akan lama sih
    
    oh iya btw komentar ini boleh dihapus kalo mengganggu
    --}}
    @yield('head')
</head>

<body>
    <div class="main-wrapper">
        <div class="header">
            <div class="header-left">
                <a href="/" class="logo">
                    <img src="../images/logo.png" width="85" height="85" alt="">
                    <span>ITERATOR</span>
                </a>
            </div>
            <a id="toggle_btn" href="javascript:void(0);">
                <i class="fa fa-bars"></i>
            </a>
            <a id="mobile_btn" class="mobile_btn float-left" href="#sidebar">
                <i class="fa fa-bars"></i>
            </a>
            <ul class="nav user-menu float-right">
                <li class="nav-item dropdown has-arrow">
                    <a href="#" class="dropdown-toggle nav-link user-link" data-toggle="dropdown">
                        <span class="user-img">
                            <img class="rounded-circle" src="/assets/img/user.jpg" width="40" alt="Admin">
                            <span class="status online"></span>
                        </span>
                        <span>{{ Session::get('nama_lengkap') }} </span>
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{ url('/logout') }}">Logout</a>
                    </div>
                </li>
            </ul>
            <div class="dropdown mobile-user-menu float-right">
                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-ellipsis-v"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="{{ url('/my-profile') }}">My Profile</a>
                    <a class="dropdown-item" href="{{ url('/logout') }}">Logout</a>
                </div>
            </div>
        </div>
        <div class="sidebar" id="sidebar">
            <div class="sidebar-inner slimscroll">
                <div id="sidebar-menu" class="sidebar-menu">
                    <ul>
                        <li class="menu-title">Main</li>
                        {{-- <li class="{{(Request::path() === 'dashboard') ? "active" : "" }}">
                <a href="{{url("/dashboard")}}">
                  <i class="fa fa-dashboard"></i>
                  <span>Dashboard</span>
                </a>
              </li> --}}
                        <li class="{{ Request::path() === 'pemasok' ? 'active' : '' }}">
                            <a href="{{ url('/pemasok') }}">
                                <i class="fa fa-truck"></i>
                                <span>Pemasok</span>
                            </a>
                        </li>
                        <li class="{{ Request::path() === 'detail-kondisi' ? 'active' : '' }}">
                            <a href="{{ url('/detail-kondisi') }}">
                                <i class="fa fa-book"></i>
                                <span> Detail Kondisi</span>
                            </a>
                        </li>
                        <li
                            class="{{ in_array(Request::path(), ['kategori', 'inventory', 'tambah-barang', 'ubah-barang', 'detail-barang']) ? 'active' : '' }}">
                            <a href="{{ url('/kategori') }}">
                                <i class="fa fa-cart-plus"></i>
                                <span> Inventory</span>
                            </a>
                        </li>
                        @if (Session::has('peran_pengguna'))
                            @if (Session::get('peran_pengguna') === 'TEKNISI')
                                <li class="{{ Request::path() === 'pengguna' ? 'active' : '' }}">
                                    <a href="{{ url('/pengguna') }}">
                                        <i class="fa fa-user"></i>
                                        <span> Data Pengguna </span>
                                    </a>
                                </li>
                            @endif
                        @endif
                    </ul>
                </div>
            </div>
        </div>
        <div class="page-wrapper">
            <div class="content">
                @yield('content')
            </div>
        </div>
    </div>
    <div class="sidebar-overlay" data-reff=""></div>
    <script src="/assets/js/jquery-3.2.1.min.js"></script>
    <script src="/assets/js/popper.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/jquery.slimscroll.js"></script>
    <script src="/assets/js/select2.min.js"></script>
    <script src="/assets/js/jquery.dataTables.min.js"></script>
    <script src="/assets/js/dataTables.bootstrap4.min.js"></script>
    <script src="/assets/js/moment.min.js"></script>
    <script src="/assets/js/bootstrap-datetimepicker.min.js"></script>
    <script src="/assets/js/app.js"></script>
    @yield('script')
</body>

</html>
