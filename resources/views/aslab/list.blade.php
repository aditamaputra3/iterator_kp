@extends('layout.layout-admin')

@section('title')
    {{ 'Asisten Lab' }}
@endsection

@section('content')
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="modal-form">
        <div class="modal-dialog" role="document">
            <form class="form-horizontal">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title"></h4>
                        <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="id" id="id" class="form-control">
                        <div class="form-group">
                            <label for="name">NRP</label>
                            <input type="number" name="nrp" id="nrp" class="form-control" autofocus>
                            <span class="text-danger" id="error-nrp"></span>
                        </div>
                        <div class="form-group">
                            <label for="name">Nama Asisten Lab</label>
                            <select name="id_user" id="id_user" class="form-control" required>
                                <option>-- Pilih Asisten Lab--</option>
                                @foreach ($user as $id => $nama_lengkap)
                                    <option value="{{ $id }}">{{ $nama_lengkap }}
                                    </option>
                                @endforeach
                            </select>
                            <span class="text-danger" id="error-nama"></span>
                        </div>
                        <div class="form-group">
                            <label for="name">No Telepon</label>
                            <input name="no_telp" type="no_telp" id="no_telp" class="form-control" placeholder="Kosongkan">
                            <span class="text-danger" id="error-notelp"></span>
                        </div>
                        <div class="form-group">
                            <label for="name">No Rekening</label>
                            <input name="no_rekening" type="no_rekening" id="no_rekening" class="form-control" placeholder="Kosongkan">
                            <span class="text-danger" id="error-norek"></span>
                        </div>
                        <div class="form-group">
                            <label for="name">Nama Bank</label>
                            <input name="nama_bank" type="nama_bank" id="nama_bank" class="form-control" placeholder="Kosongkan">
                            <span class="text-danger" id="error-nama-bank"></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-warning close-btn" data-dismiss="modal"><i
                                class="fa fa-arrow-circle-left"></i> Cancel</button>
                        <button type="submit" class="btn btn-sm btn-primary" id="saveBtn"><i class="fa fa-save"></i>
                            Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-outline card-warning">
                        <div class="card-body">
                            <div class="d-flex justify-content-between mb-4">
                                <div class="d-flex justify-content-start" id="print">
                                    <!-- Second div content (if any) -->
                                </div>
                                <div class="d-flex justify-content-end">
                                    <button type="button" onclick="addForm()" class="btn btn-warning">
                                        <i class="fa fa-plus"></i> Tambah Asisten Lab
                                    </button>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped custom-table mb-0 no-footer" role="grid">
                                    <thead>
                                        <tr role="row">
                                            <th>#</th>
                                            <th>NRP</th>
                                            <th>Nama Asisten Lab</th>
                                            <th>No Telp</th>
                                            <th>No Rekening</th>
                                            <th>Nama Bank</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        let routeUrl = "{{ route('aslab.index') }}";

            let columns = [{
                    data: 'DT_RowIndex',
                    searchable: false,
                    orderable: true,
                },
                {
                    data: 'nrp',
                    name: 'nrp',
                },
                {
                    data: 'nama_lengkap',
                    name: 'nama_lengkap',
                },
                {
                    data: 'no_telp',
                    name: 'no_telp',
                },
                {
                    data: 'no_rekening',
                    name: 'no_rekening',
                },
                {
                    data: 'nama_bank',
                    name: 'nama_bank',
                },
                
                {
                    data: 'aksi',
                    name: 'aksi',
                    orderable: false,
                    searchable: false
                }
             ];

            let table = initializeDataTables(routeUrl, columns);


        $('body').on('click', '.editData', function() {
            var id = $(this).data('id');
            $.get("{{ route('aslab.index') }}" + '/' + id + '/edit', function(data) {
                $('.modal-title').text('Edit Data');
                $('#modal-form').modal('show');
                $('#id').val(data.id);

                $('#id_user').val(data.id_user);
                $('#no_telp').val(data.no_telp);
                $('#no_rekening').val(data.no_rekening);
                $('#nama_bank').val(data.nama_bank);
                 // Tampilkan form kode_praktikum pada mode edit
                 $('#nrp').prop('readonly', true).val(data.nrp);
            })
        });

        function addForm() {
            $("#modal-form").modal('show');
            $('#id').val('');
            $('.modal-title').text('Tambah Data');
            $('#modal-form form')[0].reset();
            $('#modal-form [name=nrp').focus();
            $('#modal-form [name=id_user').focus();
            $('#modal-form [name=no_telp').focus();
            $('#modal-form [name=no_rekening').focus();
            $('#modal-form [name=nama_bank').focus();

              // Tampilkan form kode_praktikum pada mode edit
              $('#nrp').prop('readonly', false).val(data.nrp);
        }

        function validation(data, isCreate) {
            let formIsValid = true;
            $('span[id^="error"]').text('');
            if (!data.nrp) {
                formIsValid = false;
                $("#error-nrp").text('NRP aslab wajib diisi.')
            }
            return formIsValid;
        }
    </script>
@endsection
