@extends('layout.layout-admin')

@section('title')
    {{ 'Inventory' }}
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-outline card-warning">
                        <div class="card-body">
                            <div class="d-flex justify-content-between mb-4">
                                <div class="d-flex justify-content-start">
                                    <button type="button" onclick="addForm()" class="btn btn-warning">
                                        <i class="fa fa-plus"></i> Tambah Kategori
                                    </button>
                                </div>
                                <div class="d-flex justify-content-end" id="print">
                                    <!-- Second div content (if any) -->
                                </div>

                            </div>
                            <div class="row doctor-grid" id="list-data">
                                <div class="col-lg-12">
                                    <h1 class="text-center mt-4">LOADING....</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    </section>
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="modal-form">
        <div class="modal-dialog" role="document">
            <form class="form-horizontal">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title"></h4>
                        <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="id" id="id" class="form-control">
                        <div class="form-group">
                            <label for="name">Nama Kategori</label>
                            <input type="text" name="nama_kategori_barang" id="nama_kategori_barang" class="form-control"
                                autofocus>
                            <span class="text-danger" id="error-nama_kategori_barang"></span>
                        </div>
                        <div class="form-group">
                            <label for="name">Deskripsi Kategori</label>
                            <textarea name="deskripsi_kategori_barang" id="deskripsi_kategori_barang" rows="5" class="form-control"></textarea>
                            <span class="text-danger" id="error-deskripsi_kategori_barang"></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-sm btn-flat btn-primary" id="saveBtn">
                            <i class="fa fa-save"></i> Save
                        </button>
                        <button type="button" class="btn btn-sm btn-flat btn-warning close-btn" data-dismiss="modal">
                            <i class="fa fa-arrow-circle-left"></i> Cancel
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection


@section('script')
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            loadData();
        });

        const loadData = () => {
            $.ajax({
                url: "{{ url('/kategori') }}",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    if (data.length > 0) {
                        let categoryBox = "";
                        data.forEach((kategori) => {
                            categoryBox += /*html*/ `
                            <div class="col-md-4 col-sm-4 col-lg-3">
                            <div class="info-box">
                            <div style="position: absolute; top: 10px; right: 10px;">
                                <a href="#" data-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-ellipsis-v"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                <button type="button" class="dropdown-item editData" data-id="${kategori.id}">
                                    <i class="fa fa-pencil m-r-5"></i> Edit
                                </button>
                                <button type="button" class="dropdown-item deleteData" data-id="${kategori.id}">
                                    <i class="fa fa-trash-o m-r-5"></i> Delete
                                </button>
                                </div>
                            </div>
                            <div class="info-box-content">
                            <a href="{{ url('/inventory?kategori=') }}${kategori.id}">
                            <span class="info-box-text">
                                <h4>
                                ${kategori.nama_kategori_barang}
                                </h4>
                            </span>
                            <span class="info-box-text">
                                ${kategori.deskripsi_kategori_barang}</span>
                            </a>
                            </div>
                        </div>
                        </div>`;
                        })
                        $("#list-data").html(categoryBox);
                    } else {
                        $("#list-data").html(
                            `<div class="col-lg-12"><h1 class="text-center mt-4">NO DATA</h1></div>`);
                    }
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        }

        $('.close-btn').click(function(e) {
            $('.modal').modal('hide')
        });

        $("#modal-form form").on("submit", function(e) {
            e.preventDefault();
            var formdata = $(this).serializeArray();
            var data = {};

            $(formdata).each(function(index, obj) {
                data[obj.name] = obj.value;
            });

            if (validation(data, true)) {
                $.ajax({
                    data: $(this).serialize(),
                    url: "{{ route('kategori.store') }}",
                    type: "POST",
                    dataType: 'json',
                    success: function(data) {
                        $('#modal-form').modal('hide');
                        // Tampilkan pesan 'success' dari response JSON
                        if (data.success) {
                            showSuccessToast(data.success); // Gunakan response JSON sebagai judul toast
                        }
                        loadData();

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        if (data.error) {
                            showErrorToast(data.error);
                        } else {
                            // Tampilkan pesan error kustom jika tidak ada pesan error dalam response JSON
                            showErrorToast('An error occurred while delete the data.');
                        }
                        $('#saveBtn').html('Save Changes');
                    }
                });
            }
        })

        $('body').on('click', '.editData', function() {
            var id = $(this).data('id');
            $.get("{{ url('/kategori') }}" + '/' + id + '/edit', function(data) {
                $('.modal-title').text('Edit Data');
                $('#modal-form').modal('show');
                $('#id').val(data.id);
                $('#nama_kategori_barang').val(data.nama_kategori_barang);
                $('#deskripsi_kategori_barang').val(data.deskripsi_kategori_barang);
            })
        });

        $('body').on('click', '.deleteData', function() {
            var id = $(this).data("id");
            if (confirm("Kamu yakin ingin menghapus data?") == true) {
                $.ajax({
                    type: "DELETE",
                    url: "{{ route('kategori.store') }}" + '/' + id,

                    success: function(data) {
                        loadData();
                        // Tampilkan pesan 'success' dari response JSON
                        if (data.success) {
                            showSuccessToast(data.success); // Gunakan response JSON sebagai judul toast
                        }
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Save Changes');
                        // Tampilkan pesan 'error' dari response JSON jika ada
                        if (data.error) {
                            showErrorToast(data.error);
                        } else {
                            // Tampilkan pesan error kustom jika tidak ada pesan error dalam response JSON
                            showErrorToast('An error occurred while delete the data.');
                        }
                    }
                });
            }
        });

        function addForm() {
            $("#modal-form").modal('show');
            $('#id').val('');
            $('.modal-title').text('Tambah Data');
            $('#modal-form form')[0].reset();
            $('#modal-form [name=nama_kategori_barang').focus();
            $('#modal-form [name=deskripsi_kategori_barang').focus();
        }

        function validation(data, isCreate) {
            let formIsValid = true;
            $('span[id^="error"]').text('');
            if (!data.nama_kategori_barang) {
                formIsValid = false;
                $("#error-nama_kategori_barang").text('Kategori barang wajib diisi.')
            }

            if (!data.nama_kategori_barang) {
                formIsValid = false;
                $("#error-nama_kategori_barang").text('Kategori barang wajib diisi.')
            }
            return formIsValid;
        }
    </script>
@endsection
