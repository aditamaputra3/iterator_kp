@extends('layout.layout-admin')

@section('title')
    {{ 'Master Barang' }}
@endsection

@section('content')
    {{-- <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="modal-form">
        <div class="modal-dialog" role="document">
            <form class="form-horizontal">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title"></h4>
                        <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="id" id="id" class="form-control">
                        <div class="form-group">
                            <label for="name">Nama Lengkap</label>
                            <input type="text" name="nama_lengkap" id="nama_lengkap" class="form-control" autofocus>
                            <span class="text-danger" id="error-nama_lengkap"></span>
                        </div>
                        <div class="form-group">
                            <label for="name">Nama Pengguna</label>
                            <input type="text" name="nama_pengguna" id="nama_pengguna" class="form-control" autofocus>
                            <span class="text-danger" id="error-nama_pengguna"></span>
                        </div>
                        <div class="form-group">
                            <label for="name">Sandi</label>
                            <input type="password" name="sandi_pengguna" id="sandi_pengguna" class="form-control" autofocus>
                            <span class="text-danger" id="error-sandi_pengguna"></span>
                        </div>
                        <div class="form-group">
                            <label for="name">Peran Pengguna</label>
                            <select name="peran_pengguna" id="peran_pengguna" class="form-control" required>
                                <option value="MAHASISWA">MAHASISWA</option>
                                <option value="KEPALA">KEPALA</option>
                                <option value="ASISTEN">ASISTEN</option>
                                <option value="TEKNISI">TEKNISI</option>
                            </select>
                            <span class="text-danger" id="error-peran_pengguna"></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-sm btn-flat btn-primary" id="saveBtn"><i
                                class="fa fa-save"></i> Save</button>
                        <button type="button" class="btn btn-sm btn-flat btn-warning close-btn" data-dismiss="modal"><i
                                class="fa fa-arrow-circle-left"></i> Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div> --}}

    <section class="content-header">
        <a href="{{ url('kategori') }}">
            <h5 class="mb-2" style="color: #565656">
                <i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Kembali
            </h5>
        </a>
    
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>@yield('title') - {{ $kategori->nama_kategori_barang }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="kategori">{{ $kategori->nama_kategori_barang }}</a></li>
                        <li class="breadcrumb-item active">@yield('title')</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-outline card-warning">
                        <div class="card-body">
                            <div class="d-flex justify-content-between mb-4">
                                <div class="d-flex justify-content-start" id="print">
                                    <!-- Second div content (if any) -->
                                </div>
                                <div class="d-flex justify-content-end">
                                    <a href="{{ url("tambah-barang?kategori=$kategori->id") }}"
                                        class="btn btn-warning">
                                        <i class="fa fa-plus"></i> Tambah Barang Baru
                                    </a>
                                </div>

                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped custom-table mb-0 no-footer" role="grid">
                                    <thead>
                                        <tr role="row">
                                            <th>#</th>
                                            <th>Nama Barang</th>
                                            <th>Harga</th>
                                            <th>Merk</th>
                                            <th>Gambar</th>
                                            <th>Kuantitas</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });

        let table = $('.table').DataTable({
          processing: true,
            autoWidth: false,
            responsive: true,
            lengthChange: true,
            processing: true,
            serverSide: true,
            dom: "<'row'<'col-sm-3'l><'col-sm-6 text-center'B><'col-sm-3'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            buttons: [{
                extend: 'collection',
                text: '<i class="fa fa-print"></i>  Export',
                className: 'btn btn-success',
                buttons: [{
                    extend: 'excel',
                    title: 'excel'
                }, {
                    extend: 'pdf',
                    title: 'Pdf '
                }, {
                    extend: 'csv',
                    title: 'Csv '
                }, {
                    extend: 'print',
                    title: 'print '
                }, {
                    extend: 'copy',
                    title: 'copy'
                }]
            }],
            ajax: "{{ route('inventory.index') . '?kategori=' . request()->get('kategori') }}",
            columns: [{
                    data: 'DT_RowIndex',
                    searchable: false,
                    orderable: false,
                },
                {
                    data: 'nama_barang',
                    name: 'nama_barang',
                },
                {
                    data: 'harga',
                    name: 'harga',
                },
                {
                    data: 'merk',
                    name: 'merk',
                },
                {
                    data: 'gambar_barang',
                    name: 'gambar_barang',
                    render: function(data, type, full, meta) {
                        if (data) {
                            return `<img src="{{ asset('storage/${data}') }}" alt="Image" width="50">`;
                        } else {
                            return '-';
                        }
                    }
                },
                {
                    data: 'kuantitas',
                    name: 'kuantitas',
                },
                {
                    data: 'aksi',
                    name: 'aksi',
                    orderable: false,
                    searchable: false
                }
            ]
          }).buttons().container().appendTo('#print');

        // $('.close-btn').click(function(e) {
        //     $('.modal').modal('hide')
        // });

        $("#modal-form form").on("submit", function(e) {
            e.preventDefault();
            var formdata = $(this).serializeArray();
            var data = {};

            $(formdata).each(function(index, obj) {
                data[obj.name] = obj.value;
            });

            if (validation(data, true)) {
                $.ajax({
                    data: $(this).serialize(),
                    url: "{{ route('pengguna.store') }}",
                    type: "POST",
                    dataType: 'json',
                    success: function(data) {
                        $('#modal-form').modal('hide');
                        $('.table').DataTable().draw();
                        showSuccessToast();
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Save Changes');
                        showErrorToast();
                    }
                });
            }
        })

        $('body').on('click', '.editData', function() {
            var id = $(this).data('id');
            $.get("{{ route('pengguna.index') }}" + '/' + id + '/edit', function(data) {
                $('.modal-title').text('Edit Data');
                $('#modal-form').modal('show');
                $('#id').val(data.id);
                $('#nama_lengkap').val(data.nama_lengkap);
                $('#nama_pengguna').val(data.nama_pengguna);
                $('#sandi_pengguna').val("");
                $('#peran_pengguna').val(data.peran_pengguna);
            })
        });

        $('body').on('click', '.deleteData', function() {
            var id = $(this).data("id");
            if (confirm("Kamu yakin ingin menghapus data?") == true) {
                $.ajax({
                    type: "DELETE",
                    url: "{{ url('delete-barang') }}" + '/' + id,
                    success: function(data) {
                        $('.table').DataTable().draw();
                        showDeleteToast();
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        showErrorToast();
                    }
                });
            }
        });

        function addForm() {
            $("#modal-form").modal('show');
            $('#id').val('');
            $('.modal-title').text('Tambah Data');
            $('#modal-form form')[0].reset();
            $('#modal-form [name=nama_lengkap').focus();
            $('#modal-form [name=nama_pengguna').focus();
            $('#modal-form [name=sandi_pengguna').focus();
            $('#modal-form [name=peran_pengguna').focus();
        }

        function validation(data, isCreate) {
            let formIsValid = true;
            $('span[id^="error"]').text('');
            if (!data.nama_lengkap) {
                formIsValid = false;
                $("#error-nama_lengkap").text('Nama lengkap wajib diisi.')
            }

            if (!data.nama_pengguna) {
                formIsValid = false;
                $("#error-nama_pengguna").text('Nama pengguna wajib diisi.')
            }

            if (!!isCreate) {
                // if (!data.sandi_pengguna) {
                //   formIsValid = false;
                //   $("#error-sandi_pengguna").text('Sandi pengguna wajib diisi.')
                // }
            }
            return formIsValid;
        }
    </script>
@endsection
