<!DOCTYPE html>
<html lang="en">

<head>
    <!-- head -->
    @include('component.head')
    {{-- @yield('head') --}}
    <!-- head -->
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
        <!-- navbar -->
        @include('component.navbar-admin')
        <!-- /.navbar -->

        <!-- sidebar -->
        @include('component.sidebar')
        <!-- /.sidebar -->

        <!-- Content -->
        <div class="content-wrapper" style="min-height: 2171.31px;">

            <section class="content-header">
                <a
                    href="{{ request()->get('source') === 'detail' ? url("detail-barang/$master_barang->id") : url("inventory?kategori=$kategori->id") }}">
                    <h5 class="mb-2" style="color: #565656">
                        <i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Kembali
                    </h5>
                </a>

                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>@yield('title') - {{ $kategori->nama_kategori_barang }}</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item"><a href="kategori">{{ $kategori->nama_kategori_barang }}</a>
                                </li>
                                <li class="breadcrumb-item"><a
                                        href="{{ url("inventory?kategori=$kategori->id") }}">Master
                                        Barang</a></li>
                                <li class="breadcrumb-item active">@yield('title')</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card card-outline card-warning">
                                <div class="card-body">
                                    <form class="form-horizontal" id="form-input">
                                        <input type="hidden" name="id" id="id" class="form-control"
                                            value="{{ $master_barang->id }}">
                                        <input type="hidden" name="id_kategori_barang" id="id_kategori_barang"
                                            value="{{ $master_barang->id_kategori_barang }}" class="form-control">
                                        <div class="form-group">
                                            <label for="name">Nama Barang</label>
                                            <input type="text" name="nama_barang" id="nama_barang"
                                                class="form-control" autofocus
                                                value="{{ $master_barang->nama_barang }}">
                                            <span class="text-danger" id="error-nama_barang"></span>
                                        </div>
                                        <div class="form-group">
                                            <label for="name">Pemasok</label>
                                            <select name="id_pemasok" id="id_pemasok" class="form-control" required>
                                                <option>-- PILIH PEMASOK --</option>
                                                @foreach ($options_pemasok as $id => $nama)
                                                    <option value="{{ $id }}"
                                                        {{ $id == $master_barang->id_pemasok ? 'selected' : '' }}>
                                                        {{ $nama }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            <span class="text-danger" id="error-id_pemasok"></span>
                                        </div>
                                        <div class="form-group">
                                            <label for="name">Merk</label>
                                            <input type="text" name="merk" id="merk" class="form-control"
                                                autofocus value="{{ $master_barang->merk }}">
                                            <span class="text-danger" id="error-merk"></span>
                                        </div>
                                        <div class="form-group">
                                            <label for="name">Harga</label>
                                            <input type="number" name="harga" id="harga" class="form-control"
                                                autofocus value="{{ $master_barang->harga }}">
                                            <span class="text-danger" id="error-harga"></span>
                                        </div>
                                        <div class="form-group">
                                            <label for="name">Spesifikasi</label>
                                            <textarea name="spesifikasi" id="spesifikasi" class="form-control" rows="2">{{ $master_barang->spesifikasi }}</textarea>
                                            <span class="text-danger" id="error-spesifikasi"></span>
                                        </div>
                                        <div class="form-group">
                                            <label for="name">Tahun Masuk</label>
                                            <input type="month" name="tahun_masuk" id="tahun_masuk"
                                                class="form-control" autofocus
                                                value="{{ $master_barang->tahun_masuk }}">
                                            <span class="text-danger" id="error-tahun_masuk"></span>
                                        </div>
                                        <div class="form-group">
                                            <label for="name">Deskripsi</label>
                                            <textarea name="deskripsi_barang" id="deskripsi_barang" class="form-control" rows="4">{{ $master_barang->deskripsi_barang }}</textarea>
                                            <span class="text-danger" id="error-deskripsi_barang"></span>
                                        </div>
                                        <div class="form-group">
                                            <label for="name">Gambar</label>
                                            <div class="resitdc-image-choose">
                                                <input type="file" name="gambar_barang" id="gambar_barang"
                                                    class="resitdc-image-choose-input"
                                                    accept="image/.bmp, image/.png, image/.jpg, image/.jpeg">
                                                <div class="resitdc-image-choose-preview"
                                                    style="background-image: url('{{ asset("storage/$master_barang->gambar_barang") }}')">
                                                </div>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-md btn-flat btn-primary">
                                            <i class="fa fa-save"></i> Save
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Footer -->
            @include('component.footer')
            <!-- /.Footer -->

            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
            </aside>
            <!-- /.control-sidebar -->
        </div>

        <!-- jQuery -->
        <script src="/assets/plugins/jquery/jquery.min.js"></script>
        <!-- Bootstrap 4 -->
        <script src="/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- Select2 -->
        <script src="/assets/plugins/select2/js/select2.full.min.js"></script>
        <script src="https://kit.fontawesome.com/198ace4666.js" crossorigin="anonymous"></script>
        <!-- InputMask -->
        <script src="/assets/plugins/moment/moment.min.js"></script>
        <script src="/assets/plugins/inputmask/jquery.inputmask.min.js"></script>
        <!-- Tempusdominus Bootstrap 4 -->
        <script src="/assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
        <!-- Bootstrap -->
        <script src="/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- DataTables  & Plugins -->
        <script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
        <script src="/assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
        <script src="/assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
        <script src="/assets/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
        <script src="/assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
        <script src="/assets/plugins/jszip/jszip.min.js"></script>
        <script src="/assets/plugins/pdfmake/pdfmake.min.js"></script>
        <script src="/assets/plugins/pdfmake/vfs_fonts.js"></script>
        <script src="/assets/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
        <script src="/assets/plugins/datatables-buttons/js/buttons.print.min.js"></script>
        <script src="/assets/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
        <!-- overlayScrollbars -->
        <script src="/assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
        <!-- AdminLTE App -->
        <script src="/assets/dist/js/adminlte.js"></script>
        <!-- PAGE PLUGINS -->
        <!-- jQuery Mapael -->
        <script src="/assets/plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
        <script src="/assets/plugins/raphael/raphael.min.js"></script>
        <script src="/assets/plugins/jquery-mapael/jquery.mapael.min.js"></script>
        <script src="/assets/plugins/jquery-mapael/maps/usa_states.min.js"></script>
        <!-- ChartJS -->
        <script src="/assets/plugins/chart.js/Chart.min.js"></script>
        <!-- SweetAlert2 -->
        <script src="/assets/plugins/sweetalert2/sweetalert2.min.js"></script>
        <!-- Toastr -->
        <script src="/assets/plugins/toastr/toastr.min.js"></script>

        {{-- <script>
$(function () {
//Initialize Select2 Elements
$('.select2').select2()
$(this).select2({
  dropdownParent: $(this).parent(),
});

//Initialize Select2 Elements
// $('.select2bs4').select2({
//   theme: 'bootstrap4'
// })
})
</script> --}}
        <script>
            /*** add active class and stay opened when selected ***/
            var url = window.location;

            // for sidebar menu entirely but not cover treeview
            $('ul.nav-sidebar a').filter(function() {
                if (this.href) {
                    return this.href == url || url.href.indexOf(this.href) == 0;
                }
            }).addClass('active');

            // for the treeview
            $('ul.nav-treeview a').filter(function() {
                if (this.href) {
                    return this.href == url || url.href.indexOf(this.href) == 0;
                }
            }).parentsUntil(".nav-sidebar > .nav-treeview").addClass('menu-open').prev('a').addClass('active');
        </script>

        <script>
            function showSuccessToast(message) {
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000
                });

                Toast.fire({
                    icon: 'success',
                    title: message // Menggunakan pesan dari parameter fungsi sebagai judul
                });
            }

            // Fungsi untuk menampilkan toast SweetAlert error
            function showErrorToast(message) {
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000
                });

                Toast.fire({
                    icon: 'error',
                    title: message
                });
            }
        </script>
        <script>
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $(".resitdc-image-choose .resitdc-image-choose-input").change(function() {
                    const file = this.files[0];
                    if (file) {
                        const reader = new FileReader();
                        reader.onload = function(e) {
                            $(".resitdc-image-choose .resitdc-image-choose-preview").css("background-image",
                                `url('${e.target.result}')`);
                        }
                        reader.readAsDataURL(file);
                    } else {
                        $(".resitdc-image-choose .resitdc-image-choose-preview").removeAttr("style");
                    }
                });

                $('#form-input').on('submit', function(e) {
                    e.preventDefault();
                    const form = $(this);
                    const formData = new FormData(form[0]);
                    $.ajax({
                        url: '{{ url('tambah-barang/save') }}',
                        type: 'POST',
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function(response) {
                            alert("SUCCESS");
                            window.location =
                                '{{ request()->get('source') === 'detail' ? url("detail-barang/$master_barang->id") : url("inventory?kategori=$kategori->id") }}';
                        },
                        error: function(xhr, status, error) {
                            alert("ERROR")
                        }
                    });
                });
            });
        </script>
</body>

</html>
