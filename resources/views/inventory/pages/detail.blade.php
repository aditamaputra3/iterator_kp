<!DOCTYPE html>
<html lang="en">

<head>
    <!-- head -->
    @include('component.head')
    {{-- @yield('head') --}}
    <!-- head -->
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
        <!-- navbar -->
        @include('component.navbar-admin')
        <!-- /.navbar -->

        <!-- sidebar -->
        @include('component.sidebar')
        <!-- /.sidebar -->

        <!-- Content -->
        <div class="content-wrapper" style="min-height: 2171.31px;">
           
            <section class="content-header">
                <a href="{{ url("inventory?kategori=$master_barang->id_kategori_barang") }}">
                    <h5 class="mb-2" style="color: #565656">
                        <i class="fa fa-chevron-left"></i> Kembali
                    </h5>
                </a>
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-8">
                            <h4 class="page-title">Detail Barang - {{ $master_barang->nama_barang }}</h4>
                        </div>
                        {{-- <div class="col-4 text-right">
                            <a href="{{ url("edit-barang/$master_barang->id?source=detail") }}" class="btn btn-sm btn-success px-2">
                                <i class="fa fa-edit mr-2"></i>Edit
                            </a>
                        </div> --}}
                    </div>
                </div><!-- /.container-fluid -->
            </section>
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card card-outline card-warning">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-5 col-sm-12 mb-4">
                                            @if (!empty($master_barang->gambar_barang))
                                                <img src="{{ asset("storage/$master_barang->gambar_barang") }}" alt="Gambar"
                                                    class="img-fluid">
                                            @else
                                                <h3 class="text-center mt-5">NO IMAGE</h3>
                                            @endif
                                        </div>
                                        <div class="col-md-7 col-sm-12">
                                            <ul class="list-group">
                                                <li class="list-group-item"><b>Nama Barang :</b> {{ $master_barang->nama_barang }}
                                                </li>
                                                <li class="list-group-item"><b>Kategori :</b>
                                                    {{ $master_barang->nama_kategori_barang }}</li>
                                                <li class="list-group-item"><b>Pemasok :</b> {{ $master_barang->pemasok }}</li>
                                                <li class="list-group-item"><b>Harga :</b> {{ $master_barang->harga }}</li>
                                                <li class="list-group-item"><b>Spesifikasi :</b> {{ $master_barang->spesifikasi }}
                                                </li>
                                                <li class="list-group-item"><b>Merk :</b> {{ $master_barang->merk }}</li>
                                                <li class="list-group-item"><b>Tahun Masuk :</b> {{ $master_barang->tahun_masuk }}
                                                </li>
                                                <li class="list-group-item"><b>Deskripsi :</b>
                                                    {{ $master_barang->deskripsi_barang }}</li>
                                                <li class="list-group-item"><b>Kuantitas :</b> {{ $master_barang->kuantitas }}</li>
                                            </ul>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="row mt-5">
                                                <div class="col-5">
                                                    <h3 class="mb-3">Detail Barang</h3>
                                                </div>
                                                <div class="col-7 text-right">
                                                    <button type="button" class="btn btn-sm btn-primary" onclick="addForm()">
                                                        <i class="fa fa-plus"></i> TAMBAH KUANTITAS
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="table-responsive">
                                                <table class="table custom-table mb-0 no-footer" role="grid">
                                                    <thead>
                                                        <tr role="row">
                                                            <th>#</th>
                                                            <th>Status</th>
                                                            <th>Kuantitas</th>
                                                            <th>Nama Kondisi</th>
                                                            <th>Keterangan Kondisi</th>
                                                            <th>Aksi</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        
            <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="modal-form">
                <div class="modal-dialog" role="document">
                    <form class="form-horizontal">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title"></h4>
                                <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="name">Kuantitas</label>
                                    <input type="number" name="kuantitas" id="kuantitas" class="form-control" autofocus>
                                    <span class="text-danger" id="error-kuantitas"></span>
                                </div>
                                <div class="form-group">
                                    <label for="name">Kondisi</label>
                                    <select name="id_detail_kondisi" id="id_detail_kondisi" class="form-control" required>
                                        <option>-- PILIH KONDISI --</option>
                                        @foreach ($options_kondisi as $id => $nama)
                                            <option value="{{ $id }}">{{ $nama }}</option>
                                        @endforeach
                                    </select>
                                    <span class="text-danger" id="error-id_detail_kondisi"></span>
                                </div>
                                <div class="form-group">
                                    <label for="name">Status</label>
                                    <select name="status" id="status" class="form-control" required>
                                        <option>-- PILIH STATUS --</option>
                                        <option value="BARU">BARU</option>
                                        <option value="BEKAS">BEKAS</option>
                                    </select>
                                    <span class="text-danger" id="error-status"></span>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-sm btn-flat btn-primary" id="saveBtn">
                                    <i class="fa fa-save"></i> Save
                                </button>
                                <button type="button" class="btn btn-sm btn-flat btn-warning close-btn" data-dismiss="modal">
                                    <i class="fa fa-arrow-circle-left"></i> Cancel
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.Content -->

        <!-- Footer -->
        @include('component.footer')
        <!-- /.Footer -->

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>

   <!-- jQuery -->
<script src="/assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Select2 -->
<script src="/assets/plugins/select2/js/select2.full.min.js"></script>
<script src="https://kit.fontawesome.com/198ace4666.js" crossorigin="anonymous"></script>
<!-- InputMask -->
<script src="/assets/plugins/moment/moment.min.js"></script>
<script src="/assets/plugins/inputmask/jquery.inputmask.min.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="/assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Bootstrap -->
<script src="/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="/assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="/assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="/assets/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="/assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="/assets/plugins/jszip/jszip.min.js"></script>
<script src="/assets/plugins/pdfmake/pdfmake.min.js"></script>
<script src="/assets/plugins/pdfmake/vfs_fonts.js"></script>
<script src="/assets/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="/assets/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="/assets/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- overlayScrollbars -->
<script src="/assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="/assets/dist/js/adminlte.js"></script>
<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="/assets/plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="/assets/plugins/raphael/raphael.min.js"></script>
<script src="/assets/plugins/jquery-mapael/jquery.mapael.min.js"></script>
<script src="/assets/plugins/jquery-mapael/maps/usa_states.min.js"></script>
<!-- ChartJS -->
<script src="/assets/plugins/chart.js/Chart.min.js"></script>
<!-- SweetAlert2 -->
<script src="/assets/plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="/assets/plugins/toastr/toastr.min.js"></script>

{{-- <script>
 $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
    $(this).select2({
        dropdownParent: $(this).parent(),
    });

    //Initialize Select2 Elements
    // $('.select2bs4').select2({
    //   theme: 'bootstrap4'
    // })
})
</script> --}}
<script>
    /*** add active class and stay opened when selected ***/
    var url = window.location;

    // for sidebar menu entirely but not cover treeview
    $('ul.nav-sidebar a').filter(function() {
        if (this.href) {
            return this.href == url || url.href.indexOf(this.href) == 0;
        }
    }).addClass('active');

    // for the treeview
    $('ul.nav-treeview a').filter(function() {
        if (this.href) {
            return this.href == url || url.href.indexOf(this.href) == 0;
        }
    }).parentsUntil(".nav-sidebar > .nav-treeview").addClass('menu-open').prev('a').addClass('active');
</script>

<script>
    function showSuccessToast(message) {
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        });

        Toast.fire({
            icon: 'success',
            title: message // Menggunakan pesan dari parameter fungsi sebagai judul
        });
    }

    // Fungsi untuk menampilkan toast SweetAlert error
    function showErrorToast(message) {
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        });

        Toast.fire({
            icon: 'error',
            title: message
        });
    }
</script>

    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            let table = $('.table').DataTable({
                processing: true,
                autoWidth: true,
                responsive: true,
                lengthChange: true,
                processing: true,
                searching: false,
                paging: false,
                info: false,
                ordering: false,
                serverSide: true,
                dom: 'lfrtip',
                ajax: "{{ url('detail-kondisi') . '/' . $master_barang->id }}",
                columns: [{
                        data: 'DT_RowIndex',
                    },
                    {
                        data: 'status',
                        name: 'status',
                    },
                    {
                        data: 'kuantitas',
                        name: 'kuantitas',
                    },
                    {
                        data: 'nama_kondisi',
                        name: 'nama_kondisi',
                    },
                    {
                        data: 'keterangan_kondisi',
                        name: 'keterangan_kondisi',
                    },
                    {
                        data: 'aksi',
                        name: 'aksi',
                    }
                ]
            });
        });

        $('.close-btn').click(function(e) {
            $('.modal').modal('hide')
        });

        $("#modal-form form").on("submit", function(e) {
            e.preventDefault();
            var formdata = $(this).serializeArray();
            var data = {};

            $(formdata).each(function(index, obj) {
                data[obj.name] = obj.value;
            });

            if (validation(data, true)) {
                $.ajax({
                    data: $(this).serialize(),
                    url: "{{ url('add-detail-kondisi') . '/' . $master_barang->id }}",
                    type: "POST",
                    dataType: 'json',
                    success: function(data) {
                        $('#modal-form').modal('hide');
                        $('.table').DataTable().draw();
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Save Changes');
                    }
                });
            }
        })

        // $('body').on('click', '.editData', function() {
        //     var id = $(this).data('id');
        //     $.get("{{ route('pengguna.index') }}" + '/' + id + '/edit', function(data) {
        //         $('.modal-title').text('Edit Data');
        //         $('#modal-form').modal('show');
        //         $('#id').val(data.id);
        //         $('#nama_lengkap').val(data.nama_lengkap);
        //         $('#nama_pengguna').val(data.nama_pengguna);
        //         $('#sandi_pengguna').val("");
        //         $('#peran_pengguna').val(data.peran_pengguna);
        //     })
        // });

        $('body').on('click', '.deleteData', function() {
            var id = $(this).data("id");
            if (confirm("Kamu yakin ingin menghapus data?") == true) {
                $.ajax({
                    type: "DELETE",
                    url: "{{ url('delete-all-detail-kondisi') . '/' . $master_barang->id }}" + '/' + id,
                    success: function(data) {
                        $('.table').DataTable().draw();
                    },
                    error: function(data) {
                        console.log('Error:', data);
                    }
                });
            }
        });

        function addForm() {
            $("#modal-form").modal('show');
            $('#id').val('');
            $('.modal-title').text('Tambah Data');
            $('#modal-form form')[0].reset();
            $('#modal-form [name=nama_lengkap').focus();
            $('#modal-form [name=nama_pengguna').focus();
            $('#modal-form [name=sandi_pengguna').focus();
            $('#modal-form [name=peran_pengguna').focus();
        }

        function validation(data, isCreate) {
            let formIsValid = true;
            $('span[id^="error"]').text('');
            if (!data.kuantitas) {
                formIsValid = false;
                $("#error-kuantitas").text('Kuantitas wajib diisi.')
            }

            if (!data.id_detail_kondisi) {
                formIsValid = false;
                $("#error-id_detail_kondisi").text('Kondisi wajib diisi.')
            }

            if (!data.status) {
                formIsValid = false;
                $("#error-status").text('Status wajib diisi.')
            }
            return formIsValid;
        }
    </script>

    {{-- @if (session('success'))
        <script>
            // Menampilkan toast SweetAlert sukses
            Swal.fire({
                icon: 'success',
                title: '{{ session('success') }}, {{ auth()->user()->nama_lengkap }}',
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });
        </script>
    @endif --}}
</body>

</html>
