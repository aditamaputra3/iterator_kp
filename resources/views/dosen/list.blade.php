@extends('layout.layout-admin')

@section('title')
    {{ 'Dosen' }}
@endsection

@section('content')
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="modal-form">
        <div class="modal-dialog" role="document">
            <form class="form-horizontal">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title"></h4>
                        <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="id" id="id" class="form-control">
                        <div class="form-group">
                            <label for="name">NIP</label>
                            <input type="text" name="nip" id="nip" class="form-control" autofocus readonly>
                            <span class="text-danger" id="error-nip"></span>
                        </div>
                        <div class="form-group">
                            <label for="name">Nama Dosen</label>
                            <input type="text" name="nama_dosen" id="nama_dosen" class="form-control" autofocus>
                            <span class="text-danger" id="error-nama_dosen"></span>
                        </div>
                        <div class="form-group">
                            <label for="name">Email</label>
                            <input name="email" type="email" id="email" class="form-control">
                            <span class="text-danger" id="error-email"></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-warning close-btn" data-dismiss="modal"><i
                                class="fa fa-arrow-circle-left"></i> Cancel</button>
                        <button type="submit" class="btn btn-sm btn-primary" id="saveBtn"><i class="fa fa-save"></i>
                            Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-outline card-warning">
                        <div class="card-body">
                            <div class="d-flex justify-content-between mb-4">
                                <div class="d-flex justify-content-start" id="print">
                                    <!-- Second div content (if any) -->
                                </div>
                                <div class="d-flex justify-content-end">
                                    <button type="button" onclick="addForm()" class="btn btn-warning">
                                        <i class="fa fa-plus"></i> Tambah Dosen
                                    </button>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped custom-table mb-0 no-footer" role="grid">
                                    <thead>
                                        <tr role="row">
                                            <th>#</th>
                                            <th>NIP</th>
                                            <th>Nama Dosen</th>
                                            <th>Email</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        let routeUrl = "{{ route('dosen.index') }}";

            let columns = [{
                    data: 'DT_RowIndex',
                    searchable: false,
                    orderable: true
                },
                {
                    data: 'nip',
                    name: 'nip',
                },
                {
                    data: 'nama_dosen',
                    name: 'nama_dosen',
                },
                {
                    data: 'email',
                    name: 'email',
                },
                {
                    data: 'aksi',
                    name: 'aksi',
                    orderable: false,
                    searchable: false
                }
             ];

            let table = initializeDataTables(routeUrl, columns);

        $('body').on('click', '.editData', function() {
            var id = $(this).data('id');
            $.get("{{ route('dosen.index') }}" + '/' + id + '/edit', function(data) {
                $('.modal-title').text('Edit Data');
                $('#modal-form').modal('show');
                $('#id').val(data.id);

                $('#nama_dosen').val(data.nama_dosen);
                $('#email').val(data.email);

                 // Tampilkan form kode_praktikum pada mode edit
                 $('#nip').prop('readonly', true).val(data.nip);
            })
        });

        function addForm() {
            $("#modal-form").modal('show');
            $('#id').val('');
            $('.modal-title').text('Tambah Data');
            $('#modal-form form')[0].reset();
            $('#modal-form [name=nip').focus();
            $('#modal-form [name=nama_dosen').focus();
            $('#modal-form [name=email').focus();

              // Tampilkan form kode_praktikum pada mode edit
              $('#nip').prop('readonly', false).val(data.nip);
        }

        function validation(data, isCreate) {
            let formIsValid = true;
            $('span[id^="error"]').text('');
            if (!data.nama_dosen) {
                formIsValid = false;
                $("#error-nama_dosen").text('Nama dosen wajib diisi.')
            }
            return formIsValid;
        }
    </script>
@endsection
