@extends('layout.layout-admin')

@section('title', 'BAP Praktikum')

@section('content')
    {{-- <div class="container">
    <h1>Daftar BAP</h1>
    <div id="bap-container" class="row"></div>
</div> --}}

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-outline card-warning">
                        <div class="card-body">
                            <div class="row">
                                @foreach ($kodePraktikum as $praktikum)
                                    <div class="col-md-4">

                                        <div class="small-box bg-danger">
                                            <div class="inner">
                                                <h3>{{ $praktikum->kode_matakuliah }}</h3>
                                                <p>{{ $praktikum->nama_matakuliah }} - {{ $praktikum->kelas }}</p>
                                            </div>
                                            <div class="icon">
                                                <i class="fas fa-chart-pie"></i>
                                            </div>
                                            <a href="{{ route('bap.topik', ['kode_praktikum' => encrypt($praktikum->kode_praktikum)]) }}"
                                                class="small-box-footer">
                                                 Lihat BAP <i class="fas fa-arrow-circle-right"></i>
                                             </a>
                                             
                                        </div>

                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
