@extends('layout.layout-admin')

@section('title', 'BAP')

@section('content')

    <section class="content-header">
        <a href="{{ url('bap') }}">
            <h5 class="mb-2" style="color: #565656">
                <i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Kembali
            </h5>
        </a>

        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>@yield('title') - {{ $praktikum->kode_matakuliah }} - {{ $praktikum->nama_matakuliah }} -
                        {{ $praktikum->kelas }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('bap') }}">{{ $praktikum->kode_matakuliah }}</a></li>
                        <li class="breadcrumb-item active">@yield('title')</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="modal-form">
        <div class="modal-dialog" role="document">
            <form class="form-horizontal">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title"></h4>
                        <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{-- <form id="asisten-form"> --}}
                        <input type="hidden" name="id" id="id" class="form-control">
                        <input type="hidden" name="kode_praktikum" id="kode_praktikum"
                            value="{{ $praktikum->kode_praktikum }}">
                        {{-- <div class="form-group">
                            <label for="tanggal">Tanggal</label>
                            <input name="tanggal" id="tanggal" class="form-control"
                                value="{{ $tanggalSekarang->format('d-m-Y') }}" readonly>
                            <span class="text-danger" id="error-tanggal"></span>
                        </div> --}}

                        <div class="form-group">
                            <label for="topik">Materi Praktikum</label>
                            <textarea rows="3" name="topik" id="topik" class="form-control" autofocus></textarea>
                            <span class="text-danger" id="error-topik"></span>
                        </div>


                        @if (auth()->user()->peran_pengguna == 'KEPALA' ||
                                auth()->user()->peran_pengguna == 'TEKNISI' ||
                                auth()->user()->peran_pengguna == 'KOORDINATOR')
                            @php
                                $totalAsisten = count($Asisten);
                            @endphp

                            @for ($i = 0; $i < $totalAsisten; $i++)
                                <div class="form-group" data-mode="addForm">
                                    <label for="asisten{{ $i + 1 }}">Asisten {{ $i + 1 }}</label>
                                    <select name="asisten{{ $i + 1 }}" id="asisten{{ $i + 1 }}"
                                        class="form-control">
                                        <option value="HADIR" @if ($bap && $bap->{'asisten' . ($i + 1)} == 'HADIR') selected @endif>HADIR
                                        </option>
                                        <option value="TIDAK HADIR" @if ($bap && $bap->{'asisten' . ($i + 1)} == 'TIDAK HADIR') selected @endif>TIDAK
                                            HADIR</option>
                                    </select>
                                </div>
                            @endfor


                            @if ($bap)
                                <div class="form-group" data-mode="addForm">
                                    <label for="mahasiswa">Mahasiswa</label>
                                    <select name="mahasiswa" id="mahasiswa" class="form-control">
                                        <option value="HADIR" @if ($bap->mahasiswa == 'HADIR') selected @endif>HADIR
                                        </option>
                                        <option value="TIDAK HADIR" @if ($bap->mahasiswa == 'TIDAK HADIR') selected @endif>TIDAK
                                            HADIR</option>
                                    </select>
                                </div>
                            @endif

                        @endif
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-warning close-btn" data-dismiss="modal"><i
                                class="fa fa-arrow-circle-left"></i> Cancel</button>
                        <button type="submit" class="btn btn-sm btn-primary" id="saveBtn"><i class="fa fa-save"></i>
                            Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-outline card-warning">
                        <div class="card-body">
                            <div class="d-flex justify-content-between mb-4">
                                <div class="d-flex justify-content-start" id="print">
                                    <!-- Second div content (if any) -->
                                </div>
                                @if (auth()->user()->peran_pengguna == 'KEPALA' ||
                                        auth()->user()->peran_pengguna == 'TEKNISI' ||
                                        auth()->user()->peran_pengguna == 'KOORDINATOR' ||
                                        auth()->user()->peran_pengguna == 'ASISTEN')
                                    <div class="d-flex justify-content-end">
                                        <button type="button" onclick="addForm()" class="btn btn-warning">
                                            <i class="fa fa-plus"></i> Tambah BAP
                                        </button>
                                    </div>
                                @endif
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped custom-table mb-0 no-footer" role="grid">
                                    <thead>
                                        <tr role="row">
                                            <th>Pertemuan</th>
                                            <th>Tanggal</th>
                                            <th>Materi Praktikum</th>
                                            @if (auth()->user()->peran_pengguna == 'ASISTEN')
                                                <!-- Kolom Asisten untuk peran ASISTEN -->
                                                <th>{{ auth()->user()->nama_lengkap }} - {{ $nrpPengguna }} </th>
                                            @elseif (auth()->user()->peran_pengguna == 'KEPALA' ||
                                                    auth()->user()->peran_pengguna == 'TEKNISI' ||
                                                    auth()->user()->peran_pengguna == 'KOORDINATOR')
                                                @foreach ($Asisten as $asisten)
                                                    <th>{{ $asisten->nama_lengkap }}</th>
                                                @endforeach

                                                @for ($i = count($Asisten); $i < 3; $i++)
                                                    <th></th>
                                                @endfor

                                            @endif
                                            <th>Mahasiswa</th>
                                            @if (auth()->user()->peran_pengguna == 'ASISTEN' ||
                                                    auth()->user()->peran_pengguna == 'KEPALA' ||
                                                    auth()->user()->peran_pengguna == 'TEKNISI' ||
                                                    auth()->user()->peran_pengguna == 'KOORDINATOR')
                                                <th>Aksi</th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection

@section('script')
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });

        let table = $('.table').DataTable({
            processing: true,
            autoWidth: false,
            responsive: true,
            lengthChange: true,
            processing: true,
            serverSide: true,
            order: [
                [1, 'asc']
            ],
            dom: "<'row'<'col-sm-3'l><'col-sm-6 text-center'B><'col-sm-3'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            lengthMenu: [
                [25, 100, -1],
                [25, 100, "All"]
            ],
            fixedColumns: {
                left: 2
            },
            pageLength: 25,
            buttons: [
                @if (auth()->user()->peran_pengguna != 'ASISTEN' && auth()->user()->peran_pengguna != 'MAHASISWA')
                    // 'colvis',
                    {
                        extend: 'collection',
                        text: '<i class="fa fa-print"></i>  Export',
                        className: 'btn btn-success',
                        buttons: [{
                                extend: 'excel',
                                title: 'BAP {{ $praktikum->kode_matakuliah }} - {{ $praktikum->nama_matakuliah }} - {{ $praktikum->kelas }}',
                                exportOptions: {
                                    columns: [0, 1, 2, 3, 4, 5, 6]
                                }

                            },
                            {
                                extend: 'pdf',
                                filename: 'BAP_{{ $praktikum->kode_matakuliah }}_{{ $praktikum->nama_matakuliah }}_{{ $praktikum->kelas }}',
                                title: '',
                                orientation: 'landscape',
                                download: 'open',
                                exportOptions: {
                                    columns: [0, 1, 2, 3, 4, 5, 6]
                                },

                                customize: function(doc) {
                                    // Tambahkan teks dan gambar di atas tabel saat ekspor PDF
                                    doc.content.splice(0, 0, {
                                        columns: [{
                                            stack: [{
                                                    text: 'Institut Teknologi Nasional',
                                                    style: 'header',
                                                    alignment: 'center',
                                                    fontSize: 14,
                                                    bold: true
                                                }, // Tambahkan 'bold: true' di sini
                                                {
                                                    text: 'Program Studi Sistem Informasi',
                                                    style: 'subheader',
                                                    alignment: 'center',
                                                    fontSize: 14,
                                                    bold: true,
                                                    margin: [0, 3]
                                                }, // Tambahkan 'bold: true' di sini
                                                {
                                                    text: 'Jl. PKH. Mustapa No,23 Telp.7272215 - (Fax) 7202892 Bandung 40124',
                                                    style: 'subheader',
                                                    alignment: 'center',
                                                    fontSize: 10
                                                }
                                            ],
                                            alignment: 'center'
                                        }],
                                        margin: [0, 10]
                                    });

                                    doc.content.splice(1, 0, {
                                        text: 'BAP Praktikum: {{ $praktikum->kode_matakuliah }} - {{ $praktikum->nama_matakuliah }}',
                                        style: 'header'
                                    });
                                    doc.content.splice(2, 0, {
                                        text: 'Kelas: {{ $praktikum->kelas }}',
                                        style: 'subheader',
                                        margin: [0, 5] // Tambahkan spasi setelah kelas
                                    });

                                    doc.content.splice(3, 0, {
                                        text: '', // Ruang kosong
                                        margin: [0,
                                            10
                                        ] // Tambahkan ruang kosong antara kelas dan tabel
                                    });

                                    // Mengubah gaya tabel
                                    doc.styles.tableHeader = {
                                        fontSize: 10, // Ubah ukuran font header tabel
                                        bold: true, // Teks tebal
                                        fillColor: '#D8D8D8', // Warna latar belakang header tabel
                                        color: 'black', // Warna teks header tabel
                                        alignment: 'center', // Tengahkan teks
                                        margin: [0, 5], // Tambahkan margin atas dan bawah
                                        border: [true, true, true,
                                            true
                                        ] // Atur border tabel (kiri, atas, kanan, bawah)
                                    };
                                    doc.styles.tableBodyEven = {
                                        fontSize: 10, // Ubah ukuran font sel-genap
                                        fillColor: '#F2F2F2', // Warna latar belakang sel-genap
                                        alignment: 'center', // Tengahkan teks
                                        margin: [0, 5] // Tambahkan margin atas dan bawah
                                    };
                                    doc.styles.tableBodyOdd = {
                                        fontSize: 10, // Ubah ukuran font sel-ganjil
                                        fillColor: 'white', // Warna latar belakang sel-ganjil
                                        alignment: 'center', // Tengahkan teks
                                        margin: [0, 5] // Tambahkan margin atas dan bawah
                                    };

                                    // Menerapkan gaya tabel ke semua tabel dalam dokumen
                                    doc.defaultStyle = {
                                        fontSize: 10
                                    };

                                    // Mengatur lebar tabel sesuai halaman
                                    doc.content[doc.content.length - 1].table.widths = Array(doc
                                            .content[doc
                                                .content.length - 1].table.body[0].length + 1).join('*')
                                        .split(
                                            '');

                                    // Menambahkan ruang kosong antara "Program Studi Sistem Informasi" dan isi tabel
                                    doc.content.splice(1, 0, {
                                        text: '',
                                        margin: [0,
                                            10
                                        ]
                                    });
                                }

                            },
                            {
                                extend: 'csv',
                                title: 'BAP {{ $praktikum->kode_matakuliah }} - {{ $praktikum->nama_matakuliah }} - {{ $praktikum->kelas }}',
                                exportOptions: {
                                    columns: [0, 1, 2, 3, 4, 5, 6]
                                }
                            },
                            {
                                extend: 'print',
                                title: 'BAP {{ $praktikum->kode_matakuliah }} - {{ $praktikum->nama_matakuliah }} - {{ $praktikum->kelas }}',
                                exportOptions: {
                                    columns: [0, 1, 2, 3, 4, 5, 6]
                                },
                                customize: function(win) {
                                    // Tambahkan teks di atas tabel saat mencetak
                                    $(win.document.body).prepend(
                                        '<div>Institut Teknologi Nasional Bandung</div><div>Program Studi Sistem Informasi</div><br>'
                                    );
                                }
                            },
                            {
                                extend: 'copy',
                                title: 'BAP {{ $praktikum->kode_matakuliah }} - {{ $praktikum->nama_matakuliah }} - {{ $praktikum->kelas }}',
                                exportOptions: {
                                    columns: [0, 1, 2, 3, 4, 5, 6]
                                }
                            }
                        ]
                    }
                @endif
            ],

            ajax: "{{ route('bap.topik', ['kode_praktikum' => ':kode_praktikum']) }}".replace(':kode_praktikum',
                '{{ encrypt($praktikum->kode_praktikum) }}'),
            columns: [{
                    data: 'DT_RowIndex',
                    searchable: false,
                    orderable: false
                },
                {
                    data: 'tanggal',
                    name: 'tanggal',
                    orderable: true
                 
                },
                {
                    data: 'topik',
                    name: 'topik',
                    orderable: false
                },


                @if (auth()->user()->peran_pengguna == 'ASISTEN')
                    {
                        data: 'validasi',
                        name: 'validasi',
                        orderable: false,
                        searchable: false,
                        // render: function(data, type, row) {
                        //     var button = '';

                        //     // Periksa status HADIR atau TIDAK HADIR untuk setiap kolom asisten
                        //     var statusAsisten1 = row.asisten1;
                        //     var statusAsisten2 = row.asisten2;
                        //     var statusAsisten3 = row.asisten3;

                        //      // Jika salah satu dari kolom asisten memiliki status HADIR, tampilkan label dengan latar belakang hijau
                        //      if (statusAsisten1 == 'HADIR' || statusAsisten2 == 'HADIR' || statusAsisten3 == 'HADIR') {
                        //         label = '<span class="badge bg-success">HADIR</span>';
                        //     } 
                        //     // Jika tidak ada kolom asisten yang memiliki status HADIR, tampilkan label dengan latar belakang merah
                        //     else {
                        //         label = '<span class="badge bg-danger">TIDAK HADIR</span>';
                        //     }

                        //     return label;
                        // }
                    },
                @elseif (auth()->user()->peran_pengguna == 'KEPALA' ||
                        auth()->user()->peran_pengguna == 'TEKNISI' ||
                        auth()->user()->peran_pengguna == 'KOORDINATOR')

                    {
                        data: 'asisten1',
                        name: 'asisten1',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, row) {
                            if (data === 'HADIR') {
                                return '<span class="badge bg-success">HADIR</span>';
                            } else if (data === 'TIDAK HADIR') {
                                return '<span class="badge bg-danger">TIDAK HADIR</span>';
                            } else if (data !== null && data !== 'HADIR' && data !== 'TIDAK HADIR') {
                                return '<span class="badge bg-warning">MENUNGGU</span>';
                            } else if (data == null) {
                                return null;
                            }
                            return data; 
                        },
                        visible: function(data, type, row) {
                            return row.asisten1 !== null;

                        }

                    }, {
                        data: 'asisten2',
                        name: 'asisten2',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, row) {
                            if (data === 'HADIR') {
                                return '<span class="badge bg-success">HADIR</span>';
                            } else if (data === 'TIDAK HADIR') {
                                return '<span class="badge bg-danger">TIDAK HADIR</span>';
                            } else if (data !== null && data !== 'HADIR' && data !== 'TIDAK HADIR') {
                                return '<span class="badge bg-warning">MENUNGGU</span>';
                            } else if (data == null) {
                                return null;
                            }
                            return data; // Kembalikan nilai asli jika tidak cocok dengan kondisi di atas
                        },
                        visible: function(data, type, row) {
                            return row.asisten1 !== null;
                        }
                    }, {
                        data: 'asisten3',
                        name: 'asisten3',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, row) {
                            if (data === 'HADIR') {
                                return '<span class="badge bg-success">HADIR</span>';
                            } else if (data === 'TIDAK HADIR') {
                                return '<span class="badge bg-danger">TIDAK HADIR</span>';
                            } else if (data !== null && data !== 'HADIR' && data !== 'TIDAK HADIR') {
                                return '<span class="badge bg-warning">MENUNGGU</span>';
                            } else if (data == null) {
                                return null;
                            }
                            return data; // Kembalikan nilai asli jika tidak cocok dengan kondisi di atas
                        },
                        visible: function(data, type, row) {

                            return (data !== null);
                        }

                    },
                @endif

                @if (auth()->user()->peran_pengguna == 'MAHASISWA')
                    {
                        data: 'validasiMhs',
                        name: 'validasiMhs',
                        orderable: false,
                        searchable: false,
                    },
                @endif
                @if (auth()->user()->peran_pengguna == 'ASISTEN' ||
                        auth()->user()->peran_pengguna == 'KEPALA' ||
                        auth()->user()->peran_pengguna == 'TEKNISI' ||
                        auth()->user()->peran_pengguna == 'KOORDINATOR')
                    {
                        data: 'mahasiswa',
                        name: 'mahasiswa',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, row) {
                            if (data === 'HADIR') {
                                return '<span class="badge bg-success">HADIR</span>';
                            } else if (data === 'TIDAK HADIR') {
                                return '<span class="badge bg-danger">TIDAK HADIR</span>';
                            } else if (data == null) {
                                return '<span class="badge bg-warning">MENUNGGU</span>';
                            }
                            return data; // Kembalikan nilai asli jika tidak cocok dengan kondisi di atas
                        },

                    },

                    {
                        data: 'aksi',
                        name: 'aksi',
                        orderable: false,
                        searchable: false
                    },
                @endif

            ],
            order: [
                
    // Mengatur urutan berdasarkan tanggal dari yang terbaru ke yang terlama
    [1, 'asc'] // Urutan 1 merujuk pada kolom tanggal
],
            // Add this line to hide columns with null data
            // columnDefs: [{
            //     targets: [4,5], // Replace with the appropriate column indexes
            //     visible: false,
            //     render: function(data) {
            //         return data !== null ? data : '';
            //     }
            // }],
        }).buttons().container().appendTo('#print');

        //         table.on('draw', function () {
        //     table.columns().every(function (colIdx) {
        //         var column = this;
        //         if (column.data().every(function (data, index) { return data == null; })) {
        //             $(column.header()).hide();
        //         } else {
        //             $(column.header()).show();
        //         }
        //     });
        // });

        $('body').on('click', '.editData', function() {
            var id = $(this).data('id');
            $.get("{{ route('bap.index') }}" + '/' + id + '/edit', function(data) {
                $('.modal-title').text('Edit Data');
                $('#modal-form').modal('show');
                $('#id').val(data.id);
                $('#topik').val(data.topik);
                $('#asisten1').val(data.asisten1);
                $('#asisten2').val(data.asisten2);
                $('#asisten3').val(data.asisten3);
                $('#mahasiswa').val(data.mahasiswa);

            });
        });

        // Add a submit event listener to your form
        $("#modal-form form").on("submit", function(e) {
            e.preventDefault();
            var formdata = $(this).serializeArray();
            var data = {};

            $(formdata).each(function(index, obj) {
                data[obj.name] = obj.value;
            });

            if (validation(data, true)) {
                $.ajax({
                    data: $(this).serialize(),
                    url: "{{ route('bap.store') }}", // Replace with your actual route
                    type: "POST",
                    dataType: 'json',
                    success: function(data) {
                        $('#modal-form').modal('hide');
                        $('.table').DataTable().draw();
                        if (data.success) {
                            showSuccessToast(data.success);
                        }
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Save Changes');
                        if (data.responseJSON && data.responseJSON.error) {
                            showErrorToast(data.responseJSON.error);
                        } else {
                            showErrorToast('An error occurred while saving the data.');
                        }
                    }
                });
            }
        });

        $('body').on('click', '.deleteData', function() {
            var id = $(this).data("id");
            if (confirm("Kamu yakin ingin menghapus data?") == true) {
                $.ajax({
                    type: "DELETE",
                    url: "{{ route('bap.store') }}" + '/' + id,
                    success: function(data) {
                        $('.table').DataTable().draw();
                        // Tampilkan pesan 'success' dari response JSON
                        if (data.success) {
                            showSuccessToast(data.success); // Gunakan response JSON sebagai judul toast
                        }
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Save Changes');
                        // Tampilkan pesan 'error' dari response JSON jika ada
                        if (data.error) {
                            showErrorToast(data.error);
                        } else {
                            // Tampilkan pesan error kustom jika tidak ada pesan error dalam response JSON
                            showErrorToast('An error occurred while delete the data.');
                        }
                    }
                });
            }
        });


        function addForm() {
            $("#modal-form").modal('show');
            $('#id').val('');
            $('.modal-title').text('Tambah Data');
            $('#modal-form form')[0].reset();
            $('#modal-form [name=tanggal').focus();
            $('#modal-form [name=topik').focus();

            $('[data-mode="addForm"]').hide();

        }

        function validation(data, isCreate) {
            let formIsValid = true;
            $('span[id^="error"]').text('');
            if (!data.topik) {
                formIsValid = false;
                $("#error-topik").text('Topik Praktikum wajib diisi.')
            }
            return formIsValid;
        }

        $('body').on('click', '.validateAsisten', function(e) {
            e.preventDefault();
            var id = $(this).data('id');
            var column = $(this).data('column');

            if (confirm('Are you sure you want to validate this data?')) {
                $.ajax({
                    type: 'POST',
                    url: '{{ route('bap.validate') }}',
                    data: {
                        id: id,
                        column: column
                    },
                    dataType: 'json',
                    success: function(data) {
                        // Handle the success response
                        if (data.success) {
                            showSuccessToast(data.success);
                            // Refresh the DataTable to reflect the updated data
                            $('.table').DataTable().draw();
                        }
                    },
                    error: function(data) {
                        // Handle the error response
                        console.log('Error:', data);
                        if (data.responseJSON && data.responseJSON.error) {
                            showErrorToast(data.responseJSON.error);
                        } else {
                            showErrorToast('An error occurred while validating the data.');
                        }
                    }
                });
            }
        });

        $('body').on('click', '.validateMahasiswa', function(e) {
            e.preventDefault();
            var id = $(this).data('id');
            var column = $(this).data('column');

            if (confirm('Validasi BAP?')) {
                $.ajax({
                    type: 'POST',
                    url: '{{ route('bap.validateMhs') }}',
                    data: {
                        id: id,
                        column: column
                    },
                    dataType: 'json',
                    success: function(data) {
                        // Handle the success response
                        if (data.success) {
                            showSuccessToast(data.success);
                            // Refresh the DataTable to reflect the updated data
                            $('.table').DataTable().draw();
                        }
                    },
                    error: function(data) {
                        // Handle the error response
                        console.log('Error:', data);
                        if (data.responseJSON && data.responseJSON.error) {
                            showErrorToast(data.responseJSON.error);
                        } else {
                            showErrorToast('An error occurred while validating the data.');
                        }
                    }
                });
            }
        });
    </script>
@endsection
