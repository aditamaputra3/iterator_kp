 <!-- /.content-wrapper -->
 <footer class="main-footer">
    <strong>Copyright &copy; {{ now()->year }} Aditama Putra, Team SIP <a href="https://www.itenas.ac.id/brosur-program-studi/fti/program-studi-sistem-informasi/">  Prodi Sistem Informasi ITENAS</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 2.0
    </div>
  </footer>