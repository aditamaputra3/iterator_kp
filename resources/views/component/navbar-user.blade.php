<nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top ">
    <div class="container px-5">
        <a class="navbar-brand" href="{{ route('index') }}"><img width="25" src="/assets/iterator.png" /> ITERATOR</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span
                class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item"><a class="nav-link" href="{{ route('index') }}">Home</a></li>
                <li class="nav-item"><a class="nav-link" href="{{ route('asisten.index') }}">Asisten</a></li>
                <li class="nav-item"><a class="nav-link" href="{{ route('jadwal-praktikum.index') }}">Jadwal Praktikum</a></li>

                <!-- Untuk Auth -->
                @auth
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        @if (auth()->user()->peran_pengguna === 'MAHASISWA' || auth()->user()->peran_pengguna === 'ASISTEN'  || auth()->user()->peran_pengguna === 'KOORDINATOR')
                            <li class="nav-item">
                                <a class="nav-link" href="/bap">Dashboard</a>
                            </li>
                        @else
                            <li class="nav-item">
                                <a class="nav-link" href="/dashboard">Dashboard</a>
                            </li>
                        @endif
                    </ul>

                </ul>
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-user" aria-hidden="true"></i> {{ auth()->user()->nama_pengguna }}
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                        {{-- <li><a class="dropdown-item" href="#">Profile</a></li> --}}
                        <li><a class="dropdown-item" href="{{ route('logout') }}">Logout</a></li>
                    </ul>
                </div>
                <!-- Tampa Auth -->
            @else
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                    </li>
                    <li class="nav-item">
                    </li>
                </ul>
                </ul>
                <a class="btn btn-outline-light" href="login" role="button">Login <i class="fa fa-arrow-right"
                        aria-hidden="true"></i></a>
            @endauth
        </div>
    </div>
</nav>
