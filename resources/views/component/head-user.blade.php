<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Lab IS</title>
    <!-- Favicon-->
    <link rel="icon" href="/assets/favicon.ico" type="image/x-icon">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="/assets/plugins/fontawesome-free/css/all.min.css">
    <!-- Bootstrap icons-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="/assets/css/styles.css" rel="stylesheet" />
    <link rel="manifest" href="/manifest.json">
    <!-- DataTables -->
    <link rel="stylesheet" href="/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="/assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
    {{-- <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Nunito:ital,wght@0,200..1000;1,200..1000&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Source+Sans+3:ital,wght@0,200..900;1,200..900&display=swap" rel="stylesheet">
  
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Nunito:ital,wght@0,200..1000;1,200..1000&family=PT+Sans:ital,wght@0,400;0,700;1,400;1,700&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Source+Sans+3:ital,wght@0,200..900;1,200..900&display=swap" rel="stylesheet"> --}}

    <style>
        /* Gaya CSS untuk menghitamkan latar belakang carousel */
        .carousel {
            background-color: #000;
            overflow: hidden;
            /* Mengatasi potensi overflow */
        }

        /* Tambahkan gaya CSS untuk teks di tengah carousel */
        .carousel-caption {
            text-align: center;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            /* Atur latar belakang menjadi transparan */
            padding: 20px;
            border-radius: 0;
            /* Hapus border-radius */
            width: 70%;
            /* Lebar caption 100% */
            z-index: 2;
            /* Tambahkan z-indeks yang lebih tinggi */
        }


        /* Gaya teks di dalam caption */
        .carousel-caption h1 {
            font-size: 3rem;
            /* Ukuran font teks "SELAMAT DATANG" */
            font-weight: bold;
            /* Teks tebal */
            color: white;
            /* Warna teks */
            font-family: 'Poppins', sans-serif;
        }

        .carousel-caption p {
            font-size: 1.5rem;
            /* Ukuran font teks deskripsi */
            font-weight: 300;
            /* Teks tipis */
            color: white;
            /* Warna teks */
            font-family: 'Poppins', sans-serif;
        }

        /* Gaya responsif untuk teks di dalam caption */
        @media (max-width: 768px) {
            .carousel-caption h1 {
                font-size: 2rem;
            }

            .carousel-caption p {
                font-size: 1rem;
            }
        }

        /* Gaya untuk mengatur ukuran dan crop gambar dalam carousel */
        .carousel-inner img {
            width: 100%;
            /* Lebar gambar penuh carousel */
            height: 100vh;
            /* Tinggi gambar mengisi seluruh tinggi viewport */
            object-fit: cover;
            /* Untuk mengcrop gambar agar sesuai dengan tinggi dan lebar viewport */
        }

        /* Gaya untuk menghitamkan gambar */
        .drk:after {
            content: "";
            display: block;
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            background: black;
            opacity: 0.6;
            z-index: 1;
        }
    </style>

    <style>
        .feature-card {
            background-color: #fff;
            border: 1px solid #ddd;
            padding: 20px;
            border-radius: 5px;
            text-align: center;
            height: 100%;
            opacity: 0;
            transform: translateY(50px);
            transition: opacity 0.5s ease, transform 0.5s ease;
        }

        .feature-card.show {
            opacity: 1;
            /* Menampilkan elemen */
            transform: translateY(0);
            /* Mengembalikan ke posisi semula */
        }

        .feature-card:hover {
            transform: translateY(-5px);
        }

        .feature-icon {
            width: 60px;
            height: 60px;
            border-radius: 50%;
            display: flex;
            justify-content: center;
            align-items: center;
            margin: 0 auto 20px;
            font-size: 24px;
        }
    </style>

    <style>
        .card {
            border-radius: 15px;
            /* Rounded corners for the card */
        }

        .badge {
            font-size: 11px;
            padding: 6px 8px;
        }

        .shadow {
            box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);
            /* Adding shadow to the card */
        }

        .card-header {
            text-align: center;
            padding: 6px;
            font-size: 14px;
            font-weight: 600;

        }

        .card-body {
            padding: 15px;
        }

        .asisten-list {
            max-height: 250px;
            /* Tinggi maksimum untuk kartu */
            overflow-y: auto;
            /* Tambahkan scrollbar jika konten lebih panjang dari maksimum */
        }

        .asisten {
            display: flex;
            align-items: center;
            /* Sejajarkan item secara vertikal */
        }

        .nama {
            flex: 1;
            /* Memungkinkan nama_lengkap untuk mengambil ruang tersisa */
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
        }

    </style>


</head>
