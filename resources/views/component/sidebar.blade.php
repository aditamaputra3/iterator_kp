<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-warning elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link">
        <img src="/assets/iterator.png" alt="Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-medium">ITERATOR</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                data-accordion="false">

                @if (Auth::check() && (Auth::user()->peran_pengguna == 'TEKNISI' || Auth::user()->peran_pengguna == 'KEPALA'))
                    <li class="nav-item">
                        <a href="{{ url('/dashboard') }}" class="nav-link">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>
                                Dashboard
                            </p>
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a href="{{ url('/kategori') }}" class="nav-link">
                            <i class="nav-icon fas fa-cart-plus"></i>
                            <p>
                                Inventory
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-copy"></i>
                            <p>
                                Master Data
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item ">
                                <a href="{{ url('/dosen') }}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>
                                        Dosen
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ url('/matakuliah') }}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>
                                        Matakuliah
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ url('/aslab') }}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>
                                        Asisten Lab
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ url('/mahasiswa') }}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>
                                        Mahasiswa
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ url('/pemasok') }}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>
                                        Pemasok
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ url('/detail-kondisi') }}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>
                                        Detail Kondisi
                                    </p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('/praktikum') }}" class="nav-link">
                            <i class="nav-icon fas fa-book"></i>
                            <p>
                                Praktikum
                            </p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-copy"></i>
                            <p>
                                Jadwal
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ url('/jadwal') }}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>
                                        Jadwal Praktikum
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item ">
                                <a href="{{ url('/pengajuan-jadwal') }}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>
                                        Pengajuan Jadwal
                                    </p>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item">
                        <a href="{{ url('/bap') }}" class="nav-link">
                            <i class="nav-icon fas fa-book"></i>
                            <p>
                                BAP Praktikum
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('/pengguna') }}" class="nav-link">
                            <i class="nav-icon fas fa-user"></i>
                            <p>
                                User
                            </p>
                        </a>
                    </li>
                @endif

                @if (Auth::check() && Auth::user()->peran_pengguna == 'ASISTEN')
            
                   
                    <li class="nav-item">
                        <a href="{{ url('/bap') }}" class="nav-link">
                            <i class="nav-icon fas fa-book"></i>
                            <p>
                                BAP Praktikum
                            </p>
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a href="{{ url('/pengajuan-jadwal') }}" class="nav-link">
                            <i class="nav-icon fas fa-cart-plus"></i>
                            <p>
                                Pengajuan Jadwal
                            </p>
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a href="{{ url('/profile-aslab') }}" class="nav-link">
                            <i class="nav-icon fas fa-user"></i>
                            <p>
                                Profile
                            </p>
                        </a>
                    </li>
                @endif
                @if (Auth::check() && Auth::user()->peran_pengguna == 'KOORDINATOR')
                <li class="nav-item">
                    <a href="{{ url('/bap') }}" class="nav-link">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            BAP Praktikum
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('/jadwal') }}" class="nav-link">
                        <i class="far fa-clock nav-icon"></i>
                        <p>
                            Jadwal Praktikum
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('/praktikum') }}" class="nav-link">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            Praktikum
                        </p>
                    </a>
                </li>
                @endif
                @if (Auth::check() && Auth::user()->peran_pengguna == 'MAHASISWA')
                <li class="nav-item">
                    <a href="{{ url('/bap') }}" class="nav-link">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            BAP Praktikum
                        </p>
                    </a>
                </li>
                @endif
            </ul>

        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
