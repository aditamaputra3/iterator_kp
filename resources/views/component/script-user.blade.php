<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<!-- Bootstrap -->
<script src="/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="/assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="/assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="/assets/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="/assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="/assets/plugins/jszip/jszip.min.js"></script>
<script src="/assets/plugins/pdfmake/pdfmake.min.js"></script>
<script src="/assets/plugins/pdfmake/vfs_fonts.js"></script>
<script src="/assets/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="/assets/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="/assets/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>

<!-- overlayScrollbars -->
<script src="/assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="/assets/js/scripts.js"></script>



<script>
    function initializeDataTables(routeUrl, columns) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        return $('.table').DataTable({
            processing: true,
            autoWidth: false,
            responsive: true,
            lengthChange: true,
            processing: true,
            serverSide: true,
            // dom: "<'row'<'col-sm-3'l><'col-sm-6 text-center'B><'col-sm-3'f>>" +
            //     "<'row'<'col-sm-12'tr>>" +
            //     "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            // buttons: [{
            //     extend: 'collection',
            //     text: '<i class="fa fa-print"></i>  Export',
            //     className: 'btn btn-success',
            //     buttons: [{
            //         extend: 'excel',
            //         title: 'excel'
            //     }, {
            //         extend: 'pdf',
            //         title: 'Pdf '
            //     }, {
            //         extend: 'csv',
            //         title: 'Csv '
            //     }, {
            //         extend: 'print',
            //         title: 'print '
            //     }, {
            //         extend: 'copy',
            //         title: 'copy'
            //     }]
            // }],
            ajax: routeUrl,
            columns: columns
        }).buttons().container().appendTo('#print');
    }

     // Fungsi untuk memuat ulang data pada DataTables
     function reloadDataTables() {
            $('.table').DataTable().ajax.reload();
        }

        // Fungsi untuk mengatur interval autorefresh
        function setAutoRefresh(interval) {
            setInterval(function() {
                reloadDataTables();
            }, interval);
        }

        // Panggil fungsi setAutoRefresh dengan interval yang diinginkan (dalam milidetik)
        setAutoRefresh(20000); // Contoh autorefresh setiap 5 detik (5000 milidetik)

    // Expose the function to the global scope
    window.initializeDataTables = initializeDataTables;
</script>

<script>
    // Fungsi untuk memeriksa apakah elemen masuk ke dalam viewport
    function isElementInViewport(el) {
        var rect = el.getBoundingClientRect();
        return (
            rect.top >= 0 &&
            rect.left >= 0 &&
            rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
            rect.right <= (window.innerWidth || document.documentElement.clientWidth)
        );
    }

    // Fungsi untuk menampilkan elemen saat masuk ke dalam viewport
    function showElementsOnScroll() {
        var elements = document.querySelectorAll('.feature-card');
        elements.forEach(function (el) {
            if (isElementInViewport(el)) {
                el.classList.add('show');
            }
        });
    }

    // Panggil fungsi saat halaman dimuat dan di-scroll
    document.addEventListener('DOMContentLoaded', function () {
        showElementsOnScroll();
    });

    window.addEventListener('scroll', function () {
        showElementsOnScroll();
    });
</script>

{{-- 
<script>
    window.addEventListener("scroll", function() {
        var navbar = document.querySelector(".navbar");
        var carousel = document.querySelector(".carousel");
        var carouselHeight = carousel.offsetHeight;

        if (window.scrollY > carouselHeight) {
            navbar.classList.add("navbar-scrolled");
        } else {
            navbar.classList.remove("navbar-scrolled");
        }
    });
</script> --}}

{{-- <script>
    window.addEventListener('scroll', function() {
    var navbar = document.getElementById('navbar');
    if (window.scrollY > 50) {
        navbar.classList.remove('transparent');
        navbar.classList.add('bg-dark');
    } else {
        navbar.classList.remove('bg-dark');
        navbar.classList.add('transparent');
    }
});

</scrip> --}}


