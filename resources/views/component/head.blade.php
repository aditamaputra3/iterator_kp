    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>ITERATOR - @yield('title')</title>

    <link rel="icon" href="/assets/favicon.ico" type="image/x-icon">
    {{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/admin-lte@3.2/dist/css/adminlte.min.css"> --}}
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="/assets/plugins/fontawesome-free/css/all.min.css">
    <!-- IONIC Icons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="/assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/assets/dist/css/adminlte.min.css">
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="/assets/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="/assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
    <!-- Toastr -->
    <link rel="stylesheet" href="/assets/plugins/toastr/toastr.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/select2.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-datetimepicker.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet" href="/assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="/assets/plugins/select2/css/select2.min.css">

    {{-- <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/font-awesome.min.css">

    <link rel="stylesheet" type="text/css" href="/assets/css/dataTables.bootstrap4.min.css"> --}}
{{-- 
    <link rel="stylesheet" type="text/css" href="/assets/css/style.css"> --}}
    {{-- <link rel="stylesheet" href="plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css"> --}}

    {{-- <link rel="stylesheet" type="text/css" href="plugins/easyui/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="plugins/easyui/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="plugins/easyui/demo/demo.css"> --}}

    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.13.18/jquery.timepicker.min.css"> --}}
    {{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/timepicker@1.14.0/jquery.timepicker.min.css"> --}}
    {{-- Felia Notes :
    aku nyisipin csrf token disini buat nanti dipake ngehit data di javascript
    btw komentar ini boleh dihapus kalo mengganggu    
    --}}
    <link rel="manifest" href="/manifest.json">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{-- Felia Notes :
    coba kalian cari tau tehnik laravel template, biar kodenya ga sepanjang ini jadi bisa lebih pendek ngodingnya
    itu bakal ngambil waktu banyak di awal tapi kedepannya ngoding bisa cepet karna kode jadi lebih sedikit    

    tapi ngambil waktu di awalnya juga ga akan lama sih
    
    oh iya btw komentar ini boleh dihapus kalo mengganggu
    --}}

    <style>
        .resitdc-image-choose {
            position: relative;
            width: 100%;
            height: 30vh;
            border: 1px dashed #dedede;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .resitdc-image-choose .resitdc-image-choose-input {
            position: absolute;
            width: 100% !important;
            height: 100% !important;
            top: 0;
            left: 0;
            opacity: 0;
            cursor: pointer;
        }

        .resitdc-image-choose .resitdc-image-choose-preview::after {
            display: inline-block;
            font: normal normal normal 14px/1 FontAwesome;
            font-size: inherit;
            text-rendering: auto;
            -webkit-font-smoothing: antialiased;
            content: "\f093";
            font-size: 4rem;
            color: #eaeaea;
            width: 100%;
            height: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .resitdc-image-choose .resitdc-image-choose-preview[style^="background-image:"] {
            width: 100%;
            height: 100%;
            background-size: contain;
            background-repeat: no-repeat;
            background-position: center;
        }

        .resitdc-image-choose .resitdc-image-choose-preview[style^="background-image:"]::after {
            display: none !important;
        }
    </style>
<style>
    /* Set a fixed width for 'asisten' columns */


</style>