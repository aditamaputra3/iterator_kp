@extends('layout.layout-admin')

@section('title')
    {{ 'Pemasok' }}
@endsection

@section('content')
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="modal-form">
        <div class="modal-dialog" role="document">
            <form class="form-horizontal">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title"></h4>
                        <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="id" id="id" class="form-control">
                        <div class="form-group">
                            <label for="name">Nama Pemasok</label>
                            <input type="text" name="nama_pemasok" id="nama_pemasok" class="form-control" autofocus>
                            <span class="text-danger" id="error-nama_pemasok"></span>
                        </div>
                        <div class="form-group">
                            <label for="name">Alamat Pemasokk</label>
                            <textarea name="alamat_pemasok" id="alamat_pemasok" class="form-control"></textarea>
                            <span class="text-danger" id="error-alamat_pemasok"></span>
                        </div>
                        <div class="form-group">
                            <label for="name">Nomor Telepon Pemasok</label>
                            <textarea name="no_telp_pemasok" id="no_telp_pemasok" class="form-control"></textarea>
                            <span class="text-danger" id="error-no_telp_pemasok"></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-warning close-btn" data-dismiss="modal"><i
                                class="fa fa-arrow-circle-left"></i> Cancel</button>
                        <button type="submit" class="btn btn-sm btn-primary" id="saveBtn"><i class="fa fa-save"></i>
                            Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-outline card-warning">
                        <div class="card-body">
                            <div class="d-flex justify-content-between mb-4">
                                <div class="d-flex justify-content-start" id="print">
                                    <!-- Second div content (if any) -->
                                </div>
                                <div class="d-flex justify-content-end">
                                    <button type="button" onclick="addForm()" class="btn btn-warning">
                                        <i class="fa fa-plus"></i> Tambah Pemasok
                                    </button>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped custom-table mb-0 no-footer" role="grid">
                                    <thead>
                                        <tr role="row">
                                            <th>#</th>
                                            <th>Nama Pemasok</th>
                                            <th>Alamat Pemasok</th>
                                            <th>Nomor Telepon Pemasok</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        let routeUrl = "{{ route('pemasok.index') }}";

        let columns = [{
                data: 'DT_RowIndex',
                searchable: false,
                orderable: true
            },
            {
                data: 'nama_pemasok',
                name: 'nama_pemasok',
            },
            {
                data: 'alamat_pemasok',
                name: 'alamat_pemasok',
            },
            {
                data: 'no_telp_pemasok',
                name: 'no_telp_pemasok',
            },
            {
                data: 'aksi',
                name: 'aksi',
                orderable: false,
                searchable: false
            }
        ];

        let table = initializeDataTables(routeUrl, columns);

        $('body').on('click', '.editData', function() {
            var id = $(this).data('id');
            $.get("{{ route('pemasok.index') }}" + '/' + id + '/edit', function(data) {
                $('.modal-title').text('Edit Data');
                $('#modal-form').modal('show');
                $('#id').val(data.id);
                $('#nama_pemasok').val(data.nama_pemasok);
                $('#alamat_pemasok').val(data.alamat_pemasok);
                $('#no_telp_pemasok').val(data.no_telp_pemasok);
            })
        });

        function addForm() {
            $("#modal-form").modal('show');
            $('#id').val('');
            $('.modal-title').text('Tambah Data');
            $('#modal-form form')[0].reset();
            $('#modal-form [name=nama_pemasok').focus();
            $('#modal-form [name=alamat_pemasok').focus();
            $('#modal-form [name=no_telp_pemasok').focus();
        }

        function validation(data, isCreate) {
            let formIsValid = true;
            $('span[id^="error"]').text('');
            if (!data.nama_pemasok) {
                formIsValid = false;
                $("#error-nama_pemasok").text('Nama pemasok wajib diisi.')
            }
            return formIsValid;
        }
    </script>
@endsection
