@extends('layout.layout-admin')

@section('title')
    {{ 'Kondisi' }}
@endsection

@section('content')
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="modal-form">
        <div class="modal-dialog" role="document">
            <form class="form-horizontal">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title"></h4>
                        <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="id" id="id" class="form-control">
                        <div class="form-group">
                            <label for="name">Nama Kondisi</label>
                            <input type="text" name="nama_kondisi" id="nama_kondisi" class="form-control" autofocus>
                            <span class="text-danger" id="error-nama_kondisi"></span>
                        </div>
                        <div class="form-group">
                            <label for="name">Keterangan Kondisi</label>
                            <textarea name="keterangan_kondisi" id="keterangan_kondisi" class="form-control"></textarea>
                            <span class="text-danger" id="error-keterangan_kondisi"></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-sm btn-flat btn-primary" id="saveBtn"><i
                                class="fa fa-save"></i> Save</button>
                        <button type="button" class="btn btn-sm btn-flat btn-warning close-btn" data-dismiss="modal"><i
                                class="fa fa-arrow-circle-left"></i> Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-outline card-warning">
                        <div class="card-body">
                            <div class="d-flex justify-content-between mb-4">
                                <div class="d-flex justify-content-start" id="print">
                                    <!-- Second div content (if any) -->
                                </div>
                                <div class="d-flex justify-content-end">
                                    <button type="button" onclick="addForm()" class="btn btn-warning">
                                        <i class="fa fa-plus"></i> Tambah Kondisi
                                    </button>
                                </div>

                            </div>

                            <div class="table-responsive">
                                <table class="table table-striped custom-table mb-0 no-footer" role="grid">
                                    <thead>
                                        <tr role="row">
                                            <th>#</th>
                                            <th>Nama Kondisi</th>
                                            <th>Keterangan Kondisi</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        let routeUrl = "{{ route('detail-kondisi.index') }}";

        let columns = [{
                data: 'DT_RowIndex',
                searchable: false,
                orderable: true,
            },
            {
                data: 'nama_kondisi',
                name: 'nama_kondisi',
            },
            {
                data: 'keterangan_kondisi',
                name: 'keterangan_kondisi',
            },
            {
                data: 'aksi',
                name: 'aksi',
                orderable: false,
                searchable: false
            }
        ];

        let table = initializeDataTables(routeUrl, columns);


        $('body').on('click', '.editData', function() {
            var id = $(this).data('id');
            $.get("{{ route('detail-kondisi.index') }}" + '/' + id + '/edit', function(data) {
                $('.modal-title').text('Edit Data');
                $('#modal-form').modal('show');
                $('#id').val(data.id);
                $('#nama_kondisi').val(data.nama_kondisi);
                $('#keterangan_kondisi').val(data.keterangan_kondisi);
            })
        });

        function addForm() {
            $("#modal-form").modal('show');
            $('#id').val('');
            $('.modal-title').text('Tambah Data');
            $('#modal-form form')[0].reset();
            $('#modal-form [name=nama_kondisi').focus();
        }

        function validation(data, isCreate) {
            let formIsValid = true;
            $('span[id^="error"]').text('');
            if (!data.nama_kondisi) {
                formIsValid = false;
                $("#error-nama_kondisi").text('Nama kondisi wajib diisi.')
            }
            return formIsValid;
        }
    </script>
@endsection
