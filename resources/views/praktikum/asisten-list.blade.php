@extends('layout.layout-front')

@section('title')
    {{ 'Asisten Praktikum' }}
@endsection

@section('content')
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
                    <h2 class="mb-4" align="center">Asisten Lab Sistem Informasi</h2>
                </div>
                @foreach ($groupedData as $kodePraktikum => $group)
                    <div class="col-md-4 mb-4">
                        <div class="card shadow rounded">
                            <div class="card-header bg-dark text-white rounded-top">
                                @if ($group->isNotEmpty())
                                    {{ $group[0]->kode_matakuliah }} {{ $group[0]->nama_matakuliah }} -
                                    {{ $group[0]->kelas }}
                                @endif
                            </div>
                            <div class="card-body p-4">
                                <div class="asisten-list">
                                    @foreach ($group as $asisten)
                                        <div class="asisten">
                                            <span class="badge bg-secondary bg-gradient bg-sm rounded-pill me-2 mb-1">{{ $asisten->nrp }}</span>
                                            <span class="nama">{{ $asisten->nama_lengkap }}</span>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    @endsection
