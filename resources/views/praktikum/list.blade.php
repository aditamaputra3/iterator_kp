@extends('layout.layout-admin')

@section('title','Praktikum')

{{-- @section('head')
    <style>
        .edit-mode .kode-praktikum-label {
            display: none;
        }
    </style>
@endsection --}}

@section('content')
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="modal-form">
        <div class="modal-dialog" role="document">
            <form class="form-horizontal">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title"></h4>
                        <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="id" id="id" class="form-control">
                        <div class="form-group">
                            <label for="kode_praktikum">Kode Praktikum</label>
                            <input type="text" name="kode_praktikum" id="kode_praktikum" class="form-control" readonly>
                            <span class="text-danger" id="error-kode_praktikum"></span>
                        </div>

                        <div class="form-group">
                            <label for="name">Nama Matakuliah</label>
                            <select name="kode_matakuliah" id="kode_matakuliah" class="form-control" required>
                                <option>-- Pilih Matakuliah --</option>
                                @foreach ($matakuliah as $kode_matakuliah => $nama_matakuliah)
                                    <option value="{{ $kode_matakuliah }}">{{ $kode_matakuliah }} - {{ $nama_matakuliah }}
                                    </option>
                                @endforeach
                            </select>
                            <span class="text-danger" id="error-kode_matakuliah"></span>
                        </div>
                        <div class="form-group">
                            <label for="name">Kelas</label>
                            <select name="kelas" id="kelas" class="form-control" required>
                                <option>-- Pilih Kelas --</option>
                                <option value="AA">AA</option>
                                <option value="BB">BB</option>
                                <option value="CC">CC</option>
                                <option value="DD">DD</option>
                                <option value="EE">EE</option>
                                <option value="FF">FF</option>
                                <option value="GG">GG</option>
                            </select>
                            <span class="text-danger" id="error-kelas"></span>
                        </div>
                        <div class="form-group">
                            <label for="name">Jumlah Peserta</label>
                            <input type="number" name="jumlah_peserta" id="jumlah_peserta" class="form-control" autofocus>
                            <span class="text-danger" id="error-jumlah-peserta"></span>
                        </div>

                        <input type="hidden" name="jumlah_week" id="jumlah_week" value="0" class="form-control">

                        <div class="form-group">
                            <label for="name">Koordinator Praktikum</label>
                            <select name="nip" id="nip" class="form-control" required>
                                <option>-- Pilih Koordinator Praktikum --</option>
                                @foreach ($dosen as $nip => $nama_dosen)
                                    <option value="{{ $nip }}">{{ $nama_dosen }}
                                    </option>
                                @endforeach
                            </select>
                            <span class="text-danger" id="error-kode_matakuliah"></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-warning close-btn" data-dismiss="modal"><i
                                class="fa fa-arrow-circle-left"></i> Cancel</button>
                        <button type="submit" class="btn btn-sm btn-primary" id="saveBtn"><i class="fa fa-save"></i>
                            Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-outline card-warning">
                        <div class="card-body">
                            <div class="d-flex justify-content-between mb-4">
                                <div class="d-flex justify-content-start" id="print">
                                    <!-- Second div content (if any) -->
                                </div>
                                <div class="d-flex justify-content-end">
                                    <button type="button" onclick="addForm()" class="btn btn-warning">
                                        <i class="fa fa-plus"></i> Tambah Praktikum
                                    </button>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped custom-table mb-0 no-footer" role="grid">
                                    <thead>
                                        <tr role="row">
                                            <th>#</th>
                                            <th>Kode Praktikum</th>
                                            <th>Matakuliah</th>
                                            <th>Kelas</th>
                                            <th>Jumlah Peserta</th>
                                            <th>Jumlah Pertemuan</th>
                                            <th>Koordinator</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        // AJAX route for DataTables
        let routeUrl = "{{ route('praktikum.index') }}";

        // Column configuration for DataTables
        let columns = [{
                data: 'DT_RowIndex',
                searchable: false,
                orderable: true
            },
            {
                data: 'kode_praktikum',
                name: 'kode_praktikum'
            },
            {
                data: null,
                render: function(data, type, row) {
                    // Gabungkan data 'nama_matakuliah', 'kode_praktikum', dan 'kode_matakuliah' dalam satu kolom
                    return data.kode_matakuliah + ' - ' + data.nama_matakuliah + '';
                },
                searchable: true, // Aktifkan pencarian pada kolom hasil render
                name: 'kode_matakuliah', // Nama kolom yang digunakan untuk pencarian
                name: 'nama_matakuliah',
                searchable: true // Aktifkan pencarian pada kolom nama_matakuliah
            },
            {
                data: 'kelas',
                name: 'kelas'
            },
            {
                data: 'jumlah_peserta',
                name: 'jumlah_peserta'
            },
            {
                data: 'jumlah_week',
                name: 'jumlah_week'
            },
            {
                data: 'nama_dosen',
                name: 'nama_dosen'
            },
            {
                data: 'aksi',
                name: 'aksi',
                orderable: false,
                searchable: false
            }
        ];

        let table = initializeDataTables(routeUrl, columns);
        
        $('body').on('click', '.editData', function() {
            var id = $(this).data('id');
            $.get("{{ route('praktikum.index') }}" + '/' + id + '/edit', function(data) {
                $('.modal-title').text('Edit Data');
                $('#modal-form').modal('show');
                $('#id').val(data.id);
                $('#kode_matakuliah').val(data.kode_matakuliah);
                $('#kelas').val(data.kelas);
                $('#jumlah_peserta').val(data.jumlah_peserta);
                $('#nip').val(data.nip);

                // Tampilkan form kode_praktikum pada mode edit
                $('#kode_praktikum').prop('hidden', false).val(data.kode_praktikum);

                // Fokus ke input kode_matakuliah saat mode edit
                $('#modal-form [name=kode_matakuliah').focus();

                // Tampilkan label kode_praktikum pada mode tambah data
                $('#modal-form label[for="kode_praktikum"]').show();
            });
        });

        function addForm() {
            $("#modal-form").modal('show');
            $('#id').val('');
            $('.modal-title').text('Tambah Data');
            $('#modal-form form')[0].reset();
            $('#modal-form [name=kode_matakuliah').focus();
            $('#modal-form [name=kelas').focus();
            $('#modal-form [name=jumlah_peserta').focus();
            $('#modal-form [name=nip').focus();

            // Sembunyikan label kode_praktikum pada mode edit
            $('#modal-form label[for="kode_praktikum"]').hide();

            // Sembunyikan form kode_praktikum pada mode tambah data
            $('#kode_praktikum').prop('hidden', true).val('');
        }


        function validation(data, isCreate) {
            let formIsValid = true;
            $('span[id^="error"]').text('');
            if (!data.kode_matakuliah) {
                formIsValid = false;
                $("#error-kode_matakuliah").text('Matakuliah wajib dipilih.')
            }
            return formIsValid;
        }
    </script>
@endsection
