@extends('layout.layout-admin')

@section('title', 'Asisten Lab')

{{-- @section('head')
    <style>
        .edit-mode .kode-praktikum-label {
            display: none;
        }
    </style>
@endsection --}}

@section('content')
<section class="content-header">
    <a href="{{ url('praktikum') }}">
        <h5 class="mb-2" style="color: #565656">
            <i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Kembali
        </h5>
    </a>

    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>@yield('title') - {{ $praktikum->kode_matakuliah }} - {{ $praktikum->nama_matakuliah }} - {{ $praktikum->kelas }}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item"><a href="praktikum">{{ $praktikum->kode_praktikum }}</a></li>
                    <li class="breadcrumb-item active">@yield('title')</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

        <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="modal-form">
            <div class="modal-dialog" role="document">
                <form class="form-horizontal">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title"></h4>
                            <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            {{-- <form id="asisten-form"> --}}
                            <input type="hidden" name="id" id="id" class="form-control">
                            <input type="hidden" name="kode_praktikum" value="{{ $praktikum->kode_praktikum }}">

                            <div class="form-group">
                                <label for="name">Nama Asisten Lab</label>
                                <select name="nrp" class="form-control" required>
                                    <option>-- Pilih Asisten Lab --</option>
                                    @foreach ($asistenPraktikum as $data)
                                        {{-- @if (!in_array($data['kode_praktikum'], $existingKodePraktikum)) --}}
                                        <option value="{{ $data['nrp'] }}">
                                            {{ $data['nrp'] }} - {{ $data['nama_lengkap'] }} </option>
                                        {{-- @endif --}}
                                    @endforeach
                                </select>
                                <span class="text-danger" id="error-nrp"></span>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-sm btn-warning close-btn" data-dismiss="modal"><i
                                    class="fa fa-arrow-circle-left"></i> Cancel</button>
                            <button type="submit" class="btn btn-sm btn-primary" id="saveBtn"><i class="fa fa-save"></i>
                                Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card card-outline card-warning">
                            <div class="card-body">
                                <div class="d-flex justify-content-between mb-4">
                                    <div class="d-flex justify-content-start" id="print">
                                        <!-- Second div content (if any) -->
                                    </div>
                                    <div class="d-flex justify-content-end">
                                        <button type="button" onclick="addForm()" class="btn btn-warning">
                                            <i class="fa fa-plus"></i> Tambah Asisten Lab
                                        </button>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-striped custom-table mb-0 no-footer" role="grid">
                                        <thead>
                                            <tr role="row">
                                                <th>No</th>
                                                <th>NRP</th>
                                                <th>Nama Asisten Lab</th>
                                                <th>No Telepon</th>
                                                <th>No Rekening</th>
                                                <th>Nama Bank</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endsection

    @section('script')
        <script>
            $(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
            });

            let table = $('.table').DataTable({
                processing: true,
                autoWidth: false,
                responsive: true,
                lengthChange: true,
                processing: true,
                serverSide: true,
                dom: "<'row'<'col-sm-3'l><'col-sm-6 text-center'B><'col-sm-3'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                buttons: [{
                    extend: 'collection',
                    text: '<i class="fa fa-print"></i>  Export',
                    className: 'btn btn-success',
                    buttons: [{
                        extend: 'excel',
                        title: 'excel'
                    },          {
                        extend: 'pdf',
                        filename: 'ASISTEN_{{ $praktikum->kode_matakuliah }}_{{ $praktikum->nama_matakuliah }}_{{ $praktikum->kelas }}',
                        title: '',
                        orientation: 'landscape',
                        download: 'open',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        },

                        customize: function(doc) {
                            // Tambahkan teks dan gambar di atas tabel saat ekspor PDF
                            doc.content.splice(0, 0, {
                                columns: [{
                                    stack: [{
                                            text: 'Institut Teknologi Nasional',
                                            style: 'header',
                                            alignment: 'center',
                                            fontSize: 14,
                                            bold: true
                                        }, // Tambahkan 'bold: true' di sini
                                        {
                                            text: 'Program Studi Sistem Informasi',
                                            style: 'subheader',
                                            alignment: 'center',
                                            fontSize: 14,
                                            bold: true,
                                            margin: [0, 3]
                                        }, // Tambahkan 'bold: true' di sini
                                        {
                                            text: 'Jl. PKH. Mustapa No,23 Telp.7272215 - (Fax) 7202892 Bandung 40124',
                                            style: 'subheader',
                                            alignment: 'center',
                                            fontSize: 10
                                        }
                                    ],
                                    alignment: 'center'
                                }],
                                margin: [0, 10]
                            });

                            doc.content.splice(1, 0, {
                                text: 'Asisten Praktikum: {{ $praktikum->kode_matakuliah }} - {{ $praktikum->nama_matakuliah }}',
                                style: 'header'
                            });
                            doc.content.splice(2, 0, {
                                text: 'Kelas: {{ $praktikum->kelas }}',
                                style: 'subheader',
                                margin: [0, 5] // Tambahkan spasi setelah kelas
                            });

                            doc.content.splice(3, 0, {
                                text: '', // Ruang kosong
                                margin: [0,
                                    10
                                ] // Tambahkan ruang kosong antara kelas dan tabel
                            });

                            // Mengubah gaya tabel
                            doc.styles.tableHeader = {
                                fontSize: 10, // Ubah ukuran font header tabel
                                bold: true, // Teks tebal
                                fillColor: '#D8D8D8', // Warna latar belakang header tabel
                                color: 'black', // Warna teks header tabel
                                alignment: 'center', // Tengahkan teks
                                margin: [0, 5], // Tambahkan margin atas dan bawah
                                border: [true, true, true,
                                    true
                                ] // Atur border tabel (kiri, atas, kanan, bawah)
                            };
                            doc.styles.tableBodyEven = {
                                fontSize: 10, // Ubah ukuran font sel-genap
                                fillColor: '#F2F2F2', // Warna latar belakang sel-genap
                                alignment: 'center', // Tengahkan teks
                                margin: [0, 5] // Tambahkan margin atas dan bawah
                            };
                            doc.styles.tableBodyOdd = {
                                fontSize: 10, // Ubah ukuran font sel-ganjil
                                fillColor: 'white', // Warna latar belakang sel-ganjil
                                alignment: 'center', // Tengahkan teks
                                margin: [0, 5] // Tambahkan margin atas dan bawah
                            };

                            // Menerapkan gaya tabel ke semua tabel dalam dokumen
                            doc.defaultStyle = {
                                fontSize: 10
                            };

                            // Mengatur lebar tabel sesuai halaman
                            doc.content[doc.content.length - 1].table.widths = Array(doc.content[doc
                                .content.length - 1].table.body[0].length + 1).join('*').split(
                                '');

                            // Menambahkan ruang kosong antara "Program Studi Sistem Informasi" dan isi tabel
                            doc.content.splice(1, 0, {
                                text: '',
                                margin: [0,
                                    10
                                ]
                            });
                        }
                    }, {
                        extend: 'csv',
                        title: 'Csv '
                    }, {
                        extend: 'print',
                        title: 'print '
                    }, {
                        extend: 'copy',
                        title: 'copy'
                    }]
                }],
                ajax: "{{ route('praktikum.asisten', ['kode_praktikum' => $praktikum->kode_praktikum]) }}",
                columns: [{
                        data: 'DT_RowIndex',
                        searchable: false,
                        orderable: true
                    },
                    {
                        data: 'nrp',
                        name: 'nrp'
                    },
                    {
                        data: 'nama_lengkap',
                        name: 'nama_legkap'
                    },
                    {
                        data: 'no_telp',
                        name: 'no_telp'
                    },
                    {
                        data: 'no_rekening',
                        name: 'no_rekening'
                    },
                    {
                        data: 'nama_bank',
                        name: 'nama_bank'
                    },
                    {
                        data: 'aksi',
                        name: 'aksi',
                        orderable: false,
                        searchable: false
                    }
                ]
            }).buttons().container().appendTo('#print');

            // $('body').on('click', '.editData', function() {
            //     var id = $(this).data('id');
            //     $.get("{{ route('praktikum.index') }}" + '/' + id + '/edit', function(data) {
            //         $('.modal-title').text('Edit Data');
            //         $('#modal-form').modal('show');
            //         $('#id').val(data.id);
            //         $('#kode_matakuliah').val(data.kode_matakuliah);
            //         $('#kelas').val(data.kelas);
            //         $('#jumlah_peserta').val(data.jumlah_peserta);
            //         $('#jumlah_week').val(data.jumlah_week);
            //         $('#nip').val(data.nip);

            //         // Tampilkan form kode_praktikum pada mode edit
            //         $('#kode_praktikum').prop('hidden', false).val(data.kode_praktikum);

            //         // Fokus ke input kode_matakuliah saat mode edit
            //         $('#modal-form [name=kode_matakuliah').focus();

            //         // Tampilkan label kode_praktikum pada mode tambah data
            //         $('#modal-form label[for="kode_praktikum"]').show();
            //     });
            // });

            // Add a submit event listener to your form
            $("#modal-form form").on("submit", function(e) {
                e.preventDefault();
                var formdata = $(this).serializeArray();
                var data = {};

                $(formdata).each(function(index, obj) {
                    data[obj.name] = obj.value;
                });

                if (validation(data, true)) {
                    $.ajax({
                        data: $(this).serialize(),
                        url: "{{ route('praktikum.storeAsisten') }}", // Replace with your actual route
                        type: "POST",
                        dataType: 'json',
                        success: function(data) {
                            $('#modal-form').modal('hide');
                            $('.table').DataTable().draw();
                            if (data.success) {
                                showSuccessToast(data.success);
                            }
                        },
                        error: function(data) {
                            console.log('Error:', data);
                            $('#saveBtn').html('Save Changes');
                            if (data.responseJSON && data.responseJSON.error) {
                                showErrorToast(data.responseJSON.error);
                            } else {
                                showErrorToast('An error occurred while saving the data.');
                            }
                        }
                    });
                }
            });

            $('body').on('click', '.deleteData', function() {
            var id = $(this).data("id");
            if (confirm("Kamu yakin ingin menghapus data?") == true) {
                $.ajax({
                    type: "DELETE",
                    url: "{{ route('praktikum.deleteAsisten', ['id' => ':id']) }}".replace(':id', id),
                    success: function(data) {
                        $('.table').DataTable().draw();
                        // Tampilkan pesan 'success' dari response JSON
                        if (data.success) {
                            showSuccessToast(data.success); // Gunakan response JSON sebagai judul toast
                        }
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Save Changes');
                        // Tampilkan pesan 'error' dari response JSON jika ada
                        if (data.error) {
                            showErrorToast(data.error);
                        } else {
                            // Tampilkan pesan error kustom jika tidak ada pesan error dalam response JSON
                            showErrorToast('An error occurred while delete the data.');
                        }
                    }
                });
            }
        });


            function addForm() {
                $("#modal-form").modal('show');
                $('#id').val('');
                $('.modal-title').text('Tambah Data');
                $('#modal-form form')[0].reset();
                $('#modal-form [name=nrp').focus();
            }


            function validation(data, isCreate) {
                let formIsValid = true;
                $('span[id^="error"]').text('');
                if (!data.nrp) {
                    formIsValid = false;
                    $("#error-nrp").text('Asisten wajib dipilih.')
                }
                return formIsValid;
            }
        </script>
    @endsection
