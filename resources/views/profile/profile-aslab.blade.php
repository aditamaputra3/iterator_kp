@extends('layout.layout-admin')

@section('title', 'Profil')

@section('content')
    <section class="content">

        <div class="container-fluid">
            <div class="row">
                <!-- Bagian Kiri - Informasi Profil -->
                <div class="col-md-3">
                    <div class="card card-warning card-outline">
                        <div class="card-body box-profile">
                            <div class="text-center">
                                <img class="profile-user-img img-fluid img-circle" src="/assets/img/user.jpg"
                                    alt="User profile picture">
                            </div>
                            <h3 class="profile-username text-center">{{ $profile->users->nama_lengkap }}</h3>
                            <p class="text-muted text-center">{{ $profile->nrp }}</p>
                            <hr>
                            <strong><i class="fas fa-book mr-1"></i> Asisten Lab</strong>
                            <p class="text-muted">
                                @foreach ($kodePraktikum as $index => $ap)
                                    {{ $ap->kode_matakuliah }} - {{ $ap->nama_matakuliah }} - Kelas {{ $ap->kelas }}
                                    <!-- Tambahkan spasi ke bawah jika ada dua data atau lebih -->
                                    @if ($index < count($kodePraktikum) - 1)
                                        <br><br>
                                    @endif
                                @endforeach
                            </p>
                            {{-- <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a> --}}
                        </div>
                    </div>
                </div>

                <!-- Bagian Kanan - Tab Activity, Timeline, Settings -->
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header p-2">
                            <div class="d-flex justify-content-between">
                                <h5>Data Asisten Lab</h5>
                                <button type="button" class="btn btn-sm btn-warning" id="edit-profile-btn">
                                    <i class="fa fa-edit"></i> Edit Profil
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="tab-content">
                                <!-- Tab Settings -->
                                <div class="tab-pane active" id="settings">
                                    <form id="edit-profile-form">
                                        @csrf
                                        @method('PUT')

                                        <input type="text" id="nama" name="nrp"
                                            value="{{ $profile->users->nama_lengkap }}" hidden>

                                        <input type="text" id="nrp" name="nrp" value="{{ $profile->nrp }}"
                                            hidden>

                                        <div class="form-group">
                                            <label for="no_telp">No Telp</label>
                                            <input type="number" class="form-control" id="no_telp" name="no_telp"
                                                value="{{ $profile->no_telp }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="no_rekening">No Rekening</label>
                                            <input type="number" class="form-control" id="no_rekening" name="no_rekening"
                                                value="{{ $profile->no_rekening }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="nama_bank">Nama Bank</label>
                                            <input type="text" class="form-control" id="nama_bank" name="nama_bank"
                                                value="{{ $profile->nama_bank }}">
                                        </div>
                                        <div class="d-flex justify-content-end">
                                            <button type="submit" class="btn btn-warning"><i class="fa fa-save"></i>
                                                Simpan Perubahan</button>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    @endsection
    @section('script')
        <script>
            $(document).ready(function() {
                $('#edit-profile-form :input:not(#nama, #nrp)').attr('readonly', 'readonly');
                $('#edit-profile-form button[type="submit"]').hide();
                var isEditing = false; // Variabel untuk mengontrol status edit
                var isDataChanged = false; // Variabel untuk mengontrol perubahan data

                // Fungsi untuk mengaktifkan input form dan tombol "Simpan"
                function enableForm() {
                    $('#edit-profile-form :input:not(#nama, #nrp)').removeAttr('readonly');
                    $('#edit-profile-form button[type="submit"]').prop('disabled', false);
                    $('#edit-profile-form button[type="submit"]').show();
                }

                // Sembunyikan tombol "Edit" saat tombol "Simpan" diklik
                $('#edit-profile-btn').click(function() {
                    $('#edit-profile-form').show();
                    isEditing = true; // Mengaktifkan mode edit saat tombol Edit Profil diklik
                    enableForm(); // Panggil fungsi untuk mengaktifkan input
                    $(this).hide(); // Sembunyikan tombol "Edit"
                });

                // Cek apakah terjadi perubahan pada input form
                $('#edit-profile-form :input:not(#nama, #nrp)').on('input', function() {
                    enableForm(); // Aktifkan form saat terjadi perubahan input
                    isDataChanged = true; // Aktifkan status perubahan data
                });

                // Kirim permintaan AJAX untuk mengubah profil
                $('#edit-profile-form').submit(function(e) {
                    e.preventDefault();
                    var formData = $(this).serialize();
                    var profileId = {{ $profile->id }}; // ID profil pengguna
                    var updateUrl = "{{ route('profile.aslab.update', ':id') }}".replace(':id', profileId);

                    $.ajax({
                        type: "PUT",
                        url: updateUrl,
                        data: formData,
                        success: function(data) {
                            // Tampilkan pesan 'success' dari response JSON
                            if (data.success) {
                                showSuccessToast(data
                                    .success); // Gunakan response JSON sebagai judul toast
                            }

                            isEditing = false; // Matikan mode edit setelah berhasil disimpan
                            $('#edit-profile-btn').show(); // Tampilkan kembali tombol "Edit"
                            $('#edit-profile-form button[type="submit"]').hide();
                            isDataChanged = false; // Setel status perubahan data menjadi false
                            $('#edit-profile-form :input:not(#nama, #nrp)').attr('readonly',
                                'readonly');
                            $('#edit-profile-form button[type="submit"]').prop('disabled', true);
                        },
                        error: function(data) {
                            console.log('Error:', data);

                            // Tampilkan pesan 'error' dari response JSON jika ada
                            if (data.responseJSON && data.responseJSON.error) {
                                showErrorToast(data.responseJSON.error);
                            } else {
                                // Tampilkan pesan error kustom jika tidak ada pesan error dalam response JSON
                                showErrorToast('An error occurred while saving the data.');
                            }
                        }
                    });
                });

                // Jika tidak ada perubahan data, maka tombol Simpan dinonaktifkan
                setInterval(function() {
                    if (!isEditing || !isDataChanged) {
                        // $('#edit-profile-form :input:not(#nama, #nrp)').attr('readonly', 'readonly');
                        $('#edit-profile-form button[type="submit"]').prop('disabled', true);
                    }
                }, 1000);
            });
        </script>
    @endsection
