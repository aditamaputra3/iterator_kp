<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PemasokController;
use App\Http\Controllers\PenggunaController;
use App\Http\Controllers\InventoryController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\LandingPageController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\KategoriBarangController;
use App\Http\Controllers\DetailKondisiController;
use App\Http\Controllers\DosenController;
use App\Http\Controllers\MatakuliahController;
use App\Http\Controllers\AsistenLabController;
use App\Http\Controllers\AsistenList;
use App\Http\Controllers\PraktikumController;
use App\Http\Controllers\JadwalController;
use App\Http\Controllers\JadwalUserController;
use App\Http\Controllers\PengajuanJadwalController;
use App\Http\Controllers\ProfileAslabController;
use App\Http\Controllers\BapController;
use App\Http\Controllers\MahasiswaController;
use App\Http\Controllers\BapPraktikumController;
use App\Models\Mahasiswa;
use Illuminate\Support\Facades\Auth;
use App\Models\Matakuliah;
use App\Models\PengajuanJadwal;
use App\Models\Praktikum;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::view('/', 'landingpage.index');

Route::get('/', [LandingPageController::class, 'index'])->name('index');

Route::view('/about', 'landingpage/about')->name('about');
Route::view('/contact', 'landingpage/contact')->name('contact');
Route::view('/error', 'errors.403')->name('error.403');

// Route::get('/login', function () {
//     return view('auth.login');
// })->name('login');
Route::post('/login', [AuthController::class, 'login'])->name('login');

// Route::view('/', 'landingpage/index')->name('index');
// Route::view('/about', 'landingpage/about')->name('about');
// Route::view('/contact', 'landingpage/contact')->name('contact');
// Route::view('/error', 'errors.403')->name('error.403');


Route::get('/login', function () {
    if (Auth::check()) {
        return redirect()->route('index'); // Redirect to home page
    } else {
        return view('auth.login');
    }
})->name('login');
Route::post('/login', [AuthController::class, 'login'])->name('login');

// Route::get('/jadwal-praktikum', [JadwalUserController::class, 'index'])->name('jadwal-praktikum');
Route::resource('jadwal-praktikum', JadwalUserController::class);
Route::resource('asisten', AsistenList::class);

Route::group(['middleware' => ['auth']], function () {
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');
});

Route::middleware(['auth', 'role:TEKNISI,KEPALA'])->group(function () {
    Route::resource('pengguna', PenggunaController::class);

    Route::resource('dosen', DosenController::class);

    Route::resource('matakuliah', MatakuliahController::class);

    Route::resource('aslab', AsistenLabController::class);

    Route::resource('detail-kondisi', DetailKondisiController::class, ['except' => [
        'create', 'update', 'show'
    ]]);
    Route::resource('inventory', InventoryController::class, ['except' => [
        'create', 'update', 'show'
    ]]);

    Route::resource('pemasok', PemasokController::class);

    Route::resource('mahasiswa', MahasiswaController::class);

    Route::resource('kategori', KategoriBarangController::class);

    Route::get('kategori', [KategoriBarangController::class, 'list_categories'])->name('kategori.list');

    Route::get('tambah-barang', [InventoryController::class, 'add']);
    Route::post('tambah-barang/save', [InventoryController::class, 'save']);
    Route::get('edit-barang/{id}', [InventoryController::class, 'edit']);
    Route::get('detail-barang/{id}', [InventoryController::class, 'detail']);
    Route::delete('delete-barang/{id}', [InventoryController::class, 'delete']);
    Route::get('detail-kondisi/{id}', [InventoryController::class, 'detail_kondisi']);
    Route::delete('delete-all-detail-kondisi/{id_barang}/{id}', [InventoryController::class, 'delete_all_detail_kondisi']);
    Route::post('add-detail-kondisi/{id}', [InventoryController::class, 'add_detail_kondisi']);

    Route::get('pengguna/export', [PenggunaController::class, 'export'])->name('pengguna.export');
    Route::post('pengguna/import', [PenggunaController::class, 'import'])->name('pengguna.import');

    Route::post('matakuliah/import', [MatakuliahController::class, 'import'])->name('matakuliah.import');
    // Route::get('jadwal', [JadwalController::class, 'index']);
});

Route::middleware(['auth', 'role:TEKNISI,KEPALA,ASISTEN'])->group(function () {

    Route::get('profile-aslab', [ProfileAslabController::class, 'index'])->name('profile.aslab');
    // Route::get('profile-aslab/{id}',[ProfileAslabController::class, 'edit'])->name('profile.aslab.update');
    Route::put('/profile-aslab/{id}', [ProfileAslabController::class, 'edit'])->name('profile.aslab.update');

    // Route::post('profile-aslab',[AsistenLabController::class],'profileEditAslab');

    Route::get('/getKodePraktikum/{nrp}/{id_user}', [PengajuanJadwalController::class, 'getKodePraktikum']);

    Route::resource('pengajuan-jadwal', PengajuanJadwalController::class);
    Route::post('/terima-pengajuan/{id}', [PengajuanJadwalController::class, 'terimaPengajuan'])->name('terima.pengajuan');
    Route::post('/tolak-pengajuan/{id}', [PengajuanJadwalController::class, 'tolakPengajuan'])->name('tolak.pengajuan');
    Route::post('/tambah-jadwal/{id}', [PengajuanJadwalController::class, 'tambahJadwal'])->name('tambah.jadwal');

 
    // Route::post('/praktikum/store-asisten',  [PraktikumController::class, 'storeAsisten'])->name('praktikum.storeAsisten');
    // Route::delete('/praktikum/delete-asisten/{id}',  [PraktikumController::class, 'deleteAsisten'])->name('praktikum.deleteAsisten');
   
    // Route::get('/bap', [BapPraktikumController::class, 'index'])->name('bap-praktikum.list');
    // Route::get('/bap/{kode_praktikum}', [BapPraktikumController::class, 'listBap'])->name('bap-praktikum.list');
    // Route::post('/bap/store', [BapPraktikumController::class, 'storeBap'])->name('bap-praktikum.store');
    // Route::delete('/bap/delete/{id}', [BapPraktikumController::class, 'deleteBap'])->name('bap-praktikum.delete');
    // Route::post('bap/validate/asisten', [BapPraktikumController::class, 'validateAsisten'])->name('validasi.asisten');
    // Route::post('bap/validate/mahasiswa', [BapPraktikumController::class, 'validateMhs'])->name('validasi.mahasiswa');


});

Route::middleware(['auth', 'role:TEKNISI,KEPALA,KOORDINATOR'])->group(function () {

    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

    Route::resource('jadwal', JadwalController::class);

    Route::resource('praktikum', PraktikumController::class);

    Route::get('/praktikum/asisten/{kode_praktikum}', [PraktikumController::class, 'asisten'])->name('praktikum.asisten');
    Route::post('/praktikum/store-asisten',  [PraktikumController::class, 'storeAsisten'])->name('praktikum.storeAsisten');
    Route::delete('/praktikum/delete-asisten/{id}',  [PraktikumController::class, 'deleteAsisten'])->name('praktikum.deleteAsisten');
 
});

Route::middleware(['auth', 'role:TEKNISI,KEPALA,ASISTEN,KOORDINATOR,MAHASISWA'])->group(function () {
    Route::get('/bap', [BapController::class, 'index'])->name('bap.list');
    Route::get('/bap/{kode_praktikum}', [BapController::class, 'list'])->name('bap.topik');
    // Route::post('/bap/store', [BapController::class, 'storeBap'])->name('bap.store');
    // Route::delete('/bap/delete/{id}', [BapController::class, 'deleteBap'])->name('bap.delete');
    Route::post('/bap/validate', [BapController::class, 'validateBap'])->name('bap.validate');
    Route::post('/bap/validateMhs', [BapController::class, 'validateMahasiswa'])->name('bap.validateMhs');
    // Route::get('bap/{id}/edit', [BapController::class, 'edit']);
    Route::resource('bap', BapController::class);
    Route::post('/custom-pdf-data', [BapController::class, 'getCustomPdfData'])->name('custom_pdf_data_endpoint');

});