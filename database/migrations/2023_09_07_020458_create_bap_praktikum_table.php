<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('bap_praktikum', function (Blueprint $table) {
            $table->id();
            $table->string('kode_praktikum',50); 
            $table->string('tanggal');
            $table->string('materi_praktikum');

            $table->timestamps();
    
            $table->foreign('kode_praktikum')->references('kode_praktikum')->on('praktikum')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('bap_praktikum');
    }
};
