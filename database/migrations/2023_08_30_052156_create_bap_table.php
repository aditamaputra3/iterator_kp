<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('bap', function (Blueprint $table) {
            $table->id();
            $table->string('kode_praktikum', 50);
            $table->foreign('kode_praktikum')->references('kode_praktikum')->on('praktikum')->onDelete('cascade');
            $table->string('tanggal')->nullable();;
            $table->string('topik')->nullable();;
            $table->string('asisten1', 50)->nullable();
            $table->string('asisten2', 50)->nullable();
            $table->string('asisten3', 50)->nullable();
            $table->string('mahasiswa', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('bap');
    }
};
