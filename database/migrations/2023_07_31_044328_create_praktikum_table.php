<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('praktikum', function (Blueprint $table) {
            $table->id();
            $table->string('kode_praktikum',50)->unique();
            $table->string('kode_matakuliah',50);
            $table->foreign('kode_matakuliah')->references('kode_matakuliah')->on('matakuliah');
            $table->string('kelas');
            $table->string('jumlah_peserta');
            $table->string('jumlah_week');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('praktikum');
    }
};
