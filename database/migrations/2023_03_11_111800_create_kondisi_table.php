<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('kondisi', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_master_barang');
            $table->unsignedBigInteger('id_detail_kondisi');
            $table->text('status');
            $table->foreign('id_master_barang')->references('id')->on('master_barang')->onDelete('cascade');
            $table->foreign('id_detail_kondisi')->references('id')->on('detail_kondisi')->onDelete('cascade');
            $table->timestamps();
        });   
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('kondisi');
    }
};
