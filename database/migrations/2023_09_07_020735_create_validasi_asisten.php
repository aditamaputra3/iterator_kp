<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('validasi_asisten', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('bap_id'); 
            $table->string('nrp');
            $table->string('status_kehadiran');
            $table->timestamps();

            $table->foreign('bap_id')->references('id')->on('bap_praktikum')->onDelete('cascade');
                });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('validasi_asisten');
    }
};
