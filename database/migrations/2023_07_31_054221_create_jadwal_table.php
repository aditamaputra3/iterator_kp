<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('jadwal', function (Blueprint $table) {
            $table->id();
            $table->string('kode_jadwal', 50)->unique();
            $table->string('kode_praktikum', 50);
            $table->foreign('kode_praktikum')->references('kode_praktikum')->on('praktikum')->onDelete('cascade');
            $table->string('hari', 50);
            $table->time('jam_mulai');
            $table->string('ruang');
            $table->integer('durasi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('jadwal');
    }
};
