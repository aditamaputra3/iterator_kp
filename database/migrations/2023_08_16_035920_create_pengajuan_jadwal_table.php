<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pengajuan_jadwal', function (Blueprint $table) {
            $table->id();
            $table->string('kode_pengajuan',50);
            $table->string('kode_praktikum',50);
            $table->foreign('kode_praktikum')->references('kode_praktikum')->on('praktikum');
            $table->string('hari', 50);
            $table->string('jam_mulai', 50);
            $table->string('jam_berakhir', 50);
            $table->string('ruang', 50);
            $table->integer('durasi');
            $table->enum('status_pengajuan',["menunggu", "diterima", "ditolak"])->default("menunggu");
            $table->string('id_user', 50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pengajuan_jadwal');
    }
};
