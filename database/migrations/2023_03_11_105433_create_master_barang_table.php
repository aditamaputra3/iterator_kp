<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('master_barang', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_kategori_barang');
            $table->unsignedBigInteger('id_pemasok');
            $table->string('nama_barang',50);
            $table->text('deskripsi_barang');
            $table->integer('harga');
            $table->string('spesifikasi',50);
            $table->string('merk',50);
            $table->date('tahun_masuk');
            $table->text('gambar_barang');
            $table->timestamps();
            $table->foreign('id_kategori_barang')->references('id')->on('kategory_barang')->onDelete('cascade');
            $table->foreign('id_pemasok')->references('id')->on('pemasok')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('master_barang');
    }
};
