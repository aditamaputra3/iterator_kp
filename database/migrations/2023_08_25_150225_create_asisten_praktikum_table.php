<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('asisten_praktikum', function (Blueprint $table) {
            $table->id();
            $table->string('kode_praktikum', 50);
            $table->foreign('kode_praktikum')->references('kode_praktikum')->on('praktikum')->onDelete('cascade');
            $table->string('nrp', 50);
            $table->foreign('nrp')->references('nrp')->on('asisten_lab')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('asisten_praktikum');
    }
};
