<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('konten', function (Blueprint $table) {
            $table->id();
            $table->string('peraturan_praktikum',100);
            $table->string('kelompok_praktikum',50);
            $table->string('jadwal_pengguna',50);
            $table->string('nilai_praktikum',50);
            $table->string('modul_praktikum',50);
            $table->string('perizinan_praktikum',50);
            $table->string('pengumuman',50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('konten');
    }
};
